<?php
return [
	'device_images' => array(
		'original' => 'device/images/original',
	),

	'device_mask' => array(
		'original' => 'device/mask/original',
	),

	'happy_client_slides' => array(
		'original' => 'happy_client_slides/original',
	),

	'namedBgPaths' => array(
		'original' => 'namedbg/original',
		'big' => 'namedbg/big',
		'medium' => 'namedbg/medium',
		'editor' => 'namedbg/editor',
		'small' => 'namedbg/small'
	),

	'namedBgSize' => array(
		'big' => array('width' => 100, 'height' => 100),
		'medium' => array('width' => 94, 'height' => 94),
		'small' => array('width' => 44, 'height' => 59)
	),

	'foto' => array(
		'original' => '/app/case/photos/big',
	),
];