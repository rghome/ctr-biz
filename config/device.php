<?php

return [
	'namedCaseFonts' => [
		'latin' => 'Big Noodle Titling',
        'cirilyc' => 'AA Higherup'
    ],
    'emptySettings' => [
	    'id' => NULL,
	    'deviceId' => NULL,
	    'settings' => [
	        'latin' => [
	            'group' => [
	                'x' => 0,
	                'y' => 0
	            ],
	            'main' => [
	                'template1' => [
	                    [
	                        'position' => [
	                            'x' => 206,
	                            'y' => 19,
	                        ],
	                        'size' => 113,
	                        'width' => 100,
	                        'matrix' => [
	                            'a' => 1,
	                            'b' => 0,
	                            'c' => 0,
	                            'd' => 2,
	                            'e' => 0,
	                            'g' => 0,
	                        ],
	                    ]
	                ],
	                'template2' => [
	                    [
	                        'position' => [
	                            'x' => 206,
	                            'y' => 19,
	                        ],
	                        'size' => 113,
	                        'width' => 100,
	                        'matrix' => [
	                            'a' => 1,
	                            'b' => 0,
	                            'c' => 0,
	                            'd' => 2,
	                            'e' => 0,
	                            'g' => 0,
	                        ],
	                    ],
	                    [
	                        'position' => [
	                            'x' => 206,
	                            'y' => 19,
	                        ],
	                        'size' => 113,
	                        'width' => 100,
	                        'matrix' => [
	                            'a' => 1,
	                            'b' => 0,
	                            'c' => 0,
	                            'd' => 2,
	                            'e' => 0,
	                            'g' => 0,
	                        ],
	                    ]
	                ],
	            ],
	            'logo' => [
	                'position' => [
	                    'x' => 121,
	                    'y' => -379,
	                ],
	                'size' => 30,
	            ],
	        ],
	        'cirilyc' => [
	            'group' => [
	                'x' => 0,
	                'y' => 0
	            ],
	            'main' => [
	                'template1' => [
	                    [
	                        'position' => [
	                            'x' => 206,
	                            'y' => 19,
	                        ],
	                        'size' => 113,
	                        'width' => 100,
	                        'matrix' => [
	                            'a' => 1,
	                            'b' => 0,
	                            'c' => 0,
	                            'd' => 2,
	                            'e' => 0,
	                            'g' => 0,
	                        ]
	                    ]
	                ],
	                'template2' => [
	                    [
	                        'position' => [
	                            'x' => 206,
	                            'y' => 19,
	                        ],
	                        'size' => 113,
	                        'width' => 100,
	                        'matrix' => [
	                            'a' => 1,
	                            'b' => 0,
	                            'c' => 0,
	                            'd' => 2,
	                            'e' => 0,
	                            'g' => 0,
	                        ]
	                    ],
	                    [
	                        'position' => [
	                            'x' => 206,
	                            'y' => 19,
	                        ],
	                        'size' => 113,
	                        'width' => 100,
	                        'matrix' => [
	                            'a' => 1,
	                            'b' => 0,
	                            'c' => 0,
	                            'd' => 2,
	                            'e' => 0,
	                            'g' => 0,
	                        ]
	                    ],
	                ],

	            ],
	            'logo' => [
	                'position' => [
	                    'x' => 121,
	                    'y' => -379,
	                ],
	                'size' => 30,
	            ],
	        ],
	    ],
	],
	'frontDeviceModel' => [
		'width' => '245',
		'height' => '417',
		'photoLayoutType' => 1,
		'housingPath' => '',
		'strokePath' => '',
		'detailPaths' => [],
		'backgroundPattern' => [
			'x' => 0,
			'y' => 0,
			'width' => 600,
			'height' => 600,
			'href' => '',
			'isActive' => false,
		],
		'backgroundColor' => '#fff',
		'backgroundImage' => [
			'x' => 0,
			'y' => 0,
			'width' => 0,
			'height' => 0,
			'href' => '',
			'scale' => 1,
			'rotate' => 0,
			'translate' => [
				'x' => 0,
				'y' => 0,
			],
		],
		'photos' => [
		],
		'stickers' => [
		],
		'texts' => [
		],
		'nominalElement' => [
		],
		'namedTextBgImage' => [
		],
		'nominalLogoElement' => [
		],
		'monochromeColors' => [
			'r' => 0,
			'g' => 0,
			'b' => 0,
		],
		'isNominal' => false,
		'deviceId' => '30',
		'nominalTemplate' => 'template1',
		'is2D' => false,
		'materialPhotoID' => NULL,
		'materialPhotoColorID' => NULL,
		'materialPhotoType' => 1,
		'isFreestyle' => false,
		'backgroundLayerIsVisible' => true,
		'isReliefPrint' => false,
		'isBadQuality' => false,
		'deviceType' => 'phone',
		'isMonochrome' => false,
		'strokeColor2d' => NULL,
	]
];