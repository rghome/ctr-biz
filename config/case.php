<?php
return [

		'photoSize' => array('big' => array('width' => 150, 'height' => 100)),

		'photoPaths' => array('original' =>
				'case/photos/original',
				'editor' => 'case/photos/editor',
				'big' => 'case/photos/big'),

		'photoPathsBg' =>array(
				'original' => 'case/user_bg/original',
				'editor' => 'case/user_bg/editor',
				'big' => 'case/user_bg/big'),

		'logo' => [
			'path' => 'case/logo',
			'size' => ['width' => 100, 'height' => 100]
		],

		'limit' => 30,

		'designerCasePrice' => 100,

	'namedBgPaths' => array('original' =>
		'case/namedbg/original',
		'editor' => 'case/namedbg/editor',
		'big' => 'case/namedbg/big'
	),

];