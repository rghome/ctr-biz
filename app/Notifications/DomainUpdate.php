<?php

namespace App\Notifications;

use App\Models\Domain;
use App\Notifications\Helpers\DomainUpdateMessages;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\DatabaseMessage as DatabaseMessageParent;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Notifications\Messages\DatabaseMessage;

/**
 * Class DomainUpdate
 * @package App\Notifications
 */
class DomainUpdate extends Notification
{
    use Queueable;

    /**
     * @var Domain
     */
    private $domain;

    /**
     * @var int
     */
    private $status;

    /**
     * @var string
     */
    private $message;

    /**
     * DomainUpdate constructor.
     * @param Domain $domain
     * @param int $status
     * @param int $messageId
     */
    public function __construct(Domain $domain, int $status, int $messageId)
    {
        $this->domain = $domain;
        $this->status = $status;
        $this->message = DomainUpdateMessages::MESSAGES[$messageId];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * @param $notifiable
     * @return DatabaseMessageParent
     */
    public function toDatabase($notifiable)
    {
        $databaseMessage = new DatabaseMessage();
        $databaseMessage
            ->setAuthor('Admin')
            ->setIcon('server')
            ->setMessage($this->message)
            ->setRowType($this->status)
            ->setLink(route('domains'))
        ;

        return $databaseMessage->process();
    }
}
