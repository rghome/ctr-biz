<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 5/30/17
 * Time: 2:00 PM
 */

namespace App\Notifications\Helpers;

/**
 * Class DomainUpdateMessages
 * @package App\Notifications\Helpers
 */
class DomainUpdateMessages
{
    const WRONG_NS  = 100;
    const OK        = 101;

    const MESSAGES = [
        100 => 'Неверно указаны NS.',
        101 => 'Домен готов к использованию.',
        102 => 'Неизвестный DNS. Проверьте домен у регистратора.'
    ];
}