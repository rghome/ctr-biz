<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 5/30/17
 * Time: 2:36 PM
 */

namespace App\Notifications\Messages;

/**
 * Class DatabaseMessage
 * @package App\Notifications\Messages
 */
class DatabaseMessage extends \Illuminate\Notifications\Messages\DatabaseMessage
{
    const ROW_TYPES = [
        1 => 'info',
        2 => 'success',
        3 => 'warning',
        4 => 'danger'
    ];

    /**
     * @var string
     */
    private $author;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $rowType;

    /**
     * @var string
     */
    private $link;

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return DatabaseMessage
     */
    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return DatabaseMessage
     */
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return DatabaseMessage
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function getRowType(): string
    {
        return $this->rowType;
    }

    /**
     * @param int $rowType
     * @return DatabaseMessage
     */
    public function setRowType(int $rowType): self
    {
        $this->rowType = self::ROW_TYPES[$rowType];

        return $this;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return DatabaseMessage
     */
    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return DatabaseMessage
     */
    public function process()
    {
        return new self([
            'author' => $this->getAuthor(),
            'icon' => $this->getIcon(),
            'message' => $this->getMessage(),
            'link' => $this->getLink()
        ]);
    }
}