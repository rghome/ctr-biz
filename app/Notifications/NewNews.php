<?php

namespace App\Notifications;

use App\Models\Domain;
use App\Notifications\Helpers\DomainUpdateMessages;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\DatabaseMessage as DatabaseMessageParent;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Notifications\Messages\DatabaseMessage;

class NewNews extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * @param $notifiable
     * @return DatabaseMessageParent
     */
    public function toDatabase($notifiable)
    {
        $databaseMessage = new DatabaseMessage();
        $databaseMessage
            ->setAuthor('Admin')
            ->setIcon('rss')
            ->setMessage('Добавленна новость')
            ->setLink(route('news'))
        ;

        return $databaseMessage->process();
    }
}
