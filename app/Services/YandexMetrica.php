<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 4/26/17
 * Time: 7:54 AM
 */

namespace App\Services;

use App\Models\StatisticCache;
use Carbon\Carbon;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\Models\Offer;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\HttpFoundation\Response;

class YandexMetrica
{
    /**
     * @var string
     */
    protected $url = 'https://api-metrika.yandex.ru/';

    /**
     * @var int
     */
    protected $cacheMinutes = 5;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var string
     */
    protected $defaultToken;

    /**
     * @var int
     */
    protected $counterId;

    /**
     * @var int
     */
    protected $goalId;

    /**
     * @var int
     */
    protected $userId;

    /**
     * @var string
     */
    protected $dimensions;

    /**
     * @var array
     */
    protected $extend = [];

    /**
     * @var string
     */
    protected $startDate;

    /**
     * @var string
     */
    protected $endDate;

    /**
     * @var string
     */
    protected $panelUtmName;

    /**
     * @var string
     */
    protected $panelUtmValue;

    /**
     * @var string
     */
    protected $wmUtmName;

    /**
     * @var
     */
    protected $mainGoalId;

    /**
     * YandexMetrica constructor.
     * @param $counterId
     */
    public function __construct(int $counterId = null)
    {
        $this->counterId = $counterId;
        $this->defaultToken = 'AQAAAAAH3wXJAAQ-eEwHZ4JHJEA2gutbaTKittk';
    }

    /**
     * @return array
     */
    public function getByOffer()
    {
        $urlParams = [
            'oauth_token' => $this->token,
            'ids' => $this->counterId,
            'metrics' => 'ym:s:goal'.$this->goalId.'users,ym:s:visits,ym:s:users',
            'dimensions' => 'ym:s:' . $this->dimensions,
            'filters' => 'ym:s:' . $this->panelUtmName . '==\'' . $this->panelUtmValue . '\'',
            'date1' => $this->startDate,
            'date2' => $this->endDate,
            'accuracy' => '0.4'
        ];

        $requestUrl = $this->url . 'stat/v1/data?' . $this->buildUrlParams($urlParams);

        return $this->getTotalResult($this->request($requestUrl));
    }

    /**
     * @return array
     */
    public function getByLanding()
    {
        $metrics = 'ym:s:goal' . $this->goalId . 'users';

        if ($this->mainGoalId == 0) {
            $metrics .= ',ym:s:visits,ym:s:users';
        } else {
            $metrics .= ',ym:s:goal' . $this->mainGoalId . 'visits,ym:s:goal' . $this->mainGoalId . 'users';
        }

        $urlParams = [
            'oauth_token' => $this->token,
            'ids' => $this->counterId,
            'metrics' => $metrics,
            'dimensions' => 'ym:s:' . $this->dimensions,
            'filters' => 'ym:s:' . $this->panelUtmName . '==\'' . $this->panelUtmValue . '\'',
            'date1' => $this->startDate,
            'date2' => $this->endDate,
            'accuracy' => '0.4'
        ];

        $requestUrl = $this->url . 'stat/v1/data?' . $this->buildUrlParams($urlParams);

        return $this->getTotalResult($this->request($requestUrl));
    }

    /**
     * @return array
     */
    public function getUtmByOffer()
    {
        $urlParams = [
            'oauth_token' => $this->token,
            'ids' => $this->counterId,
            'metrics' => 'ym:s:goal'.$this->goalId.'users,ym:s:visits,ym:s:users',
            'dimensions' => 'ym:s:' . $this->dimensions,
            'filters' => urlencode('ym:s:' . $this->wmUtmName . '=@\'wm_\' and ym:s:' . $this->dimensions . '=~\'__[0-9]+\''),
            'date1' => $this->startDate,
            'date2' => $this->endDate,
            'accuracy' => '0.4'
        ];

        $requestUrl = $this->url . 'stat/v1/data?' . $this->buildUrlParams($urlParams);

        return $this->getTotalResult($this->request($requestUrl));
    }

    /**
     * @return array
     */
    public function getUtmByLanding()
    {
        $metrics = 'ym:s:goal' . $this->goalId . 'users';

        if ($this->mainGoalId == 0) {
            $metrics .= ',ym:s:visits,ym:s:users';
        } else {
            $metrics .= ',ym:s:goal' . $this->mainGoalId . 'visits,ym:s:goal' . $this->mainGoalId . 'users';
        }

        $urlParams = [
            'oauth_token' => $this->token,
            'ids' => $this->counterId,
            'metrics' => $metrics,
            'dimensions' => 'ym:s:' . $this->dimensions,
            'filters' => urlencode('ym:s:' . $this->wmUtmName . '=@\'wm_\' and ym:s:' . $this->dimensions . '=~\'__[0-9]+\''),
            'date1' => $this->startDate,
            'date2' => $this->endDate,
            'accuracy' => '0.4'
        ];

        $requestUrl = $this->url . 'stat/v1/data?' . $this->buildUrlParams($urlParams);

        return $this->getTotalResult($this->request($requestUrl));
    }

    /**
     * @return int
     */
    public function getTotalUsers()
    {
        $urlParams = [
            'oauth_token' => $this->token,
            'start-date' => $this->startDate,
            'end-date' => $this->endDate,
            'ids' => 'ga:' . $this->counterId,
            'metrics' => 'ym:s:visits',
            'filters' => $this->buildFilters(),
            'accuracy' => '0.4'
        ];

        $requestUrl = $this->url . 'analytics/v3/data/ga?' . $this->buildUrlParams($urlParams);


        return $this->getResult($this->request($requestUrl));
    }

    /**
     * @return int
     */
    public function getNewUsers()
    {
        $urlParams = [
            'oauth_token' => $this->getToken(),
            'start-date' => $this->startDate,
            'end-date' => $this->endDate,
            'ids' => 'ga:' . $this->counterId,
            'metrics' => 'ym:s:users',
            'filters' => $this->buildFilters(),
            'accuracy' => '0.4'
        ];

        $requestUrl = $this->url . 'analytics/v3/data/ga?' . $this->buildUrlParams($urlParams);

        return $this->getResult($this->request($requestUrl));
    }

    /**
     * @return int
     */
    public function getGoal()
    {
        $urlParams = [
            'oauth_token' => $this->token,
            'start-date' => $this->startDate,
            'end-date' => $this->endDate,
            'ids' => 'ga:' . $this->counterId,
            'metrics' => 'ga:goal' . $this->goalId . 'Completions',
            'filters' => $this->buildFilters(),
            'accuracy' => '0.4'
        ];

        $requestUrl = $this->url . 'analytics/v3/data/ga?' . $this->buildUrlParams($urlParams);

        return $this->getResult($this->request($requestUrl));
    }

    /**
     * @param string $url
     * @return mixed|null
     */
    protected function request(string $url)
    {
        try {
            sleep(2);
            $client = new Client();
            $response = $client->request('GET', $url);

            $result = json_decode($response->getBody(), true);
        } catch (ClientException $exception) {
            $result = $this->handleRequestError($exception, $url);
        } catch (ServerException $exception) {
            $result = $this->handleRequestError($exception, $url);
        }

        return $result;
    }

    /**
     * @param BadResponseException $exception
     * @param string $url
     * @return mixed|null
     */
    private function handleRequestError(BadResponseException $exception, string $url)
    {
        $response = $exception->getResponse();
        $responseBodyAsString = $response->getBody()->getContents();

        \Log::error('Yandex Metrika: ' . $exception->getMessage() . '; Body: ' . $responseBodyAsString);

        if (
            $exception->getCode() === Response::HTTP_BAD_REQUEST ||
            $exception->getCode() === Response::HTTP_INTERNAL_SERVER_ERROR ||
            $exception->getCode() === Response::HTTP_GATEWAY_TIMEOUT
        ) {
            \Log::warning('Yandex Metrika: Retry; Body: ' . $responseBodyAsString);

            sleep(3);
            return $this->request($url);
        }

        $result = null;
    }

    /**
     * @param Carbon $startDate
     * @return YandexMetrica
     */
    public function setStartDate(Carbon $startDate): self
    {
        $now = new Carbon();
        $this->startDate = ($now->diffInDays($startDate) !== 0) ? $now->diffInDays($startDate) . 'daysAgo' : 'today';

        return $this;
    }

    /**
     * @param Carbon|null $endDate
     * @return YandexMetrica
     */
    public function setEndDate(Carbon $endDate = null): self
    {
        if ($endDate === null) {
            $this->endDate = 'today';
        } else {
            $now = new Carbon();
            $this->endDate = ($now->diffInDays($endDate) !== 0) ? $now->diffInDays($endDate) . 'daysAgo' : 'today';
        }

        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @return YandexMetrica
     */
    public function setPanelUtm(string $name, string $value): self
    {
        $this->panelUtmName = $this->buildUtmName($name);
        $this->panelUtmValue = $value;

        return $this;
    }

    /**
     * @param string $name
     * @return YandexMetrica
     */
    public function setWmUtm(string $name): self
    {
        $this->wmUtmName = $this->buildUtmName($name);

        return $this;
    }

    /**
     * @param int $goalId
     * @return YandexMetrica
     */
    public function setGoal(int $goalId): self
    {
        $this->goalId = $goalId;

        return $this;
    }

    /**
     * @param int $mainGoalId
     * @return YandexMetrica
     */
    public function setMainGoal(int $mainGoalId): self
    {
        $this->mainGoalId = $mainGoalId;

        return $this;
    }

    /**
     * @param int $userId
     * @return YandexMetrica
     */
    public function setUser(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @param array $data
     * @return YandexMetrica
     */
    public function setExtend(array $data): self
    {
        $this->extend = $data;

        return $this;
    }

    /**
     * @param string $dimensions
     * @return YandexMetrica
     */
    public function setDimensions(string $dimensions): self
    {
        $this->dimensions = $this->buildUtmName($dimensions);

        return $this;
    }

    /**
     * @param string $token
     * @return YandexMetrica
     */
    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        $token = ($this->token) ? $this->token : env('YANDEX_METRICA_TOKEN', $this->defaultToken);

        return $token;
    }

    /**
     * @param string $name
     * @return string
     */
    private function buildUtmName(string $name): string
    {
        if ($name === 'utm_source') {
            return 'UTMSource';
        } elseif ($name === 'utm_campaign') {
            return 'UTMCampaign';
        } elseif ($name === 'utm_medium') {
            return 'UTMMedium';
        } elseif ($name === 'utm_content') {
            return 'UTMContent';
        } elseif ($name === 'utm_term') {
            return 'UTMTerm';
        }

        return '';
    }

    /**
     * @param array $params
     * @return string
     */
    private function buildUrlParams(array $params): string
    {
        return urldecode(http_build_query($params));
    }

    /**
     * @param array $data
     * @return int
     * @throws \Exception
     */
    private function getResult(array $data = null): int
    {
        if (is_null($data)) {
            return 0;
        }

        if (!isset($data['rows'])) {
            throw new \Exception('Результат отсутствует');
        }

        return (!empty($data['rows']) ? $data['rows'][0][0] : 0);
    }

    /**
     * @param array|null $data
     * @return array
     * @throws \Exception
     */
    private function getTotalResult(array $data = null): array
    {
        if (is_null($data)) {
            return [];
        }

        if (!isset($data['data'])) {
            throw new \Exception('Результат отсутствует');
        }

        return (!empty($data['data']) ? $data['data'] : []);
    }

    /**
     * @return string
     */
    private function buildFilters()
    {
        $string = 'ym:pv:' . $this->panelUtmName . '==\'' . $this->panelUtmValue . '\'';

        if ($this->userId) {
            $string .= ';ym:pv:' . $this->wmUtmName . '==\'wm_' . $this->userId . '\'';
        }

        if (!empty($this->extend)) {
            $string .= ';ym:pv:' . $this->buildUtmName($this->extend['name']) . '==\'' . $this->extend['value'] . '\'';
        }

        return $string;
    }
}