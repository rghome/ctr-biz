<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 4/24/17
 * Time: 10:04 AM
 */

namespace App\Services;

use App\Models\Offer;
use Illuminate\Support\Facades\Auth;

/**
 * Class ShortcodeParser
 * @package App\Services
 */
class ShortcodeParser
{
    /**
     * @var ShortcodeParser
     */
    private static $instance;

    /**
     * Shortcodes
     */
    const SHORTCODE_USERNAME = '{{username}}';
    const SHORTCODE_USERID = '{{user_id}}';
    const SHORTCODE_OPTION = '{{option}}';
    const SHORTCODE_OFFERNAME = '{{offername}}';
    const SHORTCODE_OFFERID = '{{offer_id}}';

    /**
     * @var array
     */
    private $values;

    /**
     * ShortcodeParser constructor.
     */
    public function __construct()
    {
        $this->fillUser();
    }

    /**
     * @return ShortcodeParser
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $str
     * @return string
     */
    public function simple($str)
    {
        return strtr($str, $this->values);
    }

    /**
     * @param string $str
     * @param Offer $offer
     * @return string
     */
    public function offer($str, Offer $offer)
    {
        $this->fillOffer($offer);

        return strtr($str, $this->values);
    }

    /**
     * @param $str
     * @return array|bool|int
     */
    public function checkOption($str)
    {
        $option = strpos($str, self::SHORTCODE_OPTION);

        return ($option) ? explode(self::SHORTCODE_OPTION, $str) : $option;
    }

    /**
     *
     */
    private function fillUser()
    {
        $this->values[self::SHORTCODE_USERID] = Auth::user()->id;
        $this->values[self::SHORTCODE_USERNAME] = Auth::user()->login;
    }

    /**
     * @param Offer $offer
     */
    private function fillOffer(Offer $offer)
    {
        $this->values[self::SHORTCODE_OFFERID] = $offer->id;
        $this->values[self::SHORTCODE_OFFERNAME] = $offer->name;
    }

}