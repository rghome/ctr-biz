<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 4/26/17
 * Time: 10:01 AM
 */

namespace App\Services;


use App\Models\Landing;
use App\Models\LandingPrice;
use App\Models\LandingUtm;
use App\Models\Token;
use App\Models\TokenOffers;
use App\User;
use App\Models\Offer;
use App\Models\OfferPrice;
use App\Models\OfferUtm;
use App\Models\StatisticCache;
use App\Models\UserUtm;

use App\Repository\PaymentRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use MongoDB\Driver\Cursor;
use MongoDB\Model\BSONDocument;

/**
 * Class CpaAdapter
 * @package App\Services
 */
class CpaAdapter
{
    /**
     * @var array
     */
    private $paymentsCache = [];

    /**
     * @param int|null $userId
     * @param string|null $startDate
     * @param string|null $endDate
     * @return Collection
     */
    public static function getTotalByDays(int $userId = null, string $startDate = null, string $endDate = null): Collection
    {
        if ($userId === null && ($startDate !== null && $endDate !== null)) {
            return self::getTotalByDateRange($startDate, $endDate);
        }

        if ($userId === null) {
            $cache = StatisticCache::where('days', 'exists', true);
        } else {
            $cache = StatisticCache::where('days', 'exists', true)->where('user', $userId);
        }

        return ($cache->get()) ?: new Collection();
    }

    /**
     * @param Offer $offer
     * @return array
     */
    public static function getTotalMetrikaByOffer(Offer $offer): array
    {
        $panelUtmValue = OfferUtm::where('offer_id', '=', $offer->id)
            ->where('name', $offer->metrika_utm_panel)
            ->first()
        ;

        $yandexMetrica = new YandexMetrica($offer->metrika_counter_id);
        $yandexMetrica->setGoal($offer->metrika_goal_activation_id)
            ->setDimensions($offer->metrika_utm_wm)
            ->setPanelUtm($offer->metrika_utm_panel, $panelUtmValue->value)
            ->setStartDate($offer->created_at)
            ->setEndDate()
        ;

        $data = $yandexMetrica->getByOffer();

        return self::prepareByWm($data);
    }

    /**
     * @param Offer $offer
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    public static function getTotalMetrikaByOfferAndDays(Offer $offer, Carbon $startDate, Carbon $endDate): array
    {
        $panelUtmValue = OfferUtm::where('offer_id', '=', $offer->id)
            ->where('name', $offer->metrika_utm_panel)
            ->first()
        ;

        $tokenRow = TokenOffers::where('offer_id', $offer->id)->with('token')->firstOrFail();

        $yandexMetrica = new YandexMetrica($offer->metrika_counter_id);
        $yandexMetrica
            ->setToken($tokenRow->token->token)
            ->setGoal($offer->metrika_goal_activation_id)
            ->setDimensions($offer->metrika_utm_wm)
            ->setPanelUtm($offer->metrika_utm_panel, $panelUtmValue->value)
            ->setStartDate($startDate)
            ->setEndDate($endDate)
        ;

        $data = $yandexMetrica->getByOffer();

        self::incrementToken($tokenRow->token);

        return self::prepareByWm($data);
    }

    /**
     * @param Token $token
     */
    private static function incrementToken(Token $token)
    {
        $currentTimestamp = (new Carbon())->setTime(0, 0)->getTimestamp();

        if ($token->time_flag !== $currentTimestamp) {
            $token->time_flag = $currentTimestamp;
            $token->count = 0;
            $token->save();
        }

        $query = Token::where('token', $token->token);

        $query->increment('count');

        echo 'Token count: ' . $query->first()->count . "\n";
    }

    /**
     * @param Landing $landing
     * @return array
     */
    public static function getTotalMetrikaByLanding(Landing $landing): array
    {
        $panelUtmValue = LandingUtm::where('landing_id', '=', $landing->id)
            ->where('name', $landing->metrika_utm_panel)
            ->first()
        ;

        $yandexMetrica = new YandexMetrica($landing->metrika_counter_id);
        $yandexMetrica->setGoal($landing->metrika_goal_activation_id)
            ->setMainGoal($landing->metrika_goal_main_id)
            ->setDimensions($landing->metrika_utm_wm)
            ->setPanelUtm($landing->metrika_utm_panel, $panelUtmValue->value)
            ->setStartDate($landing->created_at)
            ->setEndDate()
        ;

        $data = $yandexMetrica->getByLanding();

        return self::prepareByWm($data);
    }

    /**
     * @param Landing $landing
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    public static function getTotalMetrikaByLandingAndDays(Landing $landing, Carbon $startDate, Carbon $endDate): array
    {
        $panelUtmValue = LandingUtm::where('landing_id', '=', $landing->id)
            ->where('name', $landing->metrika_utm_panel)
            ->first()
        ;

        $tokenRow = TokenOffers::where('offer_id', $landing->offer_id)->with('token')->firstOrFail();

        $yandexMetrica = new YandexMetrica($landing->metrika_counter_id);
        $yandexMetrica
            ->setToken($tokenRow->token->token)
            ->setGoal($landing->metrika_goal_activation_id)
            ->setMainGoal($landing->metrika_goal_main_id)
            ->setDimensions($landing->metrika_utm_wm)
            ->setPanelUtm($landing->metrika_utm_panel, $panelUtmValue->value)
            ->setStartDate($startDate)
            ->setEndDate($endDate)
        ;

        $data = $yandexMetrica->getByLanding();

        self::incrementToken($tokenRow->token);

        return self::prepareByWm($data);
    }

    /**
     * @param Landing $landing
     * @param string $utm
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    public static function getTotalUtmByLandingAndDays(Landing $landing, string $utm, Carbon $startDate, Carbon $endDate): array
    {
        $tokenRow = TokenOffers::where('offer_id', $landing->offer_id)->with('token')->firstOrFail();

        $yandexMetrica = new YandexMetrica($landing->metrika_counter_id);
        $yandexMetrica
            ->setToken($tokenRow->token->token)
            ->setGoal($landing->metrika_goal_activation_id)
            ->setMainGoal($landing->metrika_goal_main_id)
            ->setDimensions($utm)
            ->setWmUtm($landing->metrika_utm_wm)
            ->setStartDate($startDate)
            ->setEndDate($endDate)
        ;

        $data = $yandexMetrica->getUtmByLanding();

        self::incrementToken($tokenRow->token);

        return self::prepareByUtm($data);
    }

    /**
     * @param Landing $landing
     * @param string $utm
     * @return array
     */
    public static function getTotalUtmByLanding(Landing $landing, string $utm): array
    {
        $yandexMetrica = new YandexMetrica($landing->metrika_counter_id);
        $yandexMetrica->setGoal($landing->metrika_goal_activation_id)
            ->setMainGoal($landing->metrika_goal_main_id)
            ->setDimensions($utm)
            ->setWmUtm($landing->metrika_utm_wm)
            ->setStartDate($landing->created_at)
            ->setEndDate()
        ;

        $data = $yandexMetrica->getUtmByLanding();

        return self::prepareByUtm($data);
    }

    /**
     * @param Offer $offer
     * @param string $utm
     * @param Carbon $startDate
     * @param Carbon $endDate
     * @return array
     */
    public static function getTotalUtmByOfferAndDays(Offer $offer, string $utm, Carbon $startDate, Carbon $endDate): array
    {
        $tokenRow = TokenOffers::where('offer_id', $offer->id)->with('token')->firstOrFail();

        $yandexMetrica = new YandexMetrica($offer->metrika_counter_id);
        $yandexMetrica
            ->setToken($tokenRow->token->token)
            ->setGoal($offer->metrika_goal_activation_id)
            ->setDimensions($utm)
            ->setWmUtm($offer->metrika_utm_wm)
            ->setStartDate($startDate)
            ->setEndDate($endDate)
        ;

        $data = $yandexMetrica->getUtmByOffer();

        self::incrementToken($tokenRow->token);

        return self::prepareByUtm($data);
    }

    /**
     * @param Offer $offer
     * @param string $utm
     * @return array
     */
    public static function getTotalUtmByOffer(Offer $offer, string $utm): array
    {
        $yandexMetrica = new YandexMetrica($offer->metrika_counter_id);
        $yandexMetrica->setGoal($offer->metrika_goal_activation_id)
            ->setDimensions($utm)
            ->setWmUtm($offer->metrika_utm_wm)
            ->setStartDate($offer->created_at)
            ->setEndDate()
        ;

        $data = $yandexMetrica->getUtmByOffer();

        return self::prepareByUtm($data);
    }

    /**
     * @param Offer $offer
     * @param int $userId
     * @param string $utm
     * @return array
     */
    public static function getTotalUtmByUser(Offer $offer, int $userId, string $utm): array
    {
        $yandexMetrica = new YandexMetrica($offer->metrika_counter_id);
        $yandexMetrica->setGoal($offer->metrika_goal_activation_id)
            ->setDimensions($utm)
            ->setWmUtm($offer->metrika_utm_wm)
            ->setStartDate($offer->created_at)
            ->setEndDate()
        ;

        $data = $yandexMetrica->getUtmByOffer();

        if (empty($data)) {
            return [];
        }

        $tmp = [];
        foreach ($data as $row) {
            $userUtm = UserUtm::where('user_id', $userId)
                ->where('offer_id', $offer->id)
                ->where('name', $utm)
                ->where('value', $row['dimensions'][0]['name'])
                ->first()
            ;

            if ($userUtm) {
                $tmp[$row['dimensions'][0]['name']] = [
                    $row['metrics'][1], $row['metrics'][2], $row['metrics'][0]
                ];
            }
        }

        return $tmp;
    }

    /**
     * @param Offer $offer
     * @return array
     */
    public static function getMetrikaByOffer(Offer $offer): array
    {
        $panelUtmValue = OfferUtm::where('offer_id', '=', $offer->id)
            ->where('name', $offer->metrika_utm_panel)
            ->first()
        ;

        $yandexMetrica = new YandexMetrica($offer->metrika_counter_id);
        $yandexMetrica
            ->setStartDate($offer->created_at)
            ->setEndDate()
            ->setPanelUtm($offer->metrika_utm_panel, $panelUtmValue->value)
        ;

        $totalUsers = $yandexMetrica->getTotalUsers();
        $newUsers = $yandexMetrica->getNewUsers();
        $goals = $yandexMetrica->setGoal($offer->metrika_goal_activation_id)->getGoal();

        return [$totalUsers, $newUsers, $goals];
    }

    /**
     * @param Offer $offer
     * @param int $userId
     * @return StatisticCache
     */
    public static function getCacheByUserAndOffer(Offer $offer, int $userId): StatisticCache
    {
        return (StatisticCache::where('user', $userId)
            ->where('offers.' . $offer->id, 'exists', true)
            ->first()) ?: new StatisticCache()
        ;
    }

    /**
     * @param Landing $landing
     * @param int $userId
     * @return StatisticCache
     */
    public static function getCacheByUserAndLanding(Landing $landing, int $userId): StatisticCache
    {
        return (StatisticCache::where('user', $userId)
            ->where('offers.' . $landing->offer->id . '.landings.' . $landing->id, 'exists', true)
            ->first()) ?: new StatisticCache()
        ;
    }

    /**
     * @param int $userId
     * @param string $startDate
     * @param string $endDate
     * @return StatisticCache
     */
    public static function getCacheByUserAndDays(int $userId, string $startDate, string $endDate): StatisticCache
    {
        if ($startDate && $endDate) {
            $result = self::getByDateRange($userId, $startDate, $endDate);
        } else {
            $result = (StatisticCache::where('user', $userId)
                ->where('days', 'exists', true)->first()) ?: new StatisticCache()
            ;
        }

        return $result;
    }

    /**
     * @param Offer $offer
     * @param int $userId
     * @return array
     */
    public static function getMetrikaByUserAndOffer(Offer $offer, int $userId): array
    {
        $panelUtmValue = OfferUtm::where('offer_id', '=', $offer->id)
            ->where('name', $offer->metrika_utm_panel)
            ->first()
        ;

        $yandexMetrica = new YandexMetrica($offer->metrika_counter_id);
        $yandexMetrica
            ->setStartDate($offer->created_at)
            ->setEndDate()
            ->setPanelUtm($offer->metrika_utm_panel, $panelUtmValue->value)
            ->setWmUtm($offer->metrika_utm_wm)
            ->setUser($userId)
        ;

        $totalUsers = $yandexMetrica->getTotalUsers();
        $newUsers = ($totalUsers > 0) ? $yandexMetrica->getNewUsers() : 0;
        $goals = ($totalUsers > 0) ? $yandexMetrica->setGoal($offer->metrika_goal_activation_id)->getGoal() : 0;

        return [$totalUsers, $newUsers, $goals];
    }

    /**
     * @param UserUtm $userUtm
     * @return StatisticCache
     */
    public static function getCacheByUserAndUtm(UserUtm $userUtm): StatisticCache
    {
        return (StatisticCache::where('user', $userUtm->user_id)
            ->where('offers.' . $userUtm->offer_id, 'exists', true)
            ->where('offers.' . $userUtm->offer_id . '.utm.' . $userUtm->name, 'exists', true)
            ->where('offers.' . $userUtm->offer_id . '.utm.' . $userUtm->name . '.' . $userUtm->value, 'exists', true)
            ->first()) ?: new StatisticCache()
        ;
    }

    /**
     * @param UserUtm $userUtm
     * @return array
     */
    public static function getMetrikaByUserAndUtm(UserUtm $userUtm): array
    {
        $panelUtmValue = OfferUtm::where('offer_id', '=', $userUtm->offer_id)
            ->where('name', $userUtm->offer->metrika_utm_panel)
            ->first()
        ;

        $yandexMetrica = new YandexMetrica($userUtm->offer->metrika_counter_id);
        $yandexMetrica
            ->setStartDate($userUtm->offer->created_at)
            ->setEndDate()
            ->setPanelUtm($userUtm->offer->metrika_utm_panel, $panelUtmValue->value)
            ->setWmUtm($userUtm->offer->metrika_utm_wm)
            ->setUser($userUtm->user_id)
        ;

        if (
            $userUtm->name !== $userUtm->offer->metrika_utm_panel &&
            $userUtm->name !== $userUtm->offer->metrika_utm_wm
        ) {
            $yandexMetrica->setExtend([
                'name' => $userUtm->name,
                'value' => $userUtm->value
            ]);
        }

        $totalUsers = $yandexMetrica->getTotalUsers();
        $newUsers = $yandexMetrica->getNewUsers();
        $goals = $yandexMetrica->setGoal($userUtm->offer->metrika_goal_activation_id)->getGoal();

        return [$totalUsers, $newUsers, $goals];
    }

    /**
     * @return int
     */
    public function getTotalFreeWrapper()
    {
        $total = $this->getTotalWrapper('available');

        $totalUser = (new PaymentRepository())->getTotalByUser();

        return $total - $totalUser;
    }

    /**
     * @return int
     */
    public function getTotalHoldWrapper()
    {
        return $this->getTotalWrapper('hold') + $this->getTotalFreeWrapper();
    }

    /**
     * @param string $type
     * @return int
     */
    private function getTotalWrapper(string $type)
    {
        $user = Auth::user()->id;

        $total = 0;
        $totalCache = CpaAdapter::getTotalByDays($user);
        foreach ($totalCache as $rows) {
            foreach ($rows['days'] as $day => $items) {
                if (isset($items['landings'])) {
                    foreach ($items['landings'] as $landingId => $data) {
                        $total += (int) (isset($data['price'][$type]) ? $data['price'][$type] : 0);
                    }
                }

                if (isset($items['offers'])) {
                    foreach ($items['offers'] as $offerId => $data) {
                        $total += (int) (isset($data['price'][$type]) ? $data['price'][$type] : 0);
                    }
                }
            }
        }

        return $total;
    }

    /**
     * @param int $goals
     * @param Offer|null $offer
     * @param Landing|null $landing
     * @return int
     */
    public function getTotalFree(int $goals, Offer $offer = null, Landing $landing = null): int
    {
        if ($goals === 0) {
            return 0;
        }

        if ($landing === null) {
            $price = OfferPrice::where('offer_id', $offer->id)
                ->where('region_id', 1)
                ->first()
            ;
        } else {
            $price = LandingPrice::where('landing_id', $landing->id)
                ->where('region_id', 1)
                ->first()
            ;
        }

        return (int) $price->price * $goals;
    }

    /**
     * @param array $data
     * @return array
     */
    private static function prepareByWm(array $data = []): array
    {
        if (empty($data)) {
            return [];
        }

        $tmp = [];
        foreach ($data as $row) {
            preg_match("/wm_(.*)/", $row['dimensions'][0]['name'], $output_array);
            if (!isset($output_array[1])) continue;

            $user = $output_array[1];
            if (User::where('id', $user)->first()) {
                $tmp[$user] = [
                    $row['metrics'][1], $row['metrics'][2], $row['metrics'][0]
                ];
            }

        }

        return $tmp;
    }

    /**
     * @param array $data
     * @return array
     */
    private static function prepareByUtm(array $data = []): array
    {
        if (empty($data)) {
            return [];
        }

        $tmp = [];
        foreach ($data as $row) {
            $exploded = explode('__', $row['dimensions'][0]['name']);
            $user = $exploded[1];
            if (User::where('id', $user)->first()) {
                $tmp[$user][$exploded[0]] = [
                    'total' => (int) $row['metrics'][1],
                    'new' => (int) $row['metrics'][2],
                    'goal' => (int) $row['metrics'][0],
                ];
            }
        }

        return $tmp;
    }

    /**
     * @param string $date
     * @return int
     */
    private static function parseDate(string $date): int
    {
        return Carbon::parse($date)->getTimestamp();
    }

    /**
     * @param int $userId
     * @param string $startDate
     * @param string $endDate
     * @return StatisticCache
     */
    private static function getByDateRange(int $userId, string $startDate, string $endDate): StatisticCache
    {
        $map = 'function(){if(this.hasOwnProperty("days")){var a={};for(var b in this.days){var c=this.days[b];b>=' . self::parseDate($startDate) . '&&b<=' . self::parseDate($endDate) . '&&(a[b]=c)}Object.keys(a).length&&emit(parseInt(this.user),a)}};';

        /** @var \MongoDB $db */
        $db = \DB::connection('mongodb');
        /** @var BSONDocument $rows */
        $rows = $db->command([
            'mapreduce' => StatisticCache::getCollectionName(),
            'map' => new \MongoDB\BSON\Javascript($map),
            'reduce' => new \MongoDB\BSON\Javascript('function() {}'),
            'query' => [
                'user' => $userId
            ],
            'out' => [
                'inline' => 1
            ]
        ])->toArray()[0];
        $rows = (isset($rows['results'][0]) ? $rows['results'][0] : []);

        $new = new StatisticCache();
        foreach ($rows as $key => $value) {
            if ($key !== 'value') continue;

            $new->user = $userId;
            $new->days = \GuzzleHttp\json_decode(\GuzzleHttp\json_encode($value->getArrayCopy()), true);
        }

        return $new;
    }

    private static function getTotalByDateRange(string $startDate, string $endDate): Collection
    {
        $map = 'function(){if(this.hasOwnProperty("days")){var a={};for(var b in this.days){var c=this.days[b];b>=' . self::parseDate($startDate) . '&&b<=' . self::parseDate($endDate) . '&&(a[b]=c)}Object.keys(a).length&&emit(parseInt(this.user),a)}};';

        /** @var \MongoDB $db */
        $db = \DB::connection('mongodb');
        /** @var BSONDocument $rows */
        $rows = $db->command([
            'mapreduce' => StatisticCache::getCollectionName(),
            'map' => new \MongoDB\BSON\Javascript($map),
            'reduce' => new \MongoDB\BSON\Javascript('function() {}'),
            'out' => [
                'inline' => 1
            ]
        ])->toArray()[0];


        $rows = (isset($rows['results']) ? $rows['results'] : []);

        $collection = new Collection();

        foreach ($rows as $row) {
            foreach ($row as $key => $value) {
                if ($key !== 'value') continue;

                $new = new StatisticCache();
                $new->days = \GuzzleHttp\json_decode(\GuzzleHttp\json_encode($value->getArrayCopy()), true);

                $collection->add($new);
            }
        }

        return $collection;
    }
}