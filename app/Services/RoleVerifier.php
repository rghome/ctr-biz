<?php

namespace App\Services;


use Illuminate\Support\Facades\Auth;

class RoleVerifier
{
	/**
	 * Role name who can do anything
	 *
	 * @array
	 */
	private static $ROLE_ALL_PERMISSION = ['Administrator', 'Monitoring'];

	/**
	 * Access to main menu
	 *
	 * @var array
	 */
	private static $accessMainMenu = [
		'User' => [
	//		'statistic',
			'faces',
	//		'materials',
	//		'data',
		],
	];

	/**
	 * Array permissions
	 *
	 * @var array
	 */
	private static $accesPermissions = [
		'User' => [
		    'index' => [
		        'index'
            ],
            'news' => [
                'index'
            ],
            'statistic' => [
                'index',
                'search'
            ],
            'offer' => [
                'index',
                'select',
                'createredirectlink'
            ],
            'domain' => [
                'index',
                'create'
            ],
            'user' => [
                'profile',
                'registerbyinvite',
                'readnotification'
            ],
            'payment' => [
                'index',
                'createwithdrawal',
                'deletewithdrawal'
            ]
		],
        'guest' => [
            'user' => [
                'registerbyinvite'
            ],
        ]
	];

	/**
	 * Get role id
	 *
	 * @return integer
	 */
	public static function getRoleId()
	{
		return Auth::user()->role_id;
	}

	/**
	 * Get role name
	 *
	 * @return string
	 */
	public static function getRoleName()
	{
		return (Auth::user()) ? Auth::user()->role->name : 'guest';
	}

	/**
	 * Check user permission
	 *
	 * @param $data
	 * @return bool
	 */
	public static function can($data)
	{
		list($category, $method) = explode('.', $data);
		$roleName = self::getRoleName();

		if(in_array($roleName, self::$ROLE_ALL_PERMISSION)) {
			return true;
		}

		if(!array_key_exists($category, self::$accesPermissions[$roleName])) {
			return false;
		}

		if(!array_key_exists($roleName, self::$accesPermissions)) {
			throw new \InvalidArgumentException('No current role in database');
		}

		return in_array(strtolower($method), self::$accesPermissions[$roleName][$category]);
	}

	/**
	 * View main menu
	 *
	 * @param $data
	 * @return bool
	 */
	public static function canViewMenu($data)
	{
		$roleName = self::getRoleName();

		if(in_array($roleName, self::$ROLE_ALL_PERMISSION)) {
			return true;
		}

		return in_array($data, self::$accessMainMenu[$roleName]);
	}
}