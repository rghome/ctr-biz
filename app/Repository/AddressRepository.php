<?php
namespace App\Repository;

use App\Models\Address;
use App\Models\Street;

/**
 * Class AddressRepository
 * @package App\Repository
 */
class AddressRepository extends Repository
{
    /**
     * @param bool $onlyActive
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getList($onlyActive = FALSE)
	{
		return Address::all();
	}

    /**
     * @param $data
     *
     * @return array|bool
     */
    public function create($data)
	{
	    if ($addressMatch = $this->checkMatches($data)) {
	        return $addressMatch;
        }

		$address = new Address();
        $address = $this->passFields($address, $data);

		$address->save();

        return $address;
	}

    /**
     * @param $id
     * @param $data
     *
     * @return array|bool
     */
    public function update($id, $data)
	{
        if ($addressMatch = $this->checkMatches($data)) {
            return $addressMatch;
        }

        $address = Address::find($id);
        $address = $this->passFields($address, $data);

        $address->save();
	}

    /**
     * @param $address
     * @param array $data
     *
     * @return mixed
     */
	private function passFields($address, array $data)
    {
        $address->city_id = $data['city_id'];
        $address->house = $data['house'];
        $address->street_id = $data['street'];

        return $address;
    }


    /**
     * @param array $data
     *
     * @return array|bool
     */
    private function checkMatches(array $data)
    {
        $addressMatch = Address::where('city_id', (int) $data['city_id'])
           ->where('house', $data['house'])
           ->count()
        ;

        $streetMatch = Street::where('id', $data['street'])->count();

        if ($addressMatch && $streetMatch) {
            return [
                'address_match' => 'Такой адрес уже используется'
            ];
        }

        return false;
    }


}