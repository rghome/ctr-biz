<?php
namespace App\Repository;

use App\Models\Payment;
use App\User;
use Illuminate\Support\Facades\Auth;

/**
 * Class PaymentRepository
 * @package App\Repository
 */
class PaymentRepository extends Repository
{
    /**
     * @var int
     */
    protected $rows;

    /**
     * PaymentRepository constructor.
     * @param int $rows
     */
    public function __construct($rows = 10)
    {
        $this->rows = $rows;
    }

    /**
     * @param null $user
     * @return int
     */
    public function getTotalByUser($user = null)
    {
        if ($user === null) {
            $user = Auth::user();
        }

        $row = Payment::selectRaw('SUM(total) as total')
            ->where('user_id', $user->id)
            ->where('status', '!=', Payment::STATUS_CANCEL)
            ->first()
        ;

        return $row->total ? $row->total : 0;
    }

    /**
     * @param User|null $user
     * @return int
     */
    public function getTotalWaitingByUser(User $user = null)
    {
        if ($user === null) {
            $user = Auth::user();
        }

        $row = Payment::selectRaw('SUM(total) as total')
            ->where('user_id', $user->id)
            ->where('status', '=', '1')
            ->first()
        ;

        return $row->total ? $row->total : 0;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return Payment::with('user')
//            ->where('status', '=', '1')
            ->orderBy('status', 'ASC')
            ->paginate($this->rows)
        ;
    }

    /**
     * @return mixed
     */
    public function getCountWaiting()
    {
        return Payment::with('user')
            ->where('status', '=', '1')
            ->count()
        ;
    }

}