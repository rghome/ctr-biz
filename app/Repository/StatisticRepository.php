<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/16/17
 * Time: 1:46 PM
 */

namespace App\Repository;

use App\Models\Offer;
use App\Models\PaymentLog;
use App\Models\StatisticLog;
use Illuminate\Support\Facades\Auth;
use MongoDB\BSON\UTCDateTime;
use MongoDB\Model\BSONDocument;

/**
 * Class StatisticRepository
 * @package App\Repository
 */
class StatisticRepository extends Repository
{
    /**
     * @param null $user
     * @param array $filter
     */
    public function getByDays($user = null, $filter = [])
    {
        $range = $this->dateRange('day');

        $match = $this->match($user, $filter);
        $match['date'] = [
            '$gte' => new UTCDateTime($range['start']*1000),
            '$lt' => new UTCDateTime($range['end']*1000)
        ];

        /**
         * @var \Illuminate\Database\Eloquent\Collection $raw
         */
//        $raw = StatisticLog::raw(function($collection) use ($match) {
//            return $collection->aggregate([
//                [
//                    '$match' => $match
//                ],
//                [
//                    '$project' => $this->getUtmProject()
//                ]
//            ]);
//        });

//        fwrite(fopen('/tmp/dump', 'w'), print_r([$match, $raw->toArray()], 1));

//        [
//            '$gte' => [
//                '$$el' . $page . '.date', new UTCDateTime($range['start']*1000)
//            ],
//        ],
//        [
//            '$lt' => [
//                '$$el' . $page . '.date', new UTCDateTime($range['end']*1000)
//            ]
//        ]
    }

    /**
     * @param null $user
     * @param array $filter
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getByOffer($user = null, $filter = [])
    {
        /**
         * @var \Illuminate\Database\Eloquent\Collection $raw
         */
        $raw = StatisticLog::raw(function($collection) use ($user, $filter) {
            if ($user) {
                $aggregate[] = [
                    '$match' => $this->match($user, $filter),
                ];
            }

            $aggregate[] = [
                '$group' => [
                    '_id' => '$offer_id',
                    'tmp' => [
                        '$push' => [
                            'page' => '$page',
                            'ip' => '$ip.ip',
                            'offer_id' => '$offer_id',
                            'payment_status' => '$payment_status',
                            'date' => '$date',
                        ]
                    ]
                ]
            ];

            $aggregate[] = [
                '$project' => [
                    '_id' => true,
                    'tmp' => true,
                    'page1_count' => [
                        '$size' => [
                            '$filter' => [
                                'input' => '$tmp',
                                'as' => 'el1',
                                'cond' => [
                                    '$eq' => ['$$el1.page', 1]
                                ]
                            ]
                        ]
                    ]
                ]
            ];

            $aggregate[] = [
                '$unwind' => '$tmp'
            ];

            $aggregate[] = [
                '$group' => [
                    '_id' => [
                        'offer_id' => '$_id',
                        'page1_count' => '$page1_count'
                    ],
//                        'page3_array' => $this->getAddToSetByPage(3, '$tmp'),
                    'page1_unique' => $this->getAddToSetByPage(1, '$tmp.ip'),
                    'page2_unique' => $this->getAddToSetByPage(2, '$tmp.ip'),
                    'page3_unique' => $this->getAddToSetByPage(3, '$tmp.ip'),
                ]
            ];

            $aggregate[] = [
                '$project' => [
                    '_id' => '$_id.offer_id',
                    'page1_count' => '$_id.page1_count',
                    'page1_unique' => $this->getSizeDifference(1),
                    'page2_unique' => $this->getSizeDifference(2),
                    'page3_unique' => $this->getSizeDifference(3),
//                        'page3_array' => [
//                            '$setDifference' => [
//                                '$page3_array', [false]
//                            ]
//                        ],
                ]
            ];

            $aggregate[] = [
                '$sort' => [
                    '_id' => -1
                ]
            ];

            return $collection->aggregate($aggregate);
        })->toArray();

        foreach ($raw as $key => $offer) {
            $offerRow = Offer::where('id', $offer['_id'])->first();
            $raw[$key]['_id'] = $offerRow->name;

            $raw[$key]['payment_hold'] = $this->getTotalHoldByOffer($user, $offerRow->id);
            $raw[$key]['payment_free'] = $this->getTotalFreeByOffer($user, $offerRow->id);
        }

        return $raw;
    }

    /**
     * @param null $user
     * @param array $filter
     * @param $utm
     * @return array
     */
    public function getByUtm($user = null, $filter = [], $utm)
    {
        $filter[$utm] = true;

        /**
         * @var \Illuminate\Database\Eloquent\Collection $raw
         */
        $raw = StatisticLog::raw(function($collection) use ($user, $filter, $utm) {
            return $collection->aggregate([
                [
                    '$match' => $this->match($user, $filter),
                ],
                [
                    '$group' => [
                        '_id' => '$' . $utm,
                        'tmp' => [
                            '$push' => [
                                'page' => '$page',
                                'ip' => '$ip.ip',
                                'offer_id' => '$offer_id',
                                'payment_status' => '$payment_status',
                                'date' => '$date',
                            ]
                        ]
                    ]
                ],
                [
                    '$project' => [
                        '_id' => true,
                        'tmp' => true,
                        'page1_count' => [
                            '$size' => [
                                '$filter' => [
                                    'input' => '$tmp',
                                    'as' => 'el1',
                                    'cond' => [
                                        '$eq' => ['$$el1.page', 1]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    '$unwind' => '$tmp'
                ],
                [
                    '$group' => [
                        '_id' => [
                            'utm' => '$_id',
                            'page1_count' => '$page1_count'
                        ],
//                        'page3_array' => $this->getAddToSetByPage(3, '$tmp'),
                        'page1_unique' => $this->getAddToSetByPage(1, '$tmp.ip'),
                        'page2_unique' => $this->getAddToSetByPage(2, '$tmp.ip'),
                        'page3_unique' => $this->getAddToSetByPage(3, '$tmp.ip'),
                    ]
                ],
                [
                    '$project' => [
                        '_id' => '$_id.utm',
                        'page1_count' => '$_id.page1_count',
                        'page1_unique' => $this->getSizeDifference(1),
                        'page2_unique' => $this->getSizeDifference(2),
                        'page3_unique' => $this->getSizeDifference(3),
//                        'page3_array' => [
//                            '$setDifference' => [
//                                '$page3_array', [false]
//                            ]
//                        ],
                    ]
                ],
                [
                    '$sort' => [
                        '_id' => -1
                    ]
                ]
            ]);
        });

        return $raw->toArray();
    }

    /**
     * @param int $page
     * @return array
     */
    private function getSizeDifference($page)
    {
        return [
            '$size' => [
                '$setDifference' => [
                    '$page' . $page . '_unique', [false]
                ]
            ]
        ];
    }

    /**
     * @param int $page
     * @param string $then
     * @return array
     */
    private function getAddToSetByPage($page, $then)
    {
        return [
            '$addToSet' => [
                '$cond' => [
                    [
                        '$eq' => ['$tmp.page', $page]
                    ],
                    $then,
                    false
                ]
            ]
        ];
    }

    /**
     * @param $array
     * @param string $type
     * @return int
     */
    private function getPayment($array, $type = 'hold')
    {
        $sum = 0;
        if (!is_array($array)) {
            return $sum;
        }

        $offersCache = [];
        foreach ($array as $row) {
            $row = (array) $row;

            if ($row['payment_status'] != $type) {
                continue;
            }

            if (!isset($offersCache[$row['offer_id']])) {
                $offersCache[$row['offer_id']] = Offer::with('prices')
                    ->where('id', $row['offer_id'])
                    ->first()
                ;
            }

            if (empty($offersCache[$row['offer_id']])) {
                continue;
            }

            $price = array_filter($offersCache[$row['offer_id']]->prices->toArray(), function($price) {
                return $price['region_id'] === 1;
            })[0];

            $sum += $price['price'];
        }

        return $sum;
    }

    /**
     * @param $user
     * @param array $filter
     * @return array
     */
    private function match($user = null, $filter = [])
    {
        $data = [
            'user_id'  => (int) $user
        ];

        if (isset($filter['by_offer']) && $filter['by_offer']) {
            $data['offer_id'] = (int) $filter['by_offer'];
        }

        $utm = [
            'utm_source' => true,
            'utm_medium' => true,
            'utm_content' => true,
            'utm_campaign' => true,
            'utm_term' => true,
        ];

        foreach ($utm as $key => $value) {
            if (isset($filter[$key]) && $filter[$key]) {
                $data[$key] = [
                    '$exists' => true,
                    '$ne' => null
                ];
            }
        }

        return $data;
    }

    /**
     * @param string $type
     * @return array
     */
    private function dateRange($type)
    {
        $dtNow = new \DateTime();
        $dtNow->setTimestamp(time());
        $begin = clone $dtNow;
        $end = clone $dtNow;

        if ($type === 'day') {
            $begin->modify('today');

            $end = clone $begin;
            $end->modify('tomorrow');

            $end->modify('1 second ago');
        } elseif ($type === 'week') {
            $begin
                ->modify('monday this week')
            ;

            $end = clone $begin;
            $end
                ->modify('sunday this week')
                ->modify('1 second ago')
            ;
        } elseif ($type === 'month') {
            $begin
                ->modify('00:00 first day of this month')
            ;

            $end = clone $begin;
            $end
                ->modify('23:59 last day of this month')
            ;
        }

        return [
            'start' => $begin->getTimestamp(),
            'end' => $end->getTimestamp()
        ];
    }

    /**
     * @param null $user
     * @return int
     */
    public function getTotalHold($user = null)
    {
        return $this->getTotal($user, 'hold');
    }

    /**
     * @param null $user
     * @return int
     */
    public function getTotalFree($user = null)
    {
        $totalUser = (new PaymentRepository())->getTotalByUser();

        return $this->getTotal($user, 'free') - $totalUser;
    }

    /**
     * @param null $user
     * @param $offer
     * @return int
     */
    public function getTotalFreeByOffer($user = null, $offer)
    {
        return $this->getTotal($user, 'free', $offer);
    }

    /**
     * @param null $user
     * @param $offer
     * @return int
     */
    public function getTotalHoldByOffer($user = null, $offer)
    {
        return $this->getTotal($user, 'hold', $offer);
    }

    /**
     * @param null $user
     * @param $type
     * @param null $offer
     * @return int
     */
    private function getTotal($user = null, $type, $offer = null)
    {
        if ($user === null) {
            $user = Auth::user()->id;
        }

        $match = [
            'user_id' => (int) $user,
            'payment_status' => $type,
            'page' => 3
        ];

        if (!is_null($offer)) {
            $match['offer_id'] = (int) $offer;
        }

        $rows = StatisticLog::raw(function($collection) use ($match) {
            return $collection->aggregate([
                [
                    '$match' => $match
                ],
                [
                    '$group' => [
                        '_id' => [
                            'offer_id' => '$offer_id',
                            'ip' => '$ip.ip'
                        ]
                    ]
                ],
                [
                    '$project' => [
                        '_id' => false,
                        'offer_id' => '$_id.offer_id',
                        'ip' => '$_id.ip'
                    ]
                ]
            ]);
        })->toArray();

        $total = 0;
        $offerCache = [];
        foreach ($rows as $row) {
            if (!isset($offerCache[$row['offer_id']])) {
                $offerCache[$row['offer_id']] = Offer::where('id', $row['offer_id'])->with('prices')->first();
            }

            if ($offerCache[$row['offer_id']]) {
                $price = array_filter($offerCache[$row['offer_id']]->prices->toArray(), function($price) {
                    return $price['region_id'] === 1;
                })[0]['price'];

                $total += $price;
            }
        }

        return $total;
    }

    /**
     * @return mixed
     */
    public function getTotalApproves()
    {
        return $this->getTotalByPage(3)[0]['count'];
    }

    /**
     * @return mixed
     */
    public function getTotalWaiting()
    {
        return $this->getTotalByPage(2)[0]['count'];
    }

    /**
     * @param int $page
     * @return mixed
     */
    public function getTotalByPage($page = 1)
    {
        return StatisticLog::raw(function($collection) use ($page) {
            return $collection->aggregate([
                [
                    '$match' => [
                        'page' => $page
                    ]
                ],
                [
                    '$group' => [
                        '_id' => [
                            'user_id' => '$user_id',
                            'offer_id' => '$offer_id',
                            'ip' => '$ip.ip'
                        ],
                        'count' => [
                            '$sum' => 1
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => false,
                        'count' => [
                            '$sum' => '$count'
                        ]
                    ]
                ],
            ]);
        })->toArray();
    }

    /**
     * @return mixed
     */
    public function getLastWeekApproves()
    {
        $dtNow = new \DateTime();
        $dtNow->setTimestamp(time());
        $begin = clone $dtNow;

        $begin->modify('monday this week');

        return StatisticLog::raw(function($collection) use ($begin) {
            return $collection->aggregate([
                [
                    '$match' => [
                        'page' => 3,
                        'date' => [
                            '$lt' => new UTCDateTime($begin->getTimestamp() * 1000)
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => [
                            'user_id' => '$user_id',
                            'offer_id' => '$offer_id',
                            'ip' => '$ip.ip'
                        ],
                        'count' => [
                            '$sum' => 1
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => false,
                        'count' => [
                            '$sum' => '$count'
                        ]
                    ]
                ],
            ]);
        })->toArray();
    }
}