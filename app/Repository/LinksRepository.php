<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 5/18/17
 * Time: 7:38 PM
 */

namespace App\Repository;


use App\Models\Links;

class LinksRepository extends Repository
{
    /**
     * @return string
     */
    public function getUniqueHash(): string
    {
        $hash = str_random(8);

        $row = Links::where('hash', $hash)->first();
        if (isset($row->hash)) {
            return $this->getUniqueHash();
        }

        return $hash;
    }
}