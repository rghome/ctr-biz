<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 4/26/17
 * Time: 1:32 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class UserUtm
 * @package App\Models
 */
class UserUtm extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'users_utm';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offer()
    {
        return $this->belongsTo('App\Models\Offer', 'offer_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}