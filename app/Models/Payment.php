<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/22/17
 * Time: 3:41 PM
 */

namespace App\Models;


use App\Role;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Invite
 * @package App\Models
 */
class Payment extends Model
{
    const STATUS_WAIT = 1;
    const STATUS_PAY = 2;
    const STATUS_CANCEL = 3;

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'payments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'total',
        'status',
        'output',
        'comment',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}