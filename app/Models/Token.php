<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/10/17
 * Time: 5:09 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Tokens
 * @package App\Models
 */
class Token extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'tokens';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function offers()
    {
        return $this->hasMany('App\Models\TokenOffers');
    }
}