<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 4/24/17
 * Time: 5:45 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OfferUtm extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'offer_utm';


}