<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/10/17
 * Time: 5:09 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Server
 * @package App\Models
 */
class Server extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'servers';

    /**
     * @var array
     */
    protected $fillable = [
        'server_id', 'ip'
    ];
}