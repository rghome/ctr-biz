<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 6/1/17
 * Time: 3:00 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class News
 * @package App\Models
 */
class News extends Model
{
    /**
     * @var string
     */
    protected $collection = 'news';
}