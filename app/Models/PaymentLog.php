<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/14/17
 * Time: 11:13 PM
 */

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class QueryLog
 * @package App\Models
 */
class PaymentLog extends Eloquent
{
    const status_type = [
        1 => 'hold',
        2 => 'free',
        3 => 'paid'
    ];

    /**
     * @var string
     */
    protected $collection = 'payment_log';

    /**
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * @param mixed $value
     */
    public function setUpdatedAt($value) {  }

    /**
     * @param $value
     */
    public function setUpdatedAtAttribute($value) { }
}