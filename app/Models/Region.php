<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/10/17
 * Time: 5:09 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Region
 * @package App\Models
 */
class Region extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'regions';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'currency'
    ];
}