<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/14/17
 * Time: 11:13 PM
 */

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class QueryLog
 * @package App\Models
 */
class StatisticLog2 extends Eloquent
{
    const pages = [
        1 => 'index',
        2 => 'activated',
        3 => 'thanks'
    ];

    /**
     * @var string
     */
    protected $collection = 'statistic_log2';

    /**
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * @param mixed $value
     */
    public function setUpdatedAt($value) {  }

    /**
     * @param $value
     */
    public function setUpdatedAtAttribute($value) { }
}