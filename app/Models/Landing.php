<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 5/9/17
 * Time: 2:15 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Landing extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'landings';

    public function offer()
    {
        return $this->belongsTo('App\Models\Offer', 'offer_id', 'id');
    }

    public function prices()
    {
        return $this->hasMany('App\Models\LandingPrice');
    }

    public function utm()
    {
        return $this->hasMany('App\Models\LandingUtm');
    }

    public function delete()
    {
        // delete all related rows
        $this->prices()->delete();
        $this->utm()->delete();

        // delete the user
        return parent::delete();
    }
}