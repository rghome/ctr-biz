<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/14/17
 * Time: 11:13 PM
 */

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class QueryLog
 * @package App\Models
 */
class StatisticCache extends Eloquent
{
    /**
     * @var string
     */
    protected $collection = 'statistic_cache';

    /**
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * @param mixed $value
     */
    public function setUpdatedAt($value) {  }

    /**
     * @param $value
     */
    public function setUpdatedAtAttribute($value) { }

    /**
     * @return mixed
     */
    public static function getCollectionName()
    {
        return with(new static)->getTable();
    }
}