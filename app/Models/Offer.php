<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/10/17
 * Time: 4:19 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Offer
 * @package App\Models
 */
class Offer extends Model
{
    public const STATUS_OPEN = 1;
    public const STATUS_HIDE = 2;

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'offers';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function server()
    {
        return $this->belongsTo('App\Models\Server', 'server_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany('App\Models\OfferPrice');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function utm()
    {
        return $this->hasMany('App\Models\OfferUtm');
    }

    /**
     * orderBy нужен для костыля ЛП, если нету main цели
     *
     * @return mixed
     */
    public function landings()
    {
        return $this->hasMany('App\Models\Landing')->orderBy('metrika_goal_main_id', 'desc');
    }

    /**
     * @return bool|null
     */
    public function delete()
    {
        // delete all related rows
        $this->prices()->delete();
        $this->utm()->delete();
        $this->server()->delete();
        $this->landings()->delete();

        return parent::delete();
    }
}