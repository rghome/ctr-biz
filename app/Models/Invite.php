<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/22/17
 * Time: 3:41 PM
 */

namespace App\Models;


use App\Role;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Invite
 * @package App\Models
 */
class Invite extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token',
        'name',
        'email',
        'accepted',
        'accepted_at',
    ];
}