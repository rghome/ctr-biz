<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 5/18/17
 * Time: 7:31 PM
 */

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class Links
 * @package App\Models
 */
class Links extends Eloquent
{
    /**
     * @var string
     */
    protected $collection = 'links';

    /**
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * @param mixed $value
     */
    public function setUpdatedAt($value) {  }

    /**
     * @param $value
     */
    public function setUpdatedAtAttribute($value) { }

    /**
     * @return mixed
     */
    public static function getCollectionName()
    {
        return with(new static)->getTable();
    }
}