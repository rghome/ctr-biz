<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 5/23/17
 * Time: 2:11 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class TokenOffers
 * @package App\Models
 */
class TokenOffers extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'token_offers';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function token()
    {
        return $this->belongsTo('App\Models\Token', 'token_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offer()
    {
        return $this->belongsTo('App\Models\Offer', 'offer_id', 'id');
    }
}