<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/10/17
 * Time: 6:04 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferPrice extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'offer_region_price';

    /**
     * @var array
     */
    protected $fillable = [
        'price'
    ];

    public function region()
    {
        return $this->belongsTo('App\Models\Region', 'region_id', 'id');
    }

    public function offer()
    {
        return $this->belongsTo('App\Models\Offer', 'offer_id', 'id');
    }
}