<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/10/17
 * Time: 4:19 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Domain
 * @package App\Models
 */
class Domain extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'domains';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'moderate'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}