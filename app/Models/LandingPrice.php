<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/10/17
 * Time: 6:04 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandingPrice extends Model
{
    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'landing_region_price';

    /**
     * @var array
     */
    protected $fillable = [
        'price'
    ];

    public function region()
    {
        return $this->belongsTo('App\Models\Region', 'region_id', 'id');
    }

    public function landing()
    {
        return $this->belongsTo('App\Models\Landing', 'landing_id', 'id');
    }
}