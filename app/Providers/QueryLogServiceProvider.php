<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Foundation\Application;

//use App\Models\Units;
//use App\Models\Program;
//use App\Models\Resource;
//use App\Models\Production;
//use App\Models\Contract;
//use App\Models\Product;

use App\Models\{
	Units, Program, Resource,
	Production, Contract, Product
};

use App\Models\QueryLog;

/**
 * Class QueryLogServiceProvider
 * @package App\Providers
 */
class QueryLogServiceProvider extends ServiceProvider
{
	/**
	 * Logged models list
	 *
	 * @var array
	 */
	private $_models;

	/**
	 * Must logged events list
	 *
	 * @var array
	 */
	private $_events;

	/**
	 * QueryLogServiceProvider constructor.
	 *
	 * @param Application $app
	 */
	public function __construct(Application $app)
	{
		parent::__construct($app);

		$this->_models = [
			Units::class,
			Program::class,
			Resource::class,
			Production::class,
			Contract::class,
			Product::class
		];

		$this->_events = [
			'updating', 'created'
		];
	}

	/**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    	foreach ($this->_models as $model) {
    		foreach ($this->_events as $event) {
				$this->registerEvents($model, $event);
		    }
	    }
    }

	/**
	 * @param $model
	 * @param $event
	 *
	 * @return mixed
	 */
    private function registerEvents($model, $event)
    {
    	return $model::$event(function ($model) use ($event) {
		    return $this->store($model, $event);
	    });
    }

	/**
	 * @param Model $model
	 * @param $event
	 */
    private function store(Model $model, $event)
    {
	    $queryLog = new QueryLog();

	    $queryLog->entity_id = (isset($model->id)) ? $model->id : null;
	    $queryLog->table = $model->getTable();
	    $queryLog->type = $event;
	    $queryLog->author = Auth::user()->id;

	    $this->fillActualAndOriginData($queryLog, $model, $event);

	    $queryLog->save();
    }

	/**
	 * @param QueryLog $queryLog
	 * @param Model $model
	 * @param $event
	 */
    private function fillActualAndOriginData(QueryLog &$queryLog, Model $model, $event)
    {
	    if ($event === 'updating') {
		    $this->setDifferenceWhenUpdated($queryLog, $model);
	    } elseif ($event === 'created') {
			$this->setActualWhenCreated($queryLog, $model);
	    }
    }

	/**
	 * @param QueryLog $queryLog
	 * @param Model $model
	 */
    private function setDifferenceWhenUpdated(QueryLog &$queryLog, Model $model)
    {
	    $origin = $model->getOriginal();
	    $actual = $model->getAttributes();

	    if ($diffed = array_diff($origin, $actual)) {
		    $originCache = [];
		    $actualCache = [];

		    foreach ($diffed as $key => $value) {
			    $originCache[$key] = $origin[$key];
			    $actualCache[$key] = $actual[$key];
		    }

		    $queryLog->origin = $originCache;
		    $queryLog->actual = $actualCache;
	    }
    }

	/**
	 * @param QueryLog $queryLog
	 * @param Model $model
	 */
    private function setActualWhenCreated(QueryLog &$queryLog, Model $model)
    {
	    unset($model->id);
	    $queryLog->actual = $model->toArray();
    }

	/**
	 * {@inheritdoc}
	 */
    public function register()
    {
	    // TODO: Implement register() method.
    }
}
