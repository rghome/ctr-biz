<?php

namespace App\Console;

use App\Console\Commands\ChangeDomainStatus;
use App\Console\Commands\ClearUsersStatistic;
use App\Console\Commands\GenerateUsers;
use App\Console\Commands\ParkDomain;
use App\Console\Commands\ReleaseHold;
use App\Console\Commands\ReleaseHoldAll;
use App\Console\Commands\ReleaseHoldByUser;
use App\Console\Commands\UpdateStatisticCache;
use App\Console\Commands\UpdateStatisticCacheByDays;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        UpdateStatisticCache::class,
        UpdateStatisticCacheByDays::class,
        GenerateUsers::class,
        ClearUsersStatistic::class,
        ReleaseHold::class,
        ParkDomain::class,
        ChangeDomainStatus::class,
        ReleaseHoldByUser::class,
        ReleaseHoldAll::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->command('ctr:release')->cron('0 0 */3 * *');
//        $schedule->command('ctr:release')->daily();
    }
}
