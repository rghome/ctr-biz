<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class GenerateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ctr:generate-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lastUser = User::limit(1)->orderBy('id', 'desc')->first();
        $lastId = $lastUser->id;
        $cont = 30;

        for ($i = 0; $i <= $cont; $i++) {
            ++$lastId;

            $password = str_random(8);

            $user = new User();
            $user->name = 'webmaster' . $lastId;
            $user->login = 'webmaster' . $lastId;
            $user->email = 'webmaster' . $lastId . '@email.com';
            $user->role_id = 2;
            $user->password = \Hash::make($password);
            $user->save();

            echo 'Login: webmaster' . $lastId . ' Password: ' . $password . "\n";
        }
    }
}
