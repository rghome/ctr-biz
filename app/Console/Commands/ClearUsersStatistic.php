<?php

namespace App\Console\Commands;

use App\Models\StatisticCache;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class ClearUsersStatistic
 * @package App\Console\Commands
 */
class ClearUsersStatistic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ctr:clear-user-statistic {users*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Example: php artisan ctr:clear-user-statistic 127:1494709200 152 173';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $args = $this->argument('users');

        foreach ($args as $row) {
            $data = explode(':', $row);

            try {
                $cache = StatisticCache::where('user', (int) $data[0])
                    ->where('days', 'exists', true)
                    ->firstOrFail()
                ;

                $tmp = [];
                if (isset($data[1])) {
                    foreach ($cache->days as $day => $statistic) {
                        if ((int) $day <= (int) $data[1]) {
                            $tmp[$day] = $statistic;
                        }
                    }
                }

                $cache->days = $tmp;
                $cache->save();
            } catch (ModelNotFoundException $e) {}
        }
    }
}
