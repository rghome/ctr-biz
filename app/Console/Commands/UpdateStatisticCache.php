<?php

namespace App\Console\Commands;

use App\Models\StatisticCache;
use App\Services\CpaAdapter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

use App\User;
use App\Models\Offer;
use App\Models\UserUtm;

use Predis\Pipeline\Pipeline;

class UpdateStatisticCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ctr:update-statistic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public function handle()
    {
        $offers = Offer::with('landings')->get();

        foreach ($offers as $offer) {
            if ($offer->multilanding == 1 && count($offer->landings)) {
                $mainsOffer = [];
                $utmTermsOffer = [];
                $utmContentsOffer = [];
                $landings = [];

                $landingSizes = count($offer->landings);

                foreach ($offer->landings as $key => $landing) {
                    $main = CpaAdapter::getTotalMetrikaByLanding($landing);
                    $utm_term = CpaAdapter::getTotalUtmByLanding($landing, 'utm_term');
                    $utm_content = CpaAdapter::getTotalUtmByLanding($landing, 'utm_content');

                    /**
                     * @todo BEGIN: Костыль для ЛП без main цели
                     */
                    if ((($landingSizes - 1) === $key) && $landing->metrika_goal_main_id == 0) {
                        $sumMainTmp = [];
                        foreach ($mainsOffer as $user => $item) {
                            if (isset($sumMainTmp[$user])) {
                                $sumMainTmp[$user] = [
                                    0 => $sumMainTmp[$user][0] + $item[0],
                                    1 => $sumMainTmp[$user][1] + $item[1],
                                ];
                            } else {
                                $sumMainTmp[$user] = $item;
                            }
                        }

                        $sumTermTmp = [];
                        foreach ($utmTermsOffer as $user => $utm) {
                            foreach ($utm as $name => $item) {
                                if (isset($sumTermTmp[$user][$name])) {
                                    $sumTermTmp[$user][$name] = [
                                        'total' => $sumTermTmp[$user][$name]['total'] + $item['total'],
                                        'new' => $sumTermTmp[$user][$name]['new'] + $item['new'],
                                    ];
                                } else {
                                    $sumTermTmp[$user][$name] = $item;
                                }
                            }
                        }

                        $sumContentTmp = [];
                        foreach ($utmContentsOffer as $user => $utm) {
                            foreach ($utm as $name => $item) {
                                if (isset($sumContentTmp[$user][$name])) {
                                    $sumContentTmp[$user][$name] = [
                                        'total' => $sumContentTmp[$user][$name]['total'] + $item['total'],
                                        'new' => $sumContentTmp[$user][$name]['new'] + $item['new'],
                                    ];
                                } else {
                                    $sumContentTmp[$user][$name] = $item;
                                }
                            }
                        }

                        foreach ($main as $user => $item) {
                            if (isset($sumMainTmp[$user])) {
                                $main[$user] = [
                                    0 => $item[0] - $sumMainTmp[$user][0],
                                    1 => $item[1] - $sumMainTmp[$user][1],
                                    2 => $item[2]
                                ];
                            }
                        }

                        foreach ($utm_term as $user => $utm) {
                            foreach ($utm as $name => $item) {
                                if (isset($sumTermTmp[$user][$name])) {
                                    $utm_term[$user][$name] = [
                                        'total' => $item['total'] - $sumTermTmp[$user][$name]['total'],
                                        'new' => $item['new'] - $sumTermTmp[$user][$name]['new'],
                                        'goal' => $item['goal']
                                    ];
                                }
                            }
                        }

                        foreach ($utm_content as $user => $utm) {
                            foreach ($utm as $name => $item) {
                                if (isset($sumContentTmp[$user][$name])) {
                                    $utm_content[$user][$name] = [
                                        'total' => $item['total'] - $sumContentTmp[$user][$name]['total'],
                                        'new' => $item['new'] - $sumContentTmp[$user][$name]['new'],
                                        'goal' => $item['goal']
                                    ];
                                }
                            }
                        }
                    }
                    /**
                     * @todo END: Костыль для ЛП без main цели
                     */

                    foreach ($main as $user => $item) {
                        $landings[$user]['main'][$landing->id] = $item;

                        if (isset($mainsOffer[$user])) {
                            $mainsOffer[$user] = [
                                0 => $mainsOffer[$user][0] + $item[0],
                                1 => $mainsOffer[$user][1] + $item[1],
                                2 => $mainsOffer[$user][2] + $item[2],
                            ];
                        } else {
                            $mainsOffer[$user] = $item;
                        }
                    }

                    foreach ($utm_term as $user => $utm) {
                        foreach ($utm as $name => $item) {
                            $landings[$user]['utm_term'][$landing->id][$name] = $item;

                            if (isset($utmTermsOffer[$user][$name])) {
                                $utmTermsOffer[$user][$name] = [
                                    'total' => $utmTermsOffer[$user][$name]['total'] + $item['total'],
                                    'new' => $utmTermsOffer[$user][$name]['new'] + $item['new'],
                                    'goal' => $utmTermsOffer[$user][$name]['goal'] + $item['goal'],
                                ];
                            } else {
                                $utmTermsOffer[$user][$name] = $item;
                            }
                        }
                    }

                    foreach ($utm_content as $user => $utm) {
                        foreach ($utm as $name => $item) {
                            $landings[$user]['utm_content'][$name][$landing->id] = $item;

                            if (isset($utmContentsOffer[$user][$name])) {
                                $utmContentsOffer[$user][$name] = [
                                    'total' => $utmContentsOffer[$user][$name]['total'] + $item['total'],
                                    'new' => $utmContentsOffer[$user][$name]['new'] + $item['new'],
                                    'goal' => $utmContentsOffer[$user][$name]['goal'] + $item['goal'],
                                ];
                            } else {
                                $utmContentsOffer[$user][$name] = $item;
                            }
                        }
                    }
                }

                $this->processOffer([
                    'main' => $mainsOffer,
                    'utm_term' => $utmTermsOffer,
                    'utm_content' => $utmContentsOffer
                ], $offer, $landings); //
            } else {
                $this->processOffer([
                    'main' => CpaAdapter::getTotalMetrikaByOffer($offer),
                    'utm_term' => CpaAdapter::getTotalUtmByOffer($offer, 'utm_term'),
                    'utm_content' => CpaAdapter::getTotalUtmByOffer($offer, 'utm_content')
                ], $offer);
            }
        }
    }

    /**
     * @param array $data
     * @param Offer $offer
     * @param array $landings
     */
    private function processOffer(array $data = [], Offer $offer, array $landings = [])
    {
        foreach ($data['main'] as $userId => $statistic) {
            $cache = StatisticCache::where('user', (int) $userId)->first();

            if (!empty($cache->offers)) {
                $tmp = $cache->offers;
                $tmp[$offer->id] = [
                    'total' => (int) $statistic[0],
                    'new' => (int) $statistic[1],
                    'goal' => (int) $statistic[2]
                ];

                $cacheOffers = $tmp;
            } else {
                $cacheOffers = [
                    $offer->id => [
                        'total' => (int) $statistic[0],
                        'new' => (int) $statistic[1],
                        'goal' => (int) $statistic[2]
                    ]
                ];
            }

            if (!empty($landings[$userId]['main'])) {
                $landingCache = [];
                foreach ($landings[$userId]['main'] as $landing => $item) {
                    $landingCache[$landing] = [
                        'total' => (int) $item[0],
                        'new' => (int) $item[1],
                        'goal' => (int) $item[2],
//                        'utm' => [
//                            'utm_term' => $landings[$userId]['utm_term'][$landing]
//                        ]
                    ];
                }

                $cacheOffers[$offer->id]['landings'] = $landingCache;
            }

            $cache->offers = $cacheOffers;
            $cache->save();
        }

        foreach ($data['utm_term'] as $userId => $utm) {
            $cache = StatisticCache::where('user', (int) $userId)->first();

            $tmp = $cache->offers;

            foreach ($utm as $name => $value) {
                if (strpos($name, '.') !== false) {
                    $utm[str_replace('.', '-', $name)] = $value;
                    unset($utm[$name]);
                }
            }

            $tmp[$offer->id]['utm']['utm_term'] = $utm;

            if (!empty($landings[$userId]['utm_term'])) {
                $landingCache = [];
                foreach ($landings[$userId]['utm_term'] as $landing => $utmLanding) {
                    foreach ($utmLanding as $name => $item) {
                        $landingCache[str_replace('.', '-', $landing)][str_replace('.', '-', $name)] = $item;
                    }
                }
                $tmp[$offer->id]['utm']['landings']['utm_term'] = $landingCache;
            }
            $cache->offers = $tmp;
            $cache->save();
        }

        foreach ($data['utm_content'] as $userId => $utm) {
            $cache = StatisticCache::where('user', (int) $userId)->first();

            $tmp = $cache->offers;

            foreach ($utm as $name => $value) {
                if (strpos($name, '.') !== false) {
                    $utm[str_replace('.', '-', $name)] = $value;
                    unset($utm[$name]);
                }
            }

            $tmp[$offer->id]['utm']['utm_content'] = $utm;

            if (!empty($landings[$userId]['utm_content'])) {
                $landingCache = [];
                foreach ($landings[$userId]['utm_content'] as $landing => $utmLanding) {
                    foreach ($utmLanding as $name => $item) {
                        $landingCache[str_replace('.', '-', $landing)][str_replace('.', '-', $name)] = $item;
                    }
                }
                $tmp[$offer->id]['utm']['landings']['utm_content'] = $landingCache;
            }

            $cache->offers = $tmp;
            $cache->save();
        }
    }

    private function prepareUsers()
    {
        $users = User::get();
        foreach ($users as $user) {
            $cache = StatisticCache::where('user', (int) $user->id)->first();

            if (!$cache) {
                $cache = new StatisticCache();
                $cache->user = $user->id;
                $cache->save();
            }
        }
    }
}
