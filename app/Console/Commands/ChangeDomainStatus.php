<?php

namespace App\Console\Commands;

use App\Models\Domain;
use App\Notifications\DomainUpdate;
use App\Notifications\Helpers\DomainUpdateMessages;
use App\User;
use Illuminate\Console\Command;

class ChangeDomainStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ctr:change-domain-status {domain} {--status=0} {--message=?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $domainId = $this->argument('domain');
        $status = $this->option('status');
        $message = $this->option('message');

        /** @var Domain $domain */
        $domain = Domain::where('id', $domainId)->firstOrFail();

        /** @var User $user */
        $user = User::where('id', $domain->user_id)->firstOrFail();

        $user->notify(new DomainUpdate($domain, $status, $message));

        $domain->moderate = (int) $status;

        if ($status == 2) {
            $domain->comment = DomainUpdateMessages::MESSAGES[$message];
        } else {
            $domain->comment = null;
        }

        $domain->save();

        return true;
    }
}
