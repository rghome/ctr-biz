<?php

namespace App\Console\Commands;

use DigitalOceanV2\Api\Domain;
use DigitalOceanV2\Entity\Domain as DomainEntity;
use DigitalOceanV2\Api\DomainRecord;
use DigitalOceanV2\Exception\HttpException;
use Illuminate\Console\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use phpWhois\Whois;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use GrahamCampbell\DigitalOcean\Facades\DigitalOcean;

/**
 * Class ParkDomain
 * @package App\Console\Commands
 */
class ParkDomain extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ctr:park-domain {domain} {--server=remote}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var Domain|null
     */
    private $domainAPI = null;

    /**
     * @var DomainRecord|null
     */
    private $domainRecord = null;

    /**
     * @var string|null
     */
    private $serverIP = null;

    /**
     * @var string|null
     */
    private $serverIPv6 = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->domainAPI =      DigitalOcean::domain();
        $this->domainRecord =   DigitalOcean::domainRecord();

        $this->serverIP =   '207.154.236.87';
        $this->serverIPv6 = '2a03:b0c0:3:d0::3de0:e001';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $domain = $this->argument('domain');

        $this->checkDNS($domain);

        /** @var $domains Domain  */
        try {
            $this->domainAPI->delete($domain);

            $this->domainAPI->getByName($domain);

            throw new RuntimeException('Домен уже используется');
        } catch (HttpException $exception) {
            $domain = $this->createDomain($domain);

            $this->finish($domain, $this->writeToNginxConfig($domain));
        }
    }

    private function finish(DomainEntity $domain, string $file)
    {
        if ($this->option('server') === 'remote') {
            $this->uploadFile($file);
            $this->restartNginx();

        } elseif ($this->option('server') === 'local') {
            /** @todo: implement local action */
            return '';
        }
    }

    /**
     * @param DomainEntity $domain
     * @return string
     */
    private function writeToNginxConfig(DomainEntity $domain)
    {
        $file = $this->getNginxConfigFile();

        if ($this->checkNginxBlockExists($file, $domain->name)) {
            file_put_contents($file, $this->getConfigBlock($domain->name) . PHP_EOL, FILE_APPEND);
        }

        return $file;
    }

    /**
     * @param string $file
     * @param string $domainName
     * @return bool
     */
    private function checkNginxBlockExists(string $file, string $domainName)
    {
        $content = file_get_contents($file);

        return (strpos($content, $domainName) === false);
    }

    /**
     * @return string
     */
    private function getNginxConfigFile()
    {
        if ($this->option('server') === 'remote') {
            return $this->downloadFile();//'/Users/romangorbatko/Development/Sites/ctr-biz/config/redirects'; //
        } elseif ($this->option('server') === 'local') {
            /** @todo: implement local action */
            return '';
        }
    }

    /**
     * @param string $domainName
     * @return string
     */
    private function getConfigBlock(string $domainName)
    {
        return "\n\n" .
            "server { \n".
                "\troot /var/www/ctr-biz-redirecting/public;\n".
                "\tindex index.php;\n".
                "\tserver_name " . $domainName . ";\n".
                "\t\n".
                "\tlocation / {\n".
                    "\t\ttry_files \$uri \$uri/ /index.php?\$query_string;\n".
                "\t}\n".
                "\t\n".
                "\tlocation ~ \\.php$ {\n".
                    "\t\ttry_files \$uri =404;\n".
                    "\t\tfastcgi_split_path_info ^(.+\\.php)(/.+)$;\n".
                    "\t\tfastcgi_pass 127.0.0.1:9000;\n".
                    "\t\tfastcgi_index index.php;\n".
                    "\t\tfastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;\n".
                    "\t\tinclude fastcgi_params;\n".
                "\t}\n".
            "}\n"
        ;
    }

    /**
     * @return string
     */
    private function downloadFile()
    {
        $connection = $this->getSshConnection();

        $file = config_path() . '/redirects';
        ssh2_scp_recv($connection, '/etc/nginx/sites-available/redirects', $file);

        return $file;
    }

    /**
     * @param string $file
     * @return string
     */
    private function uploadFile(string $file)
    {
        $connection = $this->getSshConnection();

        ssh2_scp_send($connection, $file, '/etc/nginx/sites-available/redirects');

        return $file;
    }

    private function restartNginx()
    {
        $connection = $this->getSshConnection();

        $stream = ssh2_exec($connection, '/etc/init.d/nginx restart');
    }

    /**
     * @return resource
     */
    private function getSshConnection()
    {
        $sshKeysPath = resource_path() . '/keys/ssh/';
        $passPhrase = '22roma541573';

        $connection = ssh2_connect($this->serverIP, 22);
        ssh2_auth_pubkey_file($connection, 'root', $sshKeysPath . 'id_rsa.pub', $sshKeysPath . 'id_rsa', $passPhrase);

        return $connection;
    }

    /**
     * @param string $domainName
     * @return \DigitalOceanV2\Entity\Domain
     */
    private function createDomain(string $domainName)
    {
        $domain = $this->domainAPI->create($domainName, $this->serverIP);

        $this->domainRecord->create($domain->name, 'AAAA', '@', $this->serverIPv6);
        $this->domainRecord->create($domain->name, 'CNAME', 'www', $domain->name . '.');

        return $domain;
    }

    /**
     * @param string $domain
     */
    private function checkDNS(string $domain)
    {
        $whois = new Whois();
        $dns = $whois->lookup($domain, false);

        if (empty($dns)) {
            throw new RuntimeException('Неизвестный DNS');
        }

        $ns = false;
        foreach ($dns['rawdata'] as $record) {
            if (strpos(mb_strtolower($record), '.digitalocean.com') !== false) {
                $ns = true;
            }
        }

        if ($ns === false) {
            throw new RuntimeException('Неверный NS');
        }
    }
}
