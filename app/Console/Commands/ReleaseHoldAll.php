<?php

namespace App\Console\Commands;

use App\Models\Landing;
use App\Models\Offer;
use App\Models\StatisticCache;
use App\Services\CpaAdapter;
use Illuminate\Console\Command;

class ReleaseHoldAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ctr:release-hold-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cache = StatisticCache::all();

//        if (isset($cache->days)) {
        foreach ($cache as $row) {
            $tmp = [];
            if (isset($row->days)) {
                foreach ($row->days as $day => $item) {
                    $tmp[$day] = $item;

                    foreach ($item as $type => $value) {
                        foreach ($value as $offerId => $offer) {
                            if (
//                                (isset($offer['price']['hold'])) &&
//                                ((int) $offer['price']['available'] === 0) &&
                                $day >= 1495659600
                            ) {
                                $price = 0;
                                if ($type === 'offers') {
                                    $price = (new CpaAdapter())->getTotalFree((int) $offer['goal'], Offer::where('id', $offerId)->first());
                                } elseif ($type === 'landings') {
                                    $price = (new CpaAdapter())->getTotalFree((int) $offer['goal'], null, Landing::where('id', $offerId)->first());
                                }

                                echo $row->user . ' | ' . date("d.m.Y", $day)  . ' | ' . $offer['price']['hold'] . ' | ' . $offer['price']['available'] . ' | ' . $type . ' | ' . $price . PHP_EOL;

                                $tmp[$day][$type][$offerId]['price']['available'] = $price;
                                $tmp[$day][$type][$offerId]['price']['hold'] = 0;
                            }
                        }
                    }

                }

                $row->days = $tmp;
                $row->save();
            }
        }
    }
}
