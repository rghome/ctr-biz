<?php

namespace App\Console\Commands;

use App\Models\StatisticCache;
use Illuminate\Console\Command;

class ReleaseHoldByUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ctr:release-hold-by-user {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = $this->argument('user_id');

        $cache = StatisticCache::where('user', (int) $userId)->first();
        $tmp = [];
        foreach ($cache->days as $day => $item) {
            $tmp[$day] = $item;

            foreach ($item as $type => $value) {
                foreach ($value as $offerId => $offer) {
                    if (
                        (isset($offer['price']['hold']) && (int) $offer['price']['hold'] !== 0) &&
                        ((int) $offer['price']['available'] === 0)
                    ) {
                        echo date("d.m.Y", $day) . ' | ' . $offer['price']['hold'] . PHP_EOL;

                        $tmp[$day][$type][$offerId]['price']['available'] =  (int) $offer['price']['hold'];
                        $tmp[$day][$type][$offerId]['price']['hold'] = 0;
                    }
                }
            }

        }

        $cache->days = $tmp;
        $cache->save();
    }
}
