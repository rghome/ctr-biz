<?php

namespace App\Console\Commands;

use App\Models\StatisticCache;
use Carbon\Carbon;
use Illuminate\Console\Command;

/**
 * Class ReleaseHold
 * @package App\Console\Commands
 */
class ReleaseHold extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ctr:release-hold {days?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * ReleaseHold constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = $this->argument('days') ?: 2;

        $startDay = (new Carbon())->subDays($days)->setTime(0, 0)->getTimestamp();

        $cache = StatisticCache::where('days.' . $startDay, 'exists', true)->get();
        foreach ($cache as $item) {
            $tmp = $item->days;
            foreach ($tmp[$startDay] as $key => &$value) {
                $value = array_map([ReleaseHold::class, 'releaseNested'], $value);
            }
            $item->days = $tmp;
            $item->save();
        }

    }

    /**
     * @param array $item
     * @return array
     */
    private function releaseNested(array $item): array
    {
        if (
            (isset($item['price']['hold']) && (int) $item['price']['hold'] !== 0) &&
            ((int) $item['price']['available'] === 0)
        ) {
            $item['price']['available'] = (int) $item['price']['hold'];
            $item['price']['hold'] = 0;
        }

        foreach (['utm_term', 'utm_content'] as $utm) {
            if (isset($item[$utm])) {
                $item[$utm] = array_map([ReleaseHold::class, 'releaseNested'], $item[$utm]);
            }
        }

        return $item;
    }
}
