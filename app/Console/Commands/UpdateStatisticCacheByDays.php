<?php

namespace App\Console\Commands;

use App\Models\Landing;
use App\Models\Offer;
use App\Models\StatisticCache;
use App\Services\CpaAdapter;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateStatisticCacheByDays extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ctr:update-statistic-by-days';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var array
     */
    protected $tmp = [];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->prepareUsers();

//        $startDays = array_reverse(range(0, 7));
        $startDays = [0];

        $offers = Offer::with('landings')->where('status', 1)->get();

        foreach ($startDays as $keyDays => $day) {
            foreach ($offers as $offer) {
                $startDay = (new Carbon())->subDays($day)->setTime(0, 0);
                $endDay = $startDay;

                if ($offer->multilanding == 1 && count($offer->landings)) {
                    $totalByOffer = [];
                    $landingSizes = count($offer->landings);

                    foreach ($offer->landings as $key => $landing) {
                        $main = CpaAdapter::getTotalMetrikaByLandingAndDays($landing, $startDay, $endDay);
                        $utm_term = CpaAdapter::getTotalUtmByLandingAndDays($landing, 'utm_term', $startDay, $endDay);
                        $utm_content = CpaAdapter::getTotalUtmByLandingAndDays($landing, 'utm_content', $startDay, $endDay);

                        if ((($landingSizes - 1) === $key) && $landing->metrika_goal_main_id == 0) {
                            $sumMainTmp = [];
                            foreach ($totalByOffer as $userId => $timestampValues) {
                                foreach ($timestampValues as $keyId => $landingsValues) {
                                    foreach ($landingsValues as $landingId => $itemValue) {
                                        $sumMainTmp[$userId] = $itemValue;
                                    }
                                }
                            }

                            foreach ($main as $userId => $itemValue) {
                                if (isset($sumMainTmp[$userId])) {
                                    if (!isset($sumMainTmp[$userId]['total'])) {
                                        \Log::warning('UpdateStatisticCacheByDays: user - ' . $userId . '; Body: ' . \GuzzleHttp\json_encode($sumMainTmp));
                                    }

                                    $main[$userId] = [
                                        0 => $itemValue[0] - ((isset($sumMainTmp[$userId]['total'])) ? $sumMainTmp[$userId]['total'] : 0),
                                        1 => $itemValue[1] - ((isset($sumMainTmp[$userId]['new'])) ? $sumMainTmp[$userId]['new'] : 0),
                                        2 => $itemValue[2]
                                    ];
                                }
                            }

                            foreach ($utm_term as $userId => $itemTerm) {
                                if (isset($sumMainTmp[$userId])) {
                                    foreach ($itemTerm as $utmTermName => $utmTermValue) {
                                        if (isset($sumMainTmp[$userId]['utm_term'][$utmTermName])) {
                                            $utm_term[$userId][$utmTermName] = [
                                                'total' => (int) $utmTermValue['total'] - $sumMainTmp[$userId]['utm_term'][$utmTermName]['total'],
                                                'new' => (int) $utmTermValue['new'] - $sumMainTmp[$userId]['utm_term'][$utmTermName]['new'],
                                                'goal' => (int) $utmTermValue['goal']
                                            ];
                                        }
                                    }
                                }
                            }

                            foreach ($utm_content as $userId => $itemContent) {
                                if (isset($sumMainTmp[$userId])) {
                                    foreach ($itemContent as $utmContentName => $utmContentValue) {
                                        if (isset($sumMainTmp[$userId]['utm_content'][$utmContentName])) {
                                            $utm_content[$userId][$utmContentName] = [
                                                'total' => (int) $utmContentValue['total'] - $sumMainTmp[$userId]['utm_content'][$utmContentName]['total'],
                                                'new' => (int) $utmContentValue['new'] - $sumMainTmp[$userId]['utm_content'][$utmContentName]['new'],
                                                'goal' => (int) $utmContentValue['goal']
                                            ];
                                        }
                                    }
                                }
                            }
                        }

                        foreach ($main as $user => $item) {
                            if (isset($totalByOffer[$user][$startDay->getTimestamp()][$landing->id])) {
                                $totalByOffer[$user][$startDay->getTimestamp()][$landing->id] = [
                                    'total' => (int) $totalByOffer[$user][$startDay->getTimestamp()][$landing->id]['total'] + (int) $item[0],
                                    'new' => (int) $totalByOffer[$user][$startDay->getTimestamp()][$landing->id]['new'] + (int) $item[1],
                                    'goal' => (int) $totalByOffer[$user][$startDay->getTimestamp()][$landing->id]['goal'] + (int) $item[2]
                                ];
                            } else {
                                $totalByOffer[$user][$startDay->getTimestamp()][$landing->id] = [
                                    'total' => (int) $item[0],
                                    'new' => (int) $item[1],
                                    'goal' => (int) $item[2]
                                ];
                            }
                        }

                        foreach ($utm_term as $user => $items) {
                            foreach ($items as $utmName => $item) {
                                if (strpos($utmName, '.') !== false) {
                                    $utmName = str_replace('.', '-', $utmName);
                                }

                                $totalByOffer[$user][$startDay->getTimestamp()][$landing->id]['utm_term'][$utmName] = [
                                    'total' => (int) $item['total'],
                                    'new' => (int) $item['new'],
                                    'goal' => (int) $item['goal'],
                                ];
                            }
                        }

                        foreach ($utm_content as $user => $items) {
                            foreach ($items as $utmName => $item) {
                                if (strpos($utmName, '.') !== false) {
                                    $utmName = str_replace('.', '-', $utmName);
                                }

                                $totalByOffer[$user][$startDay->getTimestamp()][$landing->id]['utm_content'][$utmName] = [
                                    'total' => (int) $item['total'],
                                    'new' => (int) $item['new'],
                                    'goal' => (int) $item['goal'],
                                ];
                            }
                        }
                    }

                    foreach ($totalByOffer as $user => $timestamp) {
                        foreach ($timestamp as $key => $landings) {
                            foreach ($landings as $landing => $item) {
                                $this->tmp[$user][$key]['landings'][$landing] = [
                                    'total' => (int)$item['total'],
                                    'new' => (int)$item['new'],
                                    'goal' => (int) $item['goal'],
                                    'price' => [
                                        'hold' => (new CpaAdapter())->getTotalFree((int) $item['goal'], null, Landing::where('id', $landing)->first()),
                                        'available' => 0
                                    ]
                                ];

                                if (!$item['total']) {
                                    unset($this->tmp[$user][$key]['landings'][$landing]);
                                    continue;
                                }

                                foreach (['utm_term', 'utm_content'] as $utm) {
                                    if (isset($item[$utm]) && !empty($item[$utm])) {
                                        $this->fillUtmToLanding($item[$utm], $user, $key, $landing, $utm);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $metrika = CpaAdapter::getTotalMetrikaByOfferAndDays($offer, $startDay, $endDay);
                    $utm_term = CpaAdapter::getTotalUtmByOfferAndDays($offer, 'utm_term', $startDay, $endDay);
                    $utm_content = CpaAdapter::getTotalUtmByOfferAndDays($offer, 'utm_content', $startDay, $endDay);

                    foreach ($metrika as $user => $item) {
                        $this->tmp[$user][$startDay->getTimestamp()]['offers'][$offer->id] = [
                            'total' => (int) $item[0],
                            'new' => (int) $item[1],
                            'goal' => (int) $item[2],
                            'price' => [
                                'hold' => (new CpaAdapter())->getTotalFree((int) $item[2], $offer),
                                'available' => 0
                            ]
                        ];
                    }

                    foreach (['utm_term', 'utm_content'] as $utm) {
                        $this->fillUtmToOffer(${$utm}, $offer, $utm, $startDay);
                    }
                }
            }
        }

        foreach ($this->tmp as $userId => $items) {
            $cache = StatisticCache::where('user', (int) $userId)->first();

            if (isset($cache->days)) {
                $tmp = $cache->days;
                foreach ($items as $timestamp => $item) {
                    $tmp[$timestamp] = $item;
                }
            } else {
                $tmp = $items;
            }

            $cache->days = $tmp;
            $cache->save();
        }
    }

    /**
     * @param array $data
     * @param Offer $offer
     * @param string $utm
     * @param Carbon $startDay
     */
    private function fillUtmToOffer(array $data, Offer $offer, string $utm, Carbon $startDay)
    {
        foreach ($data as $userId => $utmContentValues) {
            foreach ($utmContentValues as $utmContentName => $utmContentValue) {
                if (strpos($utmContentName, '.') !== false) {
                    $utmContentName = str_replace('.', '-', $utmContentName);
                }

                $this->tmp[$userId][$startDay->getTimestamp()]['offers'][$offer->id][$utm][$utmContentName] = [
                    'total' => (int) $utmContentValue['total'],
                    'new' => (int) $utmContentValue['new'],
                    'goal' => (int) $utmContentValue['goal'],
                    'price' => [
                        'hold' => (new CpaAdapter())->getTotalFree((int) $utmContentValue['goal'], $offer),
                        'available' => 0
                    ]
                ];
            }
        }
    }

    /**
     * @param array $data
     * @param int $user
     * @param int $timestamp
     * @param int $landing
     * @param string $utmType
     */
    private function fillUtmToLanding(array $data = [], int $user, int $timestamp, int $landing, string $utmType)
    {
        foreach ($data as $utm => $itemContent) {
            $this->tmp[$user][$timestamp]['landings'][$landing][$utmType][$utm] = [
                'total' => (int) $itemContent['total'],
                'new' => (int) $itemContent['new'],
                'goal' => (int) $itemContent['goal'],
                'price' => [
                    'hold' => (new CpaAdapter())->getTotalFree((int) $itemContent['goal'], null, Landing::where('id', $landing)->first()),
                    'available' => 0
                ]
            ];
        }
    }

    private function prepareUsers()
    {
        $users = User::get();
        foreach ($users as $user) {
            $cache = StatisticCache::where('user', (int) $user->id)->first();

            if (!$cache) {
                $cache = new StatisticCache();
                $cache->user = $user->id;
                $cache->save();
            }
        }
    }
}
