<?php
namespace App\Http\Middleware;

// First copy this file into your middleware directoy

use App\Services\RoleVerifier;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class CheckRole {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		list($method, $controller) = $this->getMethodAndController();

		if (!RoleVerifier::can($controller . '.' . $method)) {
			return response('Forbidden', 403);
		}

		return $next($request);
	}

	/**
	 * @return array
	 */
	private function getMethodAndController()
	{
		$routeInformation = Route::getFacadeRoot()->current()->getActionName();
		$dataRoute = explode('@', $routeInformation);
		$method = $dataRoute[1];
		$controller = explode('\\', $dataRoute[0]);
		$controller = array_pop($controller);
		$controller = strtolower(str_replace('Controller', '', $controller));

		return [$method, $controller];
	}

}
