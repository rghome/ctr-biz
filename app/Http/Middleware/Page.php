<?php

namespace App\Http\Middleware;

use App\Repository\PageRepository;
use Closure;
use Illuminate\Support\Facades\Response;

class Page
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		return $next($request);
	}
}
