<?php

namespace App\Http\Middleware;

use Closure;

class GuidCheck
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @param  string|null $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = null)
	{
		//TODO old project @remove
		$guid = $request->session()->get('guid');

		if (! $guid) {
			$newGuid = time() . uniqid() . rand(1, 1024) . uniqid();

			$request->session()->put('guid', $newGuid);
		}

		return $next($request);
	}
}
