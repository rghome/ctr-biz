<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 3/23/17
 * Time: 12:58 AM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Search\SearchTrait;
use App\Models\Payment;
use App\Repository\PaymentRepository;
use App\Services\CpaAdapter;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Repository\StatisticRepository;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    use SearchTrait;

    /**
     * Минимум к выводу
     *
     * @var int
     */
    protected $withdrawalLimit = 1000;

    /**
     * AddressController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roles', ['roles' => ['administrator', 'user']]);
    }

    public function index()
    {
        $params = [
            'data' => Payment::orderBy('id', 'desc')
                ->where('user_id', Auth::user()->id)
                ->paginate($this->rows)
        ];

        return view('admin.payments.index', $params);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function createWithdrawal(Request $request)
    {
        $user = Auth::user();
        $amount = $request->get('amount', null);
        $amountAvailable = (new CpaAdapter())->getTotalFreeWrapper();
        $amountWaiting = (new PaymentRepository())->getTotalWaitingByUser();

        if ($amount === null) {
            return response()->json([
                'message' => 'Укажите сумму к выводу',
                'status_code' => Response::HTTP_INTERNAL_SERVER_ERROR
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        if ((int) $amount > (int) $amountAvailable) {
            return response()->json([
                'message' => 'Сумма не должна быть больше доступной к выводу: ' . $amountAvailable . ' руб.',
                'status_code' => Response::HTTP_INTERNAL_SERVER_ERROR
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        if ((int) $amountWaiting > $amountAvailable) {
            return response()->json([
                'message' => 'Сумма уже запрошенна к выводу.',
                'status_code' => Response::HTTP_INTERNAL_SERVER_ERROR
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        if ((int) $amount < $this->withdrawalLimit) {
            return response()->json([
                'message' => 'Минимальная сумма к выводу: ' . $this->withdrawalLimit,
                'status_code' => Response::HTTP_INTERNAL_SERVER_ERROR
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $payment = new Payment();
        $payment->total = $amount;
        $payment->status = Payment::STATUS_WAIT;
        $payment->output = $this->getOutputMethod($user);
        $payment->user_id = $user->id;
        $payment->save();

        return response()->json([
            'data' => [
                'payment' => $payment
            ],
            'status_code' => Response::HTTP_OK
        ]);
    }

    public function deleteWithdrawal(Payment $payment)
    {
        try {
            $payment->delete();
        }
        catch(\Exception $e) {
//            return redirect('dashboard/domains')->withErrors('У адреса есть связи');
        }

        return redirect('dashboard/payments');
    }

    /**
     * @param $user
     * @return string
     */
    private function getOutputMethod($user)
    {
        if (isset($user->webmoney) && $user->webmoney !== '') {
            return 'WM: ' . $user->webmoney;
        } else {
            return false;
//            return 'ЯМ: ' . $user->yandexmondey;
        }
    }

    public function getWithdrawal()
    {
        $paymentRepository = new PaymentRepository($this->rows);
        $allWaiting = $paymentRepository->getAll();

        $params = [
            'allWaiting' => $allWaiting
        ];

        return view('admin.payments.withdrawal.index', $params);
    }

    public function editWithdrawal(Payment $payment, Request $request)
    {
        $params = [
            'payment' => $payment
        ];

        if ($request->request->all()) {
            $data = $request->request->get('form');

            $payment->status = $data['status'];
            $payment->comment = $data['comment'];
            $payment->save();

            return redirect()->route('withdrawals');
        }

        return view('admin.payments.withdrawal.edit', $params);
    }
}