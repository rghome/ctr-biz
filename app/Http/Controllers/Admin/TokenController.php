<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 5/22/17
 * Time: 6:40 AM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Offer;
use App\Models\Token;
use App\Models\TokenOffers;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class TokenController
 * @package App\Http\Controllers\Admin
 */
class TokenController extends Controller
{
    /**
     * AddressController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roles', ['roles' => ['administrator', 'user']]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $params = [
            'data' => Token::with('offers.offer')->get()
        ];

        return view('admin.token.index', $params);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $offers = Offer::all();
        $tokenOffers = TokenOffers::all();

        foreach ($offers as $offerKey => $offer) {
            foreach ($tokenOffers as $tokenOffer) {
                if ($tokenOffer->offer_id == $offer->id) {
                    $offers = $offers->forget($offerKey);
                }
            }
        }

        $params = [
            'offers' => $offers
        ];

        if ($request->request->all()) {
            $data = $request->request->get('form');

            $token = new Token();
            $token->email = $data['email'];
            $token->token = $data['token'];
            $token->time_flag = (new Carbon())->setTime(0, 0)->getTimestamp();
            $token->save();

            foreach ($data['offers'] as $offerId) {
                $tokenOffer = new TokenOffers();
                $tokenOffer->token_id = $token->id;
                $tokenOffer->offer_id = $offerId;
                $tokenOffer->save();
            }

            return redirect()->route('tokens');
        }

        return view('admin.token.create', $params);
    }

    /**
     * @param Token $token
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit(Token $token, Request $request)
    {
        $params = [
            'token' => Token::where('id', $token->id)->with('offers')->first(),
            'offers' => Offer::all()
        ];

        if ($request->request->all()) {
            $data = $request->request->get('form');

            $token->email = $data['email'];
            $token->token = $data['token'];
            $token->time_flag = (new Carbon())->setTime(0, 0)->getTimestamp();
            $token->save();

            TokenOffers::where('token_id', '=', $token->id)->delete();
            foreach ($data['offers'] as $offerId) {
                TokenOffers::where('offer_id', '=', $offerId)->delete();

                $tokenOffer = new TokenOffers();
                $tokenOffer->token_id = $token->id;
                $tokenOffer->offer_id = $offerId;
                $tokenOffer->save();
            }

            return redirect()->route('tokens');
        }

        return view('admin.token.create', $params);
    }
}