<?php

namespace App\Http\Controllers\Admin;

use Mail;
use App\Models\Domain;
use App\Role;
use App\Http\Controllers\Helper\Search\SearchTrait;
use App\Models\Offer;
use App\Models\OfferPrice;
use App\Models\Region;
use App\Models\Server;
use DigitalOceanV2\Entity\Droplet;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use App\Http\Controllers\Controller;

use GrahamCampbell\DigitalOcean\Facades\DigitalOcean;

use Illuminate\Support\Facades\Auth;

/**
 * Class AddressController
 * @package App\Http\Controllers\Admin
 */
class DomainController extends Controller
{
    use SearchTrait;

    /**
     * AddressController constructor.
     */
    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('roles', ['roles' => ['administrator', 'user']]);
	}

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
	{
	    if (Auth::user()->role_id == 1) {
            $params = [
                'data' => Domain::orderBy('id', 'desc')->paginate($this->rows)
            ];
        } else {
            $params = [
                'data' => Domain::where('user_id', '=', Auth::user()->id)
                    ->orderBy('id', 'desc')
                    ->paginate($this->rows)
            ];
        }

		return view('admin.domains.index', $params);
	}

    /**
     * @param Request $request
     * @return RedirectResponse|View
     */
	public function create(Request $request)
    {
        if ($request->request->all()) {
            $data = $request->request->get('form');

            preg_match("/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/", trim($data['name']), $output_array);

            if (empty($output_array)) {
                return redirect('dashboard/domain/create')->withErrors('Укажите домен');
            }

            $domain = new Domain();
            $domain->name = trim($data['name']);
            $domain->user_id = Auth::user()->id;

            $domain->save();

            Mail::send('emails.notify.domain.add', ['domain' => $domain], function ($m)  {
                $m->from('postmaster@sandbox5c70ae42f8e0431bb3ca35c36dc8067a.mailgun.org', 'CTR BIZ');

                $m->to('roman.gor.home@gmail.com', 'Roman Gorbatko')->subject('New notify!');
            });


            return redirect()->route('domains');
        }

        return view('admin.domains.create');
    }

    /**
     * @param $id
     * @param Request $request
     * @return View
     */
	public function edit($id, Request $request)
    {
        $domain = Domain::findOrFail($id);

        $params = [
            'domain' => $domain
        ];

        if ($request->request->all()) {
            $data = $request->request->get('form');

            preg_match("/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/", trim($data['name']), $output_array);

            if (empty($output_array)) {
                return redirect('dashboard/domain/create')->withErrors('Укажите домен');
            }

            $domain->name = trim($data['name']);

            if (isset($data['moderate'])) {
                $domain->moderate = 1;
            }

//            $domain->user_id = Auth::user()->id;

            $domain->save();

            return redirect()->route('domains');
        }

        return view('admin.domains.create', $params);
    }

    /**
     * @param Domain $domain
     * @return $this|RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(Domain $domain)
    {
        $currentUser = Auth::user();
        $getAdminRole = Role::where('name', '=', 'Administrator')->first();

        if($currentUser->role_id != $getAdminRole->id)
        {
            return redirect('dashboard/domains')->withErrors('У вас нет прав на удаление');
        }

        try {
            $domain->delete();
        }
        catch(\Exception $e) {
            return redirect('dashboard/domains')->withErrors('У адреса есть связи');
        }

        return redirect('dashboard/domains');
    }

}
