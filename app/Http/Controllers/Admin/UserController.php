<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Helper\Search\SearchTrait;
use App\Role;
use \App\User;
use App\Models\Invite;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use \App\Models;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

/**
 * Class UserController
 * @package App\Http\Controllers\Admin
 */
class UserController extends Controller
{

    use SearchTrait, AuthenticatesAndRegistersUsers;

    /**
     * UserController constructor.
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;

        $this->middleware('roles', ['roles' => ['administrator', 'user']]);
    }

    /**
     * Index page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $params = [];
        $params['users'] = User::with('role')->orderBy('id', 'desc')->paginate($this->rows);

        return view('admin.user.index', $params);
    }

    /**
     * Update user
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public  function update($id)
    {
        return view('admin.user.update', $this->getUser($id));
    }

    /**
     * @param $id
     * @return array
     */
    public  function getUser($id)
    {
        $params = [];
        $user = new User();
        $params['user'] = $user->with('role')->find($id)->toArray();
        $params['id'] = $id;
        $params['roles'] = Role::all();

        return $params;
    }

    /**
     * Create user
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if($request->isMethod('post')) {

            $user = new User();

            try {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required',
                    'login' => 'required|unique:users',
                    'role_id' => 'required',
                    'password' => 'required|min:6',
                    'password_confirm' => 'required|min:6|same:password',
                ]);

                $user->phone = $request->phone;
                $user->name = $request->name;
                $user->email = $request->email;
                $user->address = $request->address;
                $user->role_id = $request->role_id;
                $user->webmoney = $request->webmoney;
//                $user->yandexmondey = $request->yandexmondey;
                $user->login = $request->login;
                $user->password = bcrypt($request->password);

                if ($validator->fails()) {
                    return view('admin.user.create', [
                        'roles' => Role::all(),
                        'user' => $user,
                    ])->withErrors($validator);
                }

                $user->save();

                return redirect('/dashboard/users');
            }
            catch(\InvalidArgumentException $e) {
                Log::error('Ошибка при создании пользовтеля UserController@create');
            }

        }

        $roles = Role::all();

        return view('admin.user.create', [
            'roles' => $roles,
        ]);
    }

    /**
     * @param User $user
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete(User $user)
    {
        $currentUser = Auth::user();
        $getAdminRole = Role::where('name', '=', 'Administrator')->first();

        if($currentUser->role_id != $getAdminRole->id)
        {
            return redirect('dashboard/users')->withErrors('У вас нет прав на удаление');
        }

        try {
            $user->delete();
        }
        catch(\Exception $e) {
            return redirect('dashboard/users')->withErrors('У пользователя есть связи');
        }

        return redirect('dashboard/users');
    }

    /**
     * Edit user
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function edit(Request $request, User $user)
    {
        if($request->newpass && $request->newpass2) {

            if($request->newpass == $request->newpass2) {
                $password = bcrypt($request->newpass);
            } else {
                Log::error('Попытка изменить пароль. UserController@edit: user_id' . $user->id);

                $params =  $this->getUser($user->id);
                $params['error'] = 'Не верный пароль подтверждения';

                return view('admin.user.update', $params);
            }
        }

        if($request->isMethod('post')) {
            $password = isset($password) ? $password : $user->password;
            $user = User::find($user->id);

            $user->login = $request->login;
            $user->phone = $request->phone;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->address = $request->address;
            $user->webmoney = $request->webmoney;
            $user->warnings = $request->warnings;
//            $user->yandexmondey = $request->yandexmondey;
            if (Auth::user()->role_id == 1) {
                $user->role_id = $request->role_id;
            }
            $user->password = $password;

            $user->save();
        }

        $params = $this->getUser($user->id);
        $params['success']='Изменения сохранены';

        return view('admin.user.update', $params);
    }

    public function invites()
    {
        $params = [];
        $params['invites'] = Invite::orderBy('id', 'desc')->paginate($this->rows);

        return view('admin.user.invites.index', $params);
    }

    public function createInvite(Request $request)
    {
        if($request->isMethod('post')) {

            $invite = new Invite();

            try {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required'
                ]);

                $originInvite = Invite::where('name', $request->name)
                    ->orWhere('email', $request->email)
                    ->first()
                ;

                if ($originInvite) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('main', 'Инвайт для этого пользователя уже создан.');
                    });
                }

                $originUser = User::where('name', $request->name)
                    ->orWhere('email', $request->email)
                    ->first()
                ;

                if ($originUser) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('main', 'Такой пользователь уже существует');
                    });
                }

                $invite->name = $request->name;
                $invite->email = $request->email;
                $invite->token = md5($request->name.$request->email);

                if ($validator->fails()) {
                    return view('admin.user.invites.create', [
                        'invite' => $invite,
                    ])->withErrors($validator);
                }

                $invite->save();

                return redirect('/dashboard/users/invites');
            }
            catch(\InvalidArgumentException $e) {
                Log::error('Ошибка при создании инвайта UserController@createInvite');
            }

        }

        return view('admin.user.invites.create');
    }

    public function showInvite(Invite $invite)
    {
        return view('admin.user.invites.show', [
            'link' => url('/dashboard/user/create/invite/' . $invite->token)
        ]);
    }

    public function sendInvite(Invite $invite)
    {
        Mail::send('emails.notify.user.invite', [
            'invite' => $invite,
            'link' => url('/dashboard/user/create/invite/' . $invite->token)
        ], function ($m) use ($invite)  {
            $m->from('postmaster@sandbox5c70ae42f8e0431bb3ca35c36dc8067a.mailgun.org', 'CTR BIZ');

            $m->to($invite->email, $invite->name)->subject('New notify!');
        });
    }

    public function registerByInvite($token = null)
    {
        if (!$token) {
            return view('admin._commons.error', [
                'error' => 'Ошибка'
            ]);
        }

        $tokenRow = Invite::where('token', $token)
            ->first()
        ;

        if (!$tokenRow || ($tokenRow && $tokenRow->accepted == 1)) {
            return view('admin._commons.error', [
                'error' => 'Ошибка'
            ]);
        }

        $password = str_random(8);

        $user = new User();
        $user->name = $tokenRow->name;
        $user->login = $tokenRow->name;
        $user->email = $tokenRow->email;
        $user->role_id = 2;
        $user->password = Hash::make($password);
        $user->email = $tokenRow->email;

//        $this->auth->setUser($user);

        $user->save();

        $tokenRow->accepted_at = date('Y-m-d H:i:s');
        $tokenRow->accepted = 1;
        $tokenRow->save();


        if (Auth::attempt(['name' => $tokenRow->name, 'password' => $password])) {
            session([
                'name' => $tokenRow->name
            ]);

            return redirect('/dashboard/user/profile');
        }
    }

    public function profile(Request $request)
    {
        $user = Auth::user();
        $params = $this->getUser($user->id);
        $params['profile_page'] = true;

        if($request->newpass && $request->newpass2) {

            if($request->newpass == $request->newpass2) {
                $password = bcrypt($request->newpass);
            }
            else {
                Log::error('Попытка изменить пароль. UserController@edit ');

                $params =  $this->getUser($user->id);
                $params['error']='Не верный пароль подтверждения';

                return view('admin.user.update', $params);
            }
        }

        if($request->isMethod('post')) {
            $password = isset($password) ? $password : $user->password;
            $user = User::find($user->id);

            $user->login = $request->login;
            $user->phone = $request->phone;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->address = $request->address;
            $user->webmoney = $request->webmoney;
//            $user->yandexmondey = $request->yandexmondey;
            if (Auth::user()->role_id == 1) {
                $user->role_id = $request->role_id;
            }
            $user->password = $password;

            $user->save();

            $params = $this->getUser($user->id);
            $params['profile_page'] = true;

            $params['success']='Изменения сохранены';
        }


        return view('admin.user.update', $params);
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function readNotification(Request $request)
    {
        /** @var DatabaseNotification $notification */
        $notification = DatabaseNotification::where('id', $request->id)
            ->first()
            ->markAsRead()
        ;

        return Response::json(true);
    }

    /**
     * @return mixed
     */
    public function hideSundayNotification()
    {
        $cookie = Cookie::make('hide_sunday_notify', time(), 3600);

        return Response::json(true)->withCookie($cookie);
    }
}
