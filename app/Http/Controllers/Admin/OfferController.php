<?php

namespace App\Http\Controllers\Admin;

use App\Models\Domain;
use App\Models\Landing;
use App\Models\LandingPrice;
use App\Models\LandingUtm;
use App\Models\Links;
use App\Models\OfferUtm;
use App\Repository\LinksRepository;
use App\Role;
use App\Http\Controllers\Helper\Search\SearchTrait;
use App\Models\Offer;
use App\Models\OfferPrice;
use App\Models\Region;
use App\Models\Server;
use App\Services\YandexMetrica;
use DigitalOceanV2\Entity\Droplet;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\View\View;
use App\Http\Controllers\Controller;
use App\Services\ShortcodeParser;

use GrahamCampbell\DigitalOcean\Facades\DigitalOcean;

use Illuminate\Support\Facades\Auth;

use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class AddressController
 * @package App\Http\Controllers\Admin
 */
class OfferController extends Controller
{
    use SearchTrait;

    /**
     * @var string
     */
    private $defaultDomain = 'free-private-cpa.ru';

    /**
     * AddressController constructor.
     */
    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('roles', ['roles' => ['administrator', 'user']]);
	}

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
	{
        if (Auth::user()->role_id == 1) {
            $offers = Offer::with('landings')->with('landings.prices');
        } else {
            $offers = Offer::with('landings')
                ->with('landings.prices')
                ->where('status', Offer::STATUS_OPEN)
            ;
        }

	    $params = [
	        'data' => $offers
                ->orderBy('id', 'desc')
                ->paginate($this->rows)
        ];

		return view('admin.offers.index', $params);
	}

    /**
     * @param Landing|null $landing
     * @param Request $request
     * @return View
     */
	public function editLanding(Landing $landing = null, Request $request)
    {
        if ($request->method() === 'GET' && !is_null($landing)) {
            return view('admin.offers.block.add-landing-modal', [
                'offer' => Offer::where('id', $landing->offer_id)->first(),
                'landing' => $landing,
                'regions' => Region::all()
            ]);
        } elseif ($request->method() === 'POST') {
            $data = $request->request->get('form');
            $offer = Offer::where('id', $data['offer_id'])->first();

            $landing = Landing::where('id', $data['landing_id'])
                ->with('prices')
                ->with('utm')
                ->first()
            ;

            $landing->offer_id = $offer->id;
            $landing->name = $data['name'];
            $landing->hostname = $data['hostname'];
            $landing->params = $data['params'];

            $landing->metrika_counter_id = (int) $data['metrika_counter_id'];
            $landing->metrika_goal_main_id = (int) $data['metrika_goal_main_id'];
            $landing->metrika_goal_activation_id = (int) $data['metrika_goal_activation_id'];
            $landing->metrika_utm_panel = (isset($data['metrika_utm_panel'])) ? $data['metrika_utm_panel'] : null;
            $landing->metrika_utm_wm = (isset($data['metrika_utm_wm'])) ? $data['metrika_utm_wm'] : null;

            LandingPrice::where('landing_id', '=', $landing->id)->delete();
            foreach ($data['prices']['region_id'] as $key => $region) {
                if (!$region) {
                    continue;
                }

                $item = new LandingPrice();
                $item->landing_id = $landing->id;
                $item->region_id = $region;
                $item->price = $data['prices']['price'][$key];
                $item->save();
            }

            LandingUtm::where('landing_id', '=', $landing->id)->delete();
            foreach ($data['utm']['name'] as $key => $name) {
                if (!$name) {
                    continue;
                }

                $item = new LandingUtm();
                $item->landing_id = $landing->id;
                $item->name = $name;
                $item->value = (isset($data['utm']['value'][$key])) ? $data['utm']['value'][$key] : null;
                $item->disabled = (bool) $data['utm']['disabled'][$key];
                $item->save();
            }

            $landing->save();

            return view('admin.offers.block.landings-list', [
                'landings' => Landing::where('offer_id', $offer->id)
                    ->with('prices')
                    ->with('utm')
                    ->get()
            ]);
        }
    }

    /**
     * @param Offer|null $offer
     * @param Request $request
     * @return View
     */
	public function addLanding(Offer $offer = null, Request $request)
    {
        if ($request->method() === 'GET' && !is_null($offer)) {
            return view('admin.offers.block.add-landing-modal', [
                'offer' => $offer,
                'regions' => Region::all()
            ]);
        } elseif ($request->method() === 'POST') {
            $data = $request->request->get('form');
            $offer = Offer::where('id', $data['offer_id'])->first();

            $landing = new Landing();
            $landing->offer_id = $offer->id;
            $landing->name = $data['name'];
            $landing->hostname = $data['hostname'];
            $landing->params = $data['params'];

            $landing->save();

            foreach ($data['prices']['region_id'] as $key => $region) {
                if (!$region) {
                    continue;
                }

                $item = new LandingPrice();
                $item->landing_id = $landing->id;
                $item->region_id = $region;
                $item->price = $data['prices']['price'][$key];
                $item->save();
            }

            foreach ($data['utm']['name'] as $key => $name) {
                if (!$name) {
                    continue;
                }

                $item = new LandingUtm();
                $item->landing_id = $landing->id;
                $item->name = $name;
                $item->value = (isset($data['utm']['value'][$key])) ? $data['utm']['value'][$key] : null;
                $item->disabled = (bool) $data['utm']['disabled'][$key];
                $item->save();
            }

            return view('admin.offers.block.landings-list', [
                'landings' => Landing::where('offer_id', $offer->id)
                    ->with('prices')
                    ->with('utm')
                    ->get()
            ]);
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse|View
     */
	public function create(Request $request)
    {
        $params = [
            'servers' => DigitalOcean::droplet()->getAll(),
            'regions' => Region::all()
        ];

        if ($request->request->all()) {
            $data = $request->request->get('form');

            $multilanding = (isset($data['multilanding'])) ? true : false;

            $offer = new Offer();
            $offer->name = $data['name'];
            $offer->logo = $data['logo'];
            $offer->note = $data['note'];
            $offer->server_id = 1;
            $offer->status = (int) $data['status'];
            $offer->multilanding = $multilanding;

            if (!$multilanding) {
                $offer->hostname = $data['hostname'];
                $offer->path = $data['path'];
                $offer->params = $data['params'];
            }

//            $serverCheck = Server::where('server_id', '=', $data['server_id'])->first();
//            $droplet = DigitalOcean::droplet()->getById($data['server_id']);
//
//            /** @var Droplet $droplet */
//            if ($droplet && $serverCheck) {
//                $offer->server_id = $serverCheck->id;
//            } else {
//                $server = new Server();
//                $server->server_id = $data['server_id'];
//                $server->ip = $droplet->networks[0]->ipAddress;
//                $server->save();
//
//                $offer->server_id = $server->id;
//            }

            $offer->save();

            if (!$multilanding) {
                foreach ($data['prices']['region_id'] as $key => $region) {
                    if (!$region) {
                        continue;
                    }

                    $item = new OfferPrice();
                    $item->offer_id = $offer->id;
                    $item->region_id = $region;
                    $item->price = $data['prices']['price'][$key];
                    $item->save();
                }

                foreach ($data['utm']['name'] as $key => $name) {
                    if (!$name) {
                        continue;
                    }

                    $item = new OfferUtm();
                    $item->offer_id = $offer->id;
                    $item->name = $name;
                    $item->value = (isset($data['utm']['value'][$key])) ? $data['utm']['value'][$key] : null;
                    $item->disabled = (bool) $data['utm']['disabled'][$key];
                    $item->save();
                }
            }

            return redirect()->route('offers');
        }

        return view('admin.offers.create', $params);
    }

    /**
     * @param $id
     * @param Request $request
     * @return View
     */
	public function edit($id, Request $request)
    {
        $offer = Offer::with('server')
            ->with('prices')
            ->with('utm')
            ->findOrFail($id);
        
        $landings = Landing::where('offer_id', $offer->id)
            ->with('prices')
            ->with('utm')
            ->get()
        ;

        $params = [
            'offer' => $offer,
//            'servers' => DigitalOcean::droplet()->getAll(),
            'regions' => Region::all(),
            'landings' => $landings
        ];

        if ($request->request->all()) {
            $data = $request->request->get('form');

            $multilanding = (isset($data['multilanding'])) ? true : false;

            $offer->name = $data['name'];
            $offer->logo = $data['logo'];
            $offer->note = $data['note'];
            $offer->status = (int) $data['status'];
            $offer->multilanding = $multilanding;

            if (!$multilanding) {
                $offer->hostname = $data['hostname'];
                $offer->path = $data['path'];
                $offer->params = $data['params'];

                $offer->metrika_counter_id = (int) $data['metrika_counter_id'];
                $offer->metrika_goal_activation_id = (int) $data['metrika_goal_activation_id'];
                $offer->metrika_utm_panel = (isset($data['metrika_utm_panel'])) ? $data['metrika_utm_panel'] : null;
                $offer->metrika_utm_wm = (isset($data['metrika_utm_wm'])) ? $data['metrika_utm_wm'] : null;
            }

//            if ($offer->server->server_id != $data['server_id']) {
//                /** @var Droplet $droplet */
//                if ($droplet = DigitalOcean::droplet()->getById($data['server_id'])) {
//                    $server = new Server();
//                    $server->server_id = $data['server_id'];
//                    $server->ip = $droplet->networks[0]->ipAddress;
//                    $server->save();
//
//                    $offer->server_id = $server->id;
//                }
//            }

            if (isset($data['prices'])) {
                OfferPrice::where('offer_id', '=', $id)->delete();
                foreach ($data['prices']['region_id'] as $key => $region) {
                    if (!$region) {
                        continue;
                    }

                    $item = new OfferPrice();
                    $item->offer_id = $id;
                    $item->region_id = $region;
                    $item->price = $data['prices']['price'][$key];
                    $item->save();
                }
            }

            if (isset($data['utm'])) {
                OfferUtm::where('offer_id', '=', $id)->delete();
                foreach ($data['utm']['name'] as $key => $name) {
                    if (!$name) {
                        continue;
                    }

                    $item = new OfferUtm();
                    $item->offer_id = $id;
                    $item->name = $name;
                    $item->value = (isset($data['utm']['value'][$key])) ? $data['utm']['value'][$key] : null;
                    $item->disabled = (bool) $data['utm']['disabled'][$key];
                    $item->save();
                }
            }

            $offer->save();

            return redirect()->route('offers');
        }

        return view('admin.offers.create', $params);
    }

    public function delete(Offer $offer)
    {
        return false;
        $currentUser = Auth::user();
        $getAdminRole = Role::where('name', '=', 'Administrator')->first();

        if($currentUser->role_id != $getAdminRole->id)
        {
            return redirect('dashboard/offers')->withErrors('У вас нет прав на удаление');
        }

        try {
            $offer->delete();
        }
        catch(\Exception $e) {
            return redirect('dashboard/offers')->withErrors('У адреса есть связи');
        }

        return redirect('dashboard/offers');
    }

    /**
     * @param Offer $offer
     * @return View
     */
    public function select(Offer $offer)
    {
        $offer = Offer::with('utm')
            ->with('landings')
            ->with('landings.utm')
            ->where('id', $offer->id)
            ->first()
        ;

        $params = [
            'offer' => $offer,
            'ShortcodeParser' => ShortcodeParser::getInstance(),
            'defaultDomain' => $this->defaultDomain,
            'domains' => Domain::where('user_id', '=', Auth::user()->id)
                ->orderBy('id', 'desc')
                ->paginate($this->rows)
        ];

        return view('admin.offers.select', $params);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function createRedirectLink(Request $request)
    {
        $data = $request->request->all();

        if (!isset($data['domain']) || !isset($data['offer'])) {
            return response()->json([
                'message' => 'Ошибка!',
                'status_code' => Response::HTTP_INTERNAL_SERVER_ERROR
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        if ($data['domain'] === $this->defaultDomain) {
            $domain = [
                'name' => $this->defaultDomain
            ];
        } else {
            $domain = Domain::where('id', $data['domain'])->first();

            if (!$domain->moderate) {
                return response()->json([
                    'message' => 'Домен еще не привязан!',
                    'status_code' => Response::HTTP_INTERNAL_SERVER_ERROR
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        $payload = $request->request->all();
        $payload['user'] = Auth::user()->id;

        $token = encrypt(serialize($payload));

        $link = new Links();
        $link->hash = (new LinksRepository())->getUniqueHash();
        $link->token = $token;
        $link->payload = $payload;
        $link->save();

        return response()->json([
            'data' => [
                'token' => $link->hash,
                'domain' => $domain
            ],
            'status_code' => Response::HTTP_OK
        ]);
    }

}
