<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 4/14/17
 * Time: 7:37 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Helper\Search\SearchTrait;
use App\Models\Domain;
use App\Models\Offer;
use App\Models\Landing;
use App\Models\OfferUtm;
use App\Models\StatisticLog;
use App\Models\StatisticLog2;
use App\Models\UserUtm;
use App\Repository\StatisticRepository;
use App\Services\CpaAdapter;
use App\Services\YandexMetrica;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

/**
 * Class AddressController
 * @package App\Http\Controllers\Admin
 */
class ReportController extends Controller
{
    use SearchTrait;

    /**
     * AddressController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roles', ['roles' => ['administrator', 'user']]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function total(Request $request)
    {
        return $this->getStatisticTotal($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchTotal(Request $request)
    {
        return $this->getStatisticTotal($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function getStatisticTotal(Request $request)
    {
        $search = $request->request->get('search', []);
        $startDate = ((isset($search['start-date']) && $search['start-date']) ? $search['start-date'] : null);
        $endDate = ((isset($search['end-date']) && $search['end-date']) ? $search['end-date'] : null);

        $totalApproves = 0;
        $totalByOffer = [];
        $total = CpaAdapter::getTotalByDays(null, $startDate, $endDate);
        $metaOffers = [];
        $metaLandings = [];

        foreach ($total as $rows) {
            foreach ($rows['days'] as $day => $items) {
                if (isset($items['landings'])) {
                    foreach ($items['landings'] as $landingId => $data) {
                        if (!isset($metaLandings[$landingId])) {
                            $metaLandings[$landingId] = Landing::where('id', $landingId)->with('offer')->first();
                        }

                        $landing = $metaLandings[$landingId];

                        if (isset($totalByOffer[$landing->offer_id])) {
                            $totalByOffer[$landing->offer_id]['total_users'] += (int) $data['total'];
                            $totalByOffer[$landing->offer_id]['new_users'] += (int) $data['new'];
                            $totalByOffer[$landing->offer_id]['total_approves'] += (int) $data['goal'];
                            $totalByOffer[$landing->offer_id]['payment_free'] += ((int) (isset($data['price']['available']) ? $data['price']['available'] : 0) + (int) (isset($data['price']['hold']) ? $data['price']['hold'] : 0));
                        } else {
                            $totalByOffer[$landing->offer_id] = [
                                'name' => $landing->offer->name,
                                'status' => $landing->offer->status,
                                'total_users' => (int) $data['total'],
                                'new_users' => (int) $data['new'],
                                'total_approves' => (int) $data['goal'],
                                'payment_free' => ((int) (isset($data['price']['available']) ? $data['price']['available'] : 0) + (int) (isset($data['price']['hold']) ? $data['price']['hold'] : 0))
                            ];
                        }

                        $totalApproves += (int) (isset($data['goal']) ? $data['goal'] : 0);
                    }
                }

                if (isset($items['offers'])) {
                    foreach ($items['offers'] as $offerId => $data) {
                        if (!isset($metaOffers[$offerId])) {
                            $metaOffers[$offerId] = Offer::where('id', $offerId)->first();
                        }

                        $offer = $metaOffers[$offerId];

                        if (isset($totalByOffer[$offer->id])) {
                            $totalByOffer[$offer->id]['total_users'] += (int) (isset($data['total']) ? $data['total'] : 0);
                            $totalByOffer[$offer->id]['new_users'] += (int) (isset($data['new']) ? $data['new'] : 0);
                            $totalByOffer[$offer->id]['total_approves'] += (int) (isset($data['goal']) ? $data['goal'] : 0);
                            $totalByOffer[$offer->id]['payment_free'] += ((int) (isset($data['price']['available']) ? $data['price']['available'] : 0) + (int) (isset($data['price']['hold']) ? $data['price']['hold'] : 0));
                        } else {
                            $totalByOffer[$offer->id] = [
                                'name' => $offer->name,
                                'status' => $offer->status,
                                'total_users' => (int) $data['total'],
                                'new_users' => (int) $data['new'],
                                'total_approves' => (int) $data['goal'],
                                'payment_free' => ((int) (isset($data['price']['available']) ? $data['price']['available'] : 0) + (int) (isset($data['price']['hold']) ? $data['price']['hold'] : 0))
                            ];
                        }

                        $totalApproves += (int) (isset($data['goal']) ? $data['goal'] : 0);
                    }
                }
            }
        }

        if (\Request::ajax()) {
            return \Response::json(
                \View::make('admin.report.total-statistic-list', ['totalByOffer' => $totalByOffer])->render(),
                (count($totalByOffer)) ? 200 : 404
            );
        }

        return view('admin.report.total', [
            'totalApproves' => $totalApproves,
            'totalByOffer' => $totalByOffer,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return $this->getStatistic($request);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->getStatistic($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function getStatistic(Request $request)
    {
        $sort = $request->get('sort', 'offers');
        $search = $request->request->get('search', []);
        $user = (int) ((isset($search['by_user']) && $search['by_user']) ? $search['by_user'] : Auth::user()->id);
        $startDate = ((isset($search['start-date']) && $search['start-date']) ? $search['start-date'] : '01.05.2017');
        $endDate = ((isset($search['end-date']) && $search['end-date']) ? $search['end-date'] : date('d.m.Y'));

        $template = 'admin.report.statistic-list';
        $data = [];

        if ($sort === 'offers') {
            $cache = CpaAdapter::getCacheByUserAndDays($user, $startDate, $endDate);

            if (isset($cache->days)) {
                foreach ($cache->days as $timestamp => $items) {
                    if (isset($items['offers'])) {
                        foreach ($items['offers'] as $offerId => $results) {
                            $offer = Offer::where('id', $offerId)->first();

                            if (isset($data[$offerId])) {
                                $data[$offerId] = [
                                    'name' => $offer->name,
                                    'total_users' => $data[$offerId]['total_users'] + $results['total'],
                                    'new_users' => $data[$offerId]['new_users'] + $results['new'],
                                    'total_approves' => $data[$offerId]['total_approves'] + $results['goal'],
                                    'payment_free' => $data[$offerId]['payment_free'] + ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            } else {
                                $data[$offerId] = [
                                    'name' => $offer->name,
                                    'total_users' => $results['total'],
                                    'new_users' => $results['new'],
                                    'total_approves' => $results['goal'],
                                    'payment_free' => ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            }
                        }
                    }

                    if (isset($items['landings'])) {
                        foreach ($items['landings'] as $landingId => $results) {
                            $landing = Landing::where('id', $landingId)->with('offer')->first();

                            if (isset($data[$landing->offer->id])) {
                                $data[$landing->offer->id] = [
                                    'name' => $landing->offer->name,
                                    'total_users' => $data[$landing->offer->id]['total_users'] + $results['total'],
                                    'new_users' => $data[$landing->offer->id]['new_users'] + $results['new'],
                                    'total_approves' => $data[$landing->offer->id]['total_approves'] + $results['goal'],
                                    'payment_free' => $data[$landing->offer->id]['payment_free'] + ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            } else {
                                $data[$landing->offer->id] = [
                                    'name' => $landing->offer->name,
                                    'total_users' => $results['total'],
                                    'new_users' => $results['new'],
                                    'total_approves' => $results['goal'],
                                    'payment_free' => ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            }
                        }
                    }
                }
            }
        } elseif ($sort === 'days') {
            $cache = CpaAdapter::getCacheByUserAndDays($user, $startDate, $endDate);

            if (isset($cache->days)) {
                foreach ($cache->days as $timestamp => $items) {
                    if (isset($items['landings'])) {
                        foreach ($items['landings'] as $landingId => $results) {
//                            $landing = Landing::where('id', $landingId)->first();

                            if (isset($data[$timestamp])) {
                                $data[$timestamp] = [
                                    'name' => date('d.m.Y', $timestamp),
                                    'total_users' => $data[$timestamp]['total_users'] + $results['total'],
                                    'new_users' => $data[$timestamp]['new_users'] + $results['new'],
                                    'total_approves' => $data[$timestamp]['total_approves'] + $results['goal'],
                                    'payment_free' => $data[$timestamp]['payment_free'] + ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            } else {
                                $data[$timestamp] = [
                                    'name' => date('d.m.Y', $timestamp),
                                    'total_users' => $results['total'],
                                    'new_users' => $results['new'],
                                    'total_approves' => $results['goal'],
                                    'payment_free' => ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            }
                        }
                    }

                    if (isset($items['offers'])) {
                        foreach ($items['offers'] as $offerId => $results) {
//                            $offer = Offer::where('id', $offerId)->first();

                            if (isset($data[$timestamp])) {
                                $data[$timestamp] = [
                                    'name' => date('d.m.Y', $timestamp),
                                    'total_users' => $data[$timestamp]['total_users'] + $results['total'],
                                    'new_users' => $data[$timestamp]['new_users'] + $results['new'],
                                    'total_approves' => $data[$timestamp]['total_approves'] + $results['goal'],
                                    'payment_free' => $data[$timestamp]['payment_free'] + ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            } else {
                                $data[$timestamp] = [
                                    'name' => date('d.m.Y', $timestamp),
                                    'total_users' => $results['total'],
                                    'new_users' => $results['new'],
                                    'total_approves' => $results['goal'],
                                    'payment_free' => ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            }
                        }
                    }
                }
            }
        } elseif ($sort === 'landings') {
            $cache = CpaAdapter::getCacheByUserAndDays($user, $startDate, $endDate);

            if (isset($cache->days)) {
                foreach ($cache->days as $timestamp => $items) {
                    if (isset($items['landings'])) {
                        foreach ($items['landings'] as $landingId => $results) {
                            $landing = Landing::where('id', $landingId)->first();

                            if (isset($data[$landingId])) {
                                $data[$landingId] = [
                                    'name' => $landing->name,
                                    'total_users' => $data[$landingId]['total_users'] + $results['total'],
                                    'new_users' => $data[$landingId]['new_users'] + $results['new'],
                                    'total_approves' => $data[$landingId]['total_approves'] + $results['goal'],
                                    'payment_free' => $data[$landingId]['payment_free'] + ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            } else {
                                $data[$landingId] = [
                                    'name' => $landing->name,
                                    'total_users' => $results['total'],
                                    'new_users' => $results['new'],
                                    'total_approves' => $results['goal'],
                                    'payment_free' => ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            }
                        }
                    }
                }
            }
        } elseif (strpos($sort, 'utm_') !== false) {
            $template = 'admin.report.statistic-utm-list';
            $cache = CpaAdapter::getCacheByUserAndDays($user, $startDate, $endDate);

            if (isset($cache->days)) {
                foreach ($cache->days as $timestamp => $items) {
                    if (isset($items['landings'])) {
                        foreach ($items['landings'] as $landingId => $results) {
                            if (isset($results[$sort])) {
                                foreach ($results[$sort] as $utmName => $utmResult) {
                                    if (isset($data[$utmName])) {
                                        $data[$utmName] = [
                                            'name' => $utmName,
                                            'total_users' => (int) $data[$utmName]['total_users'] + (int) $utmResult['total'],
                                            'new_users' => (int) $data[$utmName]['new_users'] + (int) $utmResult['new'],
                                            'total_approves' => (int) $data[$utmName]['total_approves'] + (int) $utmResult['goal']
                                        ];
                                    } else {
                                        $data[$utmName] = [
                                            'name' => $utmName,
                                            'total_users' => $utmResult['total'],
                                            'new_users' => $utmResult['new'],
                                            'total_approves' => $utmResult['goal']
                                        ];
                                    }
                                }
                            }
                        }
                    }

                    if (isset($items['offers'])) {
                        foreach ($items['offers'] as $landingId => $results) {
                            if (isset($results[$sort])) {
                                foreach ($results[$sort] as $utmName => $utmResult) {
                                    if (isset($data[$utmName])) {
                                        $data[$utmName] = [
                                            'name' => $utmName,
                                            'total_users' => (int) $data[$utmName]['total_users'] + (int) $utmResult['total'],
                                            'new_users' => (int) $data[$utmName]['new_users'] + (int) $utmResult['new'],
                                            'total_approves' => (int) $data[$utmName]['total_approves'] + (int) $utmResult['goal']
                                        ];
                                    } else {
                                        $data[$utmName] = [
                                            'name' => $utmName,
                                            'total_users' => $utmResult['total'],
                                            'new_users' => $utmResult['new'],
                                            'total_approves' => $utmResult['goal']
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        if (\Request::ajax()) {
            return \Response::json(
                \View::make($template, compact('data', 'sort'))->render(),
                (count($data)) ? 200 : 404
            );
        }

        $params = [
            'data' => $data,
            'sort' => $sort,
            'offers' => Offer::where('status', Offer::STATUS_OPEN)->get(),
            'users' => User::all()
        ];

        return view('admin.report.index', $params);
    }

}
