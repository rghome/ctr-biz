<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * Class AdminController
 * @package App\Http\Controllers\Admin
 */
class AdminController extends Controller
{
    /**
     * AdminController constructor.
     */
    public function __construct()
	{
		$this->middleware('auth');
	}

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
	{
        return redirect()->route('statistic');
	}
}
