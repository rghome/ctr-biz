<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Helper\Search\SearchTrait;
use App\Models\Offer;
use App\Models\Region;
use App\Models\Server;
use DigitalOceanV2\Entity\Droplet;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use App\Http\Controllers\Controller;

use GrahamCampbell\DigitalOcean\Facades\DigitalOcean;


/**
 * Class AddressController
 * @package App\Http\Controllers\Admin
 */
class RegionController extends Controller
{
    use SearchTrait;

    /**
     * AddressController constructor.
     */
    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('roles', ['roles' => ['administrator', 'manager']]);
	}

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
	{
	    $params = [
	        'data' => Region::orderBy('id', 'desc')->paginate($this->rows)
        ];

		return view('admin.regions.index', $params);
	}

    /**
     * @param Request $request
     * @return RedirectResponse|View
     */
	public function create(Request $request)
    {
        if ($request->request->all()) {
            $data = $request->request->get('form');

            $offer = new Region();
            $offer->name = $data['name'];
            $offer->code = $data['code'];
            $offer->currency = $data['currency'];
            $offer->save();

            return redirect()->route('regions');
        }

        return view('admin.regions.create');
    }

	public function edit($id, Request $request)
    {
        $region = Region::findOrFail($id);

        $params = [
            'region' => $region
        ];

        if ($request->request->all()) {
            $data = $request->request->get('form');

            $region->name = $data['name'];
            $region->code = $data['code'];
            $region->currency = $data['currency'];
            $region->save();
        }

        return view('admin.regions.create', $params);
    }

    public function delete()
    {

    }

}
