<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Helper\Search\SearchTrait;
use App\Models\Domain;
use App\Models\Landing;
use App\Models\Offer;
use App\Models\StatisticLog;
use App\Models\StatisticLog2;
use App\Models\UserUtm;
use App\Repository\StatisticRepository;
use App\Services\CpaAdapter;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

/**
 * Class AddressController
 * @package App\Http\Controllers\Admin
 */
class StatisticController extends Controller
{
    use SearchTrait;

    /**
     * AddressController constructor.
     */
    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('roles', ['roles' => ['administrator', 'user']]);
	}

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
	{
        return $this->getStatistic($request);
	}

    /**
     * @param Request $request
     * @return mixed
     */
	public function search(Request $request)
    {
        return $this->getStatistic($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function getStatistic(Request $request)
    {
        $sort           = $request->get('sort', 'days');
        $search         = $request->request->get('search', []);
        $user           = (int) ((isset($search['by_user']) && $search['by_user']) ? $search['by_user'] : Auth::user()->id);
        $startDate      = ((isset($search['start-date']) && $search['start-date']) ? $search['start-date'] : '');
        $endDate        = ((isset($search['end-date']) && $search['end-date']) ? $search['end-date'] : '');
        $offerFilter    = isset($search['by_offer']) ? Offer::where('id', $search['by_offer'])->first() : null;

        $template = 'admin.statistic.statistic-list';
        $data = [];
        $chart = [];

        if ($sort === 'offers') {
            $cache = CpaAdapter::getCacheByUserAndDays($user, $startDate, $endDate);

            if (isset($cache->days)) {
                foreach ($cache->days as $timestamp => $items) {
                    if (isset($items['offers'])) {
                        foreach ($items['offers'] as $offerId => $results) {
                            $offer = Offer::where('id', $offerId)->first();

                            if (isset($data[$offerId])) {
                                $data[$offerId] = [
                                    'name' => $offer->name,
                                    'total_users' => $data[$offerId]['total_users'] + (isset($results['total']) ? $results['total'] : 0),
                                    'new_users' => $data[$offerId]['new_users'] + (isset($results['new']) ? $results['new'] : 0),
                                    'total_approves' => $data[$offerId]['total_approves'] + (isset($results['goal']) ? $results['goal'] : 0),
                                    'payment_free' => $data[$offerId]['payment_free'] + ((isset($results['price']['available']) ? $results['price']['available'] : 0) + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            } else {
                                $data[$offerId] = [
                                    'name' => $offer->name,
                                    'total_users' => $results['total'],
                                    'new_users' => $results['new'],
                                    'total_approves' => $results['goal'],
                                    'payment_free' => ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            }
                        }
                    }

                    if (isset($items['landings'])) {
                        foreach ($items['landings'] as $landingId => $results) {
                            $landing = Landing::where('id', $landingId)->with('offer')->first();

                            if (isset($data[$landing->offer->id])) {
                                $data[$landing->offer->id] = [
                                    'name' => $landing->offer->name,
                                    'total_users' => $data[$landing->offer->id]['total_users'] + $results['total'],
                                    'new_users' => $data[$landing->offer->id]['new_users'] + $results['new'],
                                    'total_approves' => $data[$landing->offer->id]['total_approves'] + $results['goal'],
                                    'payment_free' => $data[$landing->offer->id]['payment_free'] + ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            } else {
                                $data[$landing->offer->id] = [
                                    'name' => $landing->offer->name,
                                    'total_users' => $results['total'],
                                    'new_users' => $results['new'],
                                    'total_approves' => $results['goal'],
                                    'payment_free' => ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            }
                        }
                    }
                }
            }

            if ($offerFilter !== null) {
                $tmp[$offerFilter->id] = $data[$offerFilter->id] ?: [];
                $data = $tmp;
            }

            if (!empty($data)) {
                ksort($data);

                foreach ($data as $offerId => $item) {
                    $chart['data']['category'] = 'Офферы';
                    $chart['data']['column-' . $offerId] = $item['total_approves'];

                    $chart['graphs'][$offerId] = [
                        'id' => 'column-' . $offerId,
                        'offer' => $offerId,
                        'name' => $item['name']
                    ];
                }
            }
        } elseif ($sort === 'days') {
            $cache = CpaAdapter::getCacheByUserAndDays($user, $startDate, $endDate);

            if (isset($cache->days)) {
                foreach ($cache->days as $timestamp => $items) {
                    if (isset($items['landings'])) {
                        foreach ($items['landings'] as $landingId => $results) {
                            $landing = Landing::where('id', $landingId)->with('offer')->first();

                            if ($offerFilter !== null && !($landing->offer->id == $offerFilter->id)) {
                                continue;
                            }

                            if (isset($data[$timestamp])) {
                                $data[$timestamp] = [
                                    'name' => date('d.m.Y', $timestamp),
                                    'total_users' => $data[$timestamp]['total_users'] + $results['total'],
                                    'new_users' => $data[$timestamp]['new_users'] + $results['new'],
                                    'total_approves' => $data[$timestamp]['total_approves'] + $results['goal'],
                                    'payment_free' => $data[$timestamp]['payment_free'] + ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            } else {
                                $data[$timestamp] = [
                                    'name' => date('d.m.Y', $timestamp),
                                    'total_users' => $results['total'],
                                    'new_users' => $results['new'],
                                    'total_approves' => $results['goal'],
                                    'payment_free' => ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            }
                        }
                    }

                    if (isset($items['offers'])) {
                        foreach ($items['offers'] as $offerId => $results) {
                            $offer = Offer::where('id', $offerId)->first();

                            if ($offerFilter !== null && !($offer->id == $offerFilter->id)) {
                                continue;
                            }

                            if (isset($data[$timestamp])) {
                                $data[$timestamp] = [
                                    'name' => date('d.m.Y', $timestamp),
                                    'total_users' => $data[$timestamp]['total_users'] + $results['total'],
                                    'new_users' => $data[$timestamp]['new_users'] + $results['new'],
                                    'total_approves' => $data[$timestamp]['total_approves'] + $results['goal'],
                                    'payment_free' => $data[$timestamp]['payment_free'] + ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            } else {
                                $data[$timestamp] = [
                                    'name' => date('d.m.Y', $timestamp),
                                    'total_users' => $results['total'],
                                    'new_users' => $results['new'],
                                    'total_approves' => $results['goal'],
                                    'payment_free' => ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            }
                        }
                    }
                }
            }

            if (!empty($data)) {
                ksort($data);

                foreach ($data as $timestamp => $day) {
                    $chart[] = [
                        'date' => date('Y-m-d', $timestamp),
                        'total_approves' => $day['total_approves'],
                        'payment_free' => $day['payment_free']
                    ];
                }

                krsort($data);
            }
        } elseif ($sort === 'landings') {
            $cache = CpaAdapter::getCacheByUserAndDays($user, $startDate, $endDate);

            if (isset($cache->days)) {
                foreach ($cache->days as $timestamp => $items) {
                    if (isset($items['landings'])) {
                        foreach ($items['landings'] as $landingId => $results) {
                            $landing = Landing::where('id', $landingId)->with('offer')->first();

                            if ($offerFilter !== null && !($landing->offer->id == $offerFilter->id)) {
                                continue;
                            }

                            if (isset($data[$landingId])) {
                                $data[$landingId] = [
                                    'name' => $landing->name,
                                    'offer_name' => $landing->offer->name,
                                    'total_users' => $data[$landingId]['total_users'] + $results['total'],
                                    'new_users' => $data[$landingId]['new_users'] + $results['new'],
                                    'total_approves' => $data[$landingId]['total_approves'] + $results['goal'],
                                    'payment_free' => $data[$landingId]['payment_free'] + ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            } else {
                                $data[$landingId] = [
                                    'name' => $landing->name,
                                    'offer_name' => $landing->offer->name,
                                    'total_users' => $results['total'],
                                    'new_users' => $results['new'],
                                    'total_approves' => $results['goal'],
                                    'payment_free' => ($results['price']['available'] + (isset($results['price']['hold']) ? $results['price']['hold'] : 0))
                                ];
                            }
                        }
                    }
                }
            }

            if (!empty($data)) {
                ksort($data);

                foreach ($data as $offerId => $item) {
                    $chart['data']['category'] = 'Лендинги';
                    $chart['data']['column-' . $offerId] = $item['total_approves'];

                    $chart['graphs'][$offerId] = [
                        'id' => 'column-' . $offerId,
                        'offer' => $offerId,
                        'name' => $item['name']
                    ];
                }
            }
        } elseif (strpos($sort, 'utm_') !== false) {
            $template = 'admin.statistic.statistic-utm-list';

            $cache = CpaAdapter::getCacheByUserAndDays($user, $startDate, $endDate);

            if (isset($cache->days)) {
                foreach ($cache->days as $timestamp => $items) {
                    if (isset($items['landings'])) {
                        foreach ($items['landings'] as $landingId => $results) {
                            $landing = Landing::where('id', $landingId)->with('offer')->first();

                            if ($offerFilter !== null && !($landing->offer->id == $offerFilter->id)) {
                                continue;
                            }

                            if (isset($results[$sort])) {
                                foreach ($results[$sort] as $utmName => $utmResult) {
                                    if (isset($data[$utmName])) {
                                        $data[$utmName] = [
                                            'name' => $utmName,
                                            'offer_name' => $landing->offer->name,
                                            'total_users' => (int) $data[$utmName]['total_users'] + (int) $utmResult['total'],
                                            'new_users' => (int) $data[$utmName]['new_users'] + (int) $utmResult['new'],
                                            'total_approves' => (int) $data[$utmName]['total_approves'] + (int) $utmResult['goal']
                                        ];
                                    } else {
                                        $data[$utmName] = [
                                            'name' => $utmName,
                                            'offer_name' => $landing->offer->name,
                                            'total_users' => $utmResult['total'],
                                            'new_users' => $utmResult['new'],
                                            'total_approves' => $utmResult['goal']
                                        ];
                                    }
                                }
                            }
                        }
                    }

                    if (isset($items['offers'])) {
                        foreach ($items['offers'] as $offerId => $results) {
                            $offer = Offer::where('id', $offerId)->first();

                            if ($offerFilter !== null && !($offer->id == $offerFilter->id)) {
                                continue;
                            }

                            if (isset($results[$sort])) {
                                foreach ($results[$sort] as $utmName => $utmResult) {
                                    if (isset($data[$utmName])) {
                                        $data[$utmName] = [
                                            'name' => $utmName,
                                            'offer_name' => $offer->name,
                                            'total_users' => (int) $data[$utmName]['total_users'] + (int) $utmResult['total'],
                                            'new_users' => (int) $data[$utmName]['new_users'] + (int) $utmResult['new'],
                                            'total_approves' => (int) $data[$utmName]['total_approves'] + (int) $utmResult['goal']
                                        ];
                                    } else {
                                        $data[$utmName] = [
                                            'name' => $utmName,
                                            'offer_name' => $offer->name,
                                            'total_users' => $utmResult['total'],
                                            'new_users' => $utmResult['new'],
                                            'total_approves' => $utmResult['goal']
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!empty($data)) {
                ksort($data);

                foreach ($data as $utmName => $item) {
                    $chart['data']['category'] = ($sort === 'utm_term') ? 'Потоки' : 'Источники трафика';
                    $chart['data']['column-' . $utmName] = $item['total_approves'];

                    $chart['graphs'][$utmName] = [
                        'id' => 'column-' . $utmName,
                        'offer' => $utmName,
                        'name' => $item['name']
                    ];
                }
            }
        }

        $chart = \GuzzleHttp\json_encode($chart);

        if (\Request::ajax()) {
            return \Response::json(
                \View::make($template, compact('data', 'sort', 'chart'))->render(),
                (count($data)) ? 200 : 404
            );
        }

        $params = [
            'data' => $data,
            'sort' => $sort,
            'chart' => $chart,
            'offers' => Offer::get(),
            'users' => User::all()
        ];

        return view('admin.statistic.index', $params);
    }

}
