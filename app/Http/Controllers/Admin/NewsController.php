<?php

namespace App\Http\Controllers\Admin;

use App\Models\News;
use App\Notifications\NewNews;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * Class NewsController
 * @package App\Http\Controllers\Admin
 */
class NewsController extends Controller
{
    /**
     * AddressController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roles', ['roles' => ['administrator', 'user']]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = News::all();

        return view('admin.news.index', compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if ($request->request->all()) {
            $data = $request->request->get('form');

            $news = new News();
            $news->title = trim($data['title']);
//            $news->image = trim($data['image']);
            $news->text = trim($data['text']);
            $news->save();

            $users = User::whereNotNull('remember_token')->get();
            foreach ($users as $user) {
                $user->notify(new NewNews());
            }

            return redirect()->route('news');
        }

        return view('admin.news.create');
    }
}
