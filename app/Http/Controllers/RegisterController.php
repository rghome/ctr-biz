<?php

namespace App\Http\Controllers;


use App\Role;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
	public function ajax(Request $request)
    {
		$formData = $request->input();


	    $validator = Validator::make($request->all(), [
		    'login' => 'required|unique:users|max:255|login',
		    'password' => 'required|min:1|confirmed',
		    'password_confirmation' => 'required|min:1',
	    ]);


	    if($validator->fails()){


		    $result['message'][] = $validator->messages()->first();
		    $result['valid'] = false;

	    }else
	    {
		    $result['valid'] = true;
		    $user =User::create([
			    'login' => $formData['login'],
			    'password' => bcrypt($formData['password']),
			    'role_id' => Role::where(['name' => 'User'])->first()->id
		    ]);





		    if (Auth::attempt(['login' => $formData['login'], 'password' => $formData['password']], true))
		    {

			    Session::put('userId', $user->id);
			    Cookie::queue('userId', $user->id, 31556926);
			    $result['red_url'] = '/';
		    }
	    }


	    //print_r($formData);

	    return response()->json($result);
    }

	public function loginAjax(Request $request)
	{
		$formData = $request->all();

		if (Auth::attempt(['login' => $formData['login'], 'password' => $formData['password']], true))
		{
			return response('ok', 200);
		}
		return response('error', 403);
	}
}
