<?php

namespace App\Http\Controllers\Helper\Search;


/**
 * Class SearchTrait
 * @package App\Http\Controllers\Helper\Search
 */
trait SearchTrait
{
    /**
     * @var int
     */
    private $rows = 20;
}