<?php


namespace App\Http\Controllers\Helper\Filter;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

/**
 * Class FilterTrait
 * @package App\Http\Controllers\Helper\Filter
 */
trait FilterTrait
{
	/**
	 * @param $model
	 *
	 * @return bool
	 */
	public function checkModel($model)
	{
		if (!method_exists($this, 'allowModels')) {
			return false;
		}

		if (empty($this->allowModels())) {
			return false;
		}

		if (!in_array($model, $this->allowModels())) {
			return false;
		}

		return true;
	}

	/**
	 * @param array $data
	 * @param bool $returnPlain
	 *
	 * @return mixed
	 */
	protected function doFilter($data, $returnPlain = false)
	{
		$validator = \Validator::make($data, [
				'model' => 'required',
				'key' => 'required',
				'value' => 'required',
			]
		);

		if ($validator->fails()) {
			return $this->filterResponse($validator->getMessageBag(), $returnPlain);
		}

		if ($this->checkModel($data['model'])) {
			$model = 'App\Models\\' . $data['model'];
			$rows = $model::where($data['key'], $data['value'])->get();

			return $this->filterResponse($rows, $returnPlain);
		}

		$validator->getMessageBag()->add('filter_model_not_allow', 'filter_model_not_allow');
		return $this->filterResponse($validator->getMessageBag(), $returnPlain);
	}

	/**
	 * @param Request $request
	 *
	 * @return bool
	 */
	public function filter(Request $request)
	{
		if (!$request->ajax()) return false;

		return $this->doFilter($request->all());
	}

	/**
	 * @param $data
	 * @param bool $plain
	 *
	 * @return mixed
	 */
	public function filterResponse($data,  $plain = false)
	{
		if ($plain) {
			return $data;
		}

		if ($data instanceof Collection) {
			return \Response::json($data);
		}

		if ($data instanceof MessageBag) {
			return \Response::json($data, 500);
		}
	}
}