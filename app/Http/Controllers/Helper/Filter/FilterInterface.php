<?php


namespace App\Http\Controllers\Helper\Filter;

/**
 * Interface FilterInterface
 * @package App\Http\Controllers\Helper\Filter
 */
interface FilterInterface
{
	/**
	 * @return array
	 */
	public function allowModels();

	/**
	 * @param string $model
	 *
	 * @return bool
	 */
	public function checkModel($model);
}