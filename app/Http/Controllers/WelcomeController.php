<?php namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\City;
use App\Models\District;
use App\Models\MemberCooperative;
use App\Models\P1;
use App\Models\PhysicalPerson;
use App\Models\Reg3;
use App\Models\Region;
use App\Models\Street;
use App\Models\Yard;
use App\Models\LoadData;
use Illuminate\Database\QueryException;
use Mockery\CountValidator\Exception;

use DB;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('welcome');
	}

	public $a = array(
		'Шевченка',
		'Грибоєдова',
		'Мира',
		'Обухова',
		'Жовтнева',
		'Богдана Хмельницького',
		'Київська',
		'Енгельса',
		'Чубенко',
	);

	public function import2()
	{
//		$data = LoadData::limit(10)->get();

		@set_time_limit ( 6000000 );

		$data = LoadData::all();

		foreach ($data as $value) {
			list($region, $district, $city) = explode(';', $value->test);

			if (
				strpos($region, ".") !== false || strrpos($region," ")
				|| strpos($district, ".") !== false || strrpos($district," ")
			) {
//				echo $region . '<br/>';
				continue;
			}

			$regionDB = Region::where('name', '=', $region)->first();
			if (!$regionDB) {
				$newRegion = new Region();
				$newRegion->name = $region;
				$newRegion->save();

				$regionId = $newRegion->id;
			} else {
				$regionId = $regionDB->id;
			}

			$districtDB = District::where('name', '=', $district)->first();
			if (!$districtDB) {
				$newDistrict = new District();
				$newDistrict->name = $district;
				$newDistrict->region_id = $regionId;
				$newDistrict->save();

				$districtId = $newDistrict->id;
			} else {
				$districtId = $districtDB->id;
			}

			$cityDB = City::where('name', '=', $city)->first();
			if (!$cityDB) {
				$newCity = new City();
				$newCity->name = $this->mb_ucfirst(mb_strtolower($city), 'UTF-8');
				$newCity->district_id = $districtId;
				$newCity->save();
			}
		}

		$this->import3();
	}

	public function import3()
	{
//		$yards = Yard::all();
//
//		$cache = [];
//		$updatedAddress = [];
//		foreach ($yards as $yard) {
//			$city = City::inRandomOrder()->first();
//
//			if (isset($cache[$city->id])) continue;
//
//			$address = Address::where('id', $yard->address_id);
//
//			$address->update(
//				[
//					'city_id' => $city->id,
//				]
//			);
//
//			$address = $address->first();
//
//			Street::where('id', $address->id)->update(
//				[
//					'city_id' => $city->id,
//				]
//			);
//
//			$updatedAddress[] = $city->id;
//
//			$cache[$city->id] = true;
//		}
//
//		Address::whereNotIn('city_id', $updatedAddress)->delete();
//
//		DB::update(DB::raw("UPDATE addresses SET street_id  = (SELECT id FROM streets ORDER BY RAND() LIMIT 1 );"));
//
//		$filled = explode(', ', 'Рівненська, Хмельницька, Житомирська, Чернівецька, Миколаївська, Одеська, Херсонська');
//
//		$pps = PhysicalPerson::all();
//
//		foreach ($pps as $pp) {
//			PhysicalPerson::where('id', $pp->id)->update([
//				'yard_id' => Yard::whereHas('address.cities.district.region', function ($query) use ($filled) {
//					$query->where('name', $filled[array_rand($filled, 1)]);
//				})->first()->id
//			]);
//		}

		$streets = Street::all();
		foreach ($streets as $street) {
			Address::where('street_id', $street->id)->update([
				'city_id' => $street->city_id
			]);
		}
	}

	private function mb_ucfirst($string, $enc = 'UTF-8')
	{
		return mb_strtoupper(mb_substr($string, 0, 1, $enc), $enc) .
		mb_substr($string, 1, mb_strlen($string, $enc), $enc);
	}

	public function import()
	{
		$this->reg();

		$ps = P1::all();
		$n = 0;
		foreach ($ps as $p)
		{
			$city = City::where('code', '=', $p->city)->first();
			if(!isset($city->id))
			{
				$city = City::all()->random(1);
				print_r($p->city.'<br>');
				//continue;
			}

			$sname =  $this->a[rand(0,8)];
			$street = Street::where('name', '=', $sname)->first();
			if(!isset($street->id))
			{
				$street = new Street();
				$street->name = $sname;
				$street->city_id = $city->id;
				$street->save();
			}

			$adr = new Address();
			$adr->city_id =  $city->id;
			$adr->street_id =  $street->id;
			$adr->house =  $n;
			$adr->save();

			$y = new Yard();
			$y->address_id = $adr->id;
			$y->save();

			$p->inn = trim(explode(',',$p->inn)[0]);
			try{
				$names = explode(' ', $p->kname);
				$persone = new PhysicalPerson();
				$persone->firstname = $names[1];
				$persone->lastname = $names[0];
				$persone->middlename = $names[3];
				$persone->inn = $p->inn;
				$persone->yard_id = $y->id;
				$persone->phone = $p->phone ? $p->phone : '+380678641213';
				$persone->save();

				if(isset($persone->id))
				{
					$mc = new MemberCooperative();
					$mc->inn = $persone->id;
					$mc->image = $p->inn.'.jpg';
					$mc->phone = $persone->phone;
					$mc->admin = 1;
					$mc->manager = 1;
					$mc->address_register = '';
					$mc->passport_number = $this->l[rand(0,10)].$this->l[rand(0,10)].rand(100000,999999);
					$mc->save();
				}

			}catch (QueryException $ex)
			{
				print_r($ex->getMessage());
			}


		}
	}
	function trim_all( $str , $what = NULL , $with = ' ' )
	{
		if( $what === NULL )
		{
			//  Character      Decimal      Use
			//  "\0"            0           Null Character
			//  "\t"            9           Tab
			//  "\n"           10           New line
			//  "\x0B"         11           Vertical Tab
			//  "\r"           13           New Line in Mac
			//  " "            32           Space

			$what   = "\\x00-\\x20";    //all white-spaces and control chars
		}

		return trim( preg_replace( "/[".$what."]+/" , $with , $str ) , $what );
	}

	public $l = array(
		'A',
		'Б',
		'В',
		'Г',
		'Д',
		'К',
		'Н',
		'Ш',
		'Ф',
		'Р',
		'О',
		'Т',
	);

	public function reg()
	{
		$regs = Reg3::all();
		foreach ($regs as $reg)
		{
			$region = Region::where('name', '=', $reg->name_obl)->first();
			if(!isset($region->id))
			{
				$region = new Region();
				$region->name = $reg->name_obl;
				$region->save();
			}

			$district = District::where('name', '=', $reg->name_r)->first();
			if(!isset($district->id))
			{
				$district = new District();
				$district->name = $reg->name_r;
				$district->region_id = $region->id;
				$district->save();
			}

			$city = City::where('name', '=', $reg->name_city)->first();
			if(!isset($city->id))
			{
				$city = new City();
				$city->name = $reg->name_city;
				$city->district_id	 = $district->id;
				$city->code	 = $reg->code_city;
				$city->save();
			}
		}
	}

}
