<?php

namespace App\Http\Controllers\Api;

use App\Models\PaymentLog;
use App\Models\StatisticLog;
use Illuminate\Support\Facades\Crypt;
use Mail;
use App\Models\Domain;
use App\Role;
use App\Http\Controllers\Helper\Search\SearchTrait;
use App\Models\Offer;
use App\Models\OfferPrice;
use App\Models\Region;
use App\Models\Server;
use DigitalOceanV2\Entity\Droplet;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use App\Http\Controllers\Controller;

use GrahamCampbell\DigitalOcean\Facades\DigitalOcean;

use Illuminate\Support\Facades\Auth;
use MongoDB\BSON\UTCDateTime;

/**
 * Class ApiController
 * @package App\Http\Controllers\Admin
 */
class ApiController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
	{
	    $token = $request->get('token');

	    if ($token) {
	        $parsed = json_decode(base64_decode($token));
	        $payload = unserialize(decrypt($parsed->token));

	        $domain = Domain::where('id', '=', $payload['domain'])
                ->first()
            ;

	        if (!$domain) {
                return $this->json(false);
            }

            if ((int) $parsed->page === 3) {
                $this->removeWaiting($parsed, $payload);
            }

            return $this->createLog($parsed, $domain, $payload);
        } else {
	        $this->logError();
        }

        return $this->json(true);
	}

    /**
     * @param $parsed
     * @param $payload
     */
	private function removeWaiting($parsed, $payload)
    {
        try {
            StatisticLog::where('offer_id', (int) $payload['offer'])
                ->where('ip.ip', $parsed->ip->ip)
                ->where('page', 2)
                ->where('user_id', (int) $payload['user'])
                ->delete()
            ;
        } catch (\Exception $e) {

        }
    }

    /**
     * @param $parsed
     * @param $domain
     * @param $payload
     * @return \Illuminate\Http\JsonResponse
     */
	private function createLog($parsed, $domain, $payload)
    {
        $log = new StatisticLog();
        $log->hostname = $parsed->location->hostname;
        $log->domain_id = (int) $domain->id;
        $log->user_id = (int) $payload['user'];
        $log->offer_id = (int) $payload['offer'];
        $log->location = $parsed->location;
        $log->ip = $parsed->ip;
        $log->navigator = $parsed->navigator;
        $log->page = $parsed->page;
        $log->payment_status = PaymentLog::status_type[1];
        $log->date = new UTCDateTime((new \DateTime())->getTimestamp()*1000);

        if (property_exists($parsed, 'utm')) {
            foreach ((array) $parsed->utm as $key => $value) {
                if ($value) {
                    $log->{$key} = $value;
                }
            }
        }


        $log->save();

        return $this->json(true);
    }

    private function updateLog(StatisticLog $log, $parsed)
    {
        $log->push('track', $this->fillTrack($parsed));
        $log->save();

        return $this->json(true);
    }

    private function fillTrack($parsed)
    {
        $data = [
            'location' => $parsed->location,
            'ip' => $parsed->ip,
            'navigator' => $parsed->navigator,
            'page' => $parsed->page,
            'payment_status' => PaymentLog::status_type[1],
//            'date' => new UTCDateTime((new \DateTime('- 2 days'))->getTimestamp()*1000)
            'date' => new UTCDateTime((new \DateTime())->getTimestamp()*1000)
        ];

        foreach ((array) $parsed->utm as $key => $value) {
            if ($value) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

	private function logError()
    {

    }

    private function json($status)
    {
        return response()->json(['status' => $status]);
    }

}
