<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends Controller {

	/**
	 * Handle an authentication attempt.
	 *
	 * @return Response
	 */
	public function login(Request $request)
	{
		if($request->all())
		{
			if (Auth::attempt(['login' => $request->get('login'), 'password' => $request->get('password')], $request->has('remember'))) {
				// перекидывает сразу в админку
				return redirect()->route('statistic');
			}
			else {
				return redirect('/login')->withErrors('Неправильный логин или пароль');
			}

		}
		return view('auth.login');
	}

	/**
	 * @param Request $request
	 * @param $lang
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function switchLanguage(Request $request, $lang)
	{
		session()->put('locale', $lang);

		return redirect()->back();
	}

	public function logout(Request $request)
	{
		Auth::logout();
		return redirect('/');
	}
}