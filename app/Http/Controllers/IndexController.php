<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Admin\AddressController;
use App\Models\Address;
use App\Models\Contract;
use App\Models\HappyClientSlide;
use App\Models\DeviceCategory;
use App\Models\DeviceMaterial;
use App\Models\Device;

use App\Models\MemberCooperative;
use App\Models\P1;
use App\Models\PhysicalPerson;
use App\Models\Program;
use App\Models\Reg3;
use App\Models\TestRegion;
use App\Repository\DashboardRepository;
use App\Repository\MCResourceRepository;
use App\Repository\MemberRepository;
use App\Repository\ProductRepository;
use File;

use \Torann\GeoIP\Facades\GeoIP;

use Illuminate\Support\Facades\Lang;

/**
 * Class IndexController
 * @package App\Http\Controllers
 */
class IndexController extends Controller {

    /**
     * IndexController constructor.
     */
    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('roles', ['roles' => ['administrator', 'manager']]);
	}

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
	public function index()
	{
        return redirect()->route('statistic');
	}
}