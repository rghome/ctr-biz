<?php

Route::group(['middleware' => 'web'], function () {
    Route::get('/', ['uses' => 'IndexController@index']);

	Route::post('/loginAjax', 'RegisterController@loginAjax');
	Route::get('/logout', 'AuthController@logout');
	Route::any('/login', 'AuthController@login');
	Route::get('/language/{lang}', 'AuthController@switchLanguage');

	Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

	Route::group(['namespace' => 'Admin', 'prefix' => 'dashboard'], function() {
		Route::get('/', ['uses' => 'AdminController@index','roles' => ['administrator', 'manager']]);

		Route::get('/statistic', ['uses' => 'StatisticController@index', 'as' => 'statistic']);
		Route::post('/statistic/search', ['uses' => 'StatisticController@search', 'as' => 'statistic_search']);

        Route::get('/users', ['uses' => 'UserController@index', 'as' => 'users']);
        Route::any('/user/profile', ['uses' => 'UserController@profile', 'as' => 'user_profile']);
        Route::get('/users/invites', ['uses' => 'UserController@invites', 'as' => 'users_invites']);
        Route::any('/users/{id}', ['uses' => 'UserController@update', 'as' => 'users_get']);
        Route::any('/users/edit/{user}', ['uses' => 'UserController@edit', 'as' => 'users_edit']);
        Route::any('/user/create', ['uses' => 'UserController@create', 'as' => 'users_create']);
        Route::any('/user/create/invite/{token}', ['uses' => 'UserController@registerByInvite', 'as' => 'users_create_by_invite']);
        Route::any('/user/invite/create', ['uses' => 'UserController@createInvite', 'as' => 'users_create_invite']);
        Route::get('/user/invite/show/{invite}', ['uses' => 'UserController@showInvite', 'as' => 'users_show_invite']);
        Route::get('/user/invite/send/{invite}', ['uses' => 'UserController@sendInvite', 'as' => 'users_send_invite']);
        Route::get('/users/delete/{user}', ['uses' => 'UserController@delete', 'as' => 'users_delete']);
        Route::post('/user/readNotification', ['uses' => 'UserController@readNotification', 'as' => 'user_readNotification']);
        Route::post('/user/hideSundayNotification', ['uses' => 'UserController@hideSundayNotification', 'as' => 'user_hideSundayNotification']);

        Route::get('/payments', ['uses' => 'PaymentController@index', 'as' => 'payments']);
        Route::get('/payments/withdrawals', ['uses' => 'PaymentController@getWithdrawal', 'as' => 'withdrawals']);
        Route::any('/payments/withdrawals/edit/{payment}', ['uses' => 'PaymentController@editWithdrawal', 'as' => 'withdrawals_edit']);
        Route::post('/payment/withdrawal', ['uses' => 'PaymentController@createWithdrawal', 'as' => 'payment_withdrawal']);
        Route::get('/payment/delete/{payment}', ['uses' => 'PaymentController@deleteWithdrawal', 'as' => 'payment_withdrawal_delete']);

        Route::get('/reports', ['uses' => 'ReportController@index', 'as' => 'reports']);
        Route::post('/reports/search', ['uses' => 'ReportController@search', 'as' => 'reports_search']);
        Route::get('/reports/total', ['uses' => 'ReportController@total', 'as' => 'reports_total']);
        Route::post('/reports/total/search', ['uses' => 'ReportController@searchTotal', 'as' => 'reports_total']);

        Route::get('/news', ['uses' => 'NewsController@index', 'as' => 'news']);
        Route::any('/news/create', ['uses' => 'NewsController@create', 'as' => 'news_create']);
        Route::any('/news/edit/{news}', ['uses' => 'NewsController@edit', 'as' => 'news_edit']);
        Route::get('/news/delete', ['uses' => 'NewsController@delete', 'as' => 'news_delete']);

        Route::get('/offers', ['uses' => 'OfferController@index', 'as' => 'offers']);
        Route::any('/offer/edit/{offer}', ['uses' => 'OfferController@edit', 'as' => 'offers_edit']);
        Route::any('/offer/create', ['uses' => 'OfferController@create', 'as' => 'offers_create']);
        Route::get('/offer/delete/{offer}', ['uses' => 'OfferController@delete', 'as' => 'offers_delete']);
        Route::get('/offer/select/{offer}', ['uses' => 'OfferController@select', 'as' => 'offers_select']);
        Route::any('/offer/add-landing/{offer?}', ['uses' => 'OfferController@addLanding', 'as' => 'offers_add_landing']);
        Route::any('/offer/edit-landing/{landing?}', ['uses' => 'OfferController@editLanding', 'as' => 'offers_edit_landing']);
        Route::post('/offer/select', ['uses' => 'OfferController@createRedirectLink', 'as' => 'offers_createRedirectLink']);

        Route::get('/regions', ['uses' => 'RegionController@index', 'as' => 'regions']);
        Route::any('/region/edit/{region}', ['uses' => 'RegionController@edit', 'as' => 'regions_edit']);
        Route::any('/region/create', ['uses' => 'RegionController@create', 'as' => 'regions_create']);
        Route::get('/region/delete/{region}', ['uses' => 'RegionController@delete', 'as' => 'regions_delete']);

        Route::get('/domains', ['uses' => 'DomainController@index', 'as' => 'domains']);
        Route::any('/domain/edit/{domain}', ['uses' => 'DomainController@edit', 'as' => 'domains_edit']);
        Route::any('/domain/create', ['uses' => 'DomainController@create', 'as' => 'domains_create']);
        Route::get('/domain/delete/{domain}', ['uses' => 'DomainController@delete', 'as' => 'domains_delete']);

        Route::get('/tokens', ['uses' => 'TokenController@index', 'as' => 'tokens']);
        Route::any('/token/create', ['uses' => 'TokenController@create', 'as' => 'token_create']);
        Route::any('/token/edit/{token}', ['uses' => 'TokenController@edit', 'as' => 'token_edit']);
    });

    Route::group(['namespace' => 'Api', 'prefix' => 'api'], function() {
        Route::post('/add', ['uses' => 'ApiController@add', 'as' => 'api_add']);
    });

    Route::group(['namespace' => 'Bot', 'prefix' => 'bot'], function() {
        Route::any('/tg/wh', ['uses' => 'WebHookController@index']);
    });
});