<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cases extends Model
{

	protected $fillable = [
		'guid', 'user_id', 'data', 'device_id','material_id', 'type', 'logo'
	];

	public function device()
	{
		return $this->belongsTo('App\Models\Device');
	}
}
