<?php
/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylorotwell@gmail.com>
 */

//phpinfo();

header("Access-Control-Allow-Origin: *");
//header("link: </vendors/normalize-css/normalize.css>; rel=preload; as=stylesheet", false);
//header("link: </vendors/font-awesome/css/font-awesome.min.css>; rel=preload; as=stylesheet", false);
//header("link: </vendors/bootstrap/dist/css/bootstrap.min.css>; rel=preload; as=stylesheet", false);
//header("link: </vendors/vendors/select2/dist/css/select2.min.css>; rel=preload; as=stylesheet", false);
//header("link: </vendors/themes/gentelella/css/custom.min.css>; rel=preload; as=stylesheet", false);
//header("link: </vendors/jquery-ui-1.12.1/jquery-ui.min.css>; rel=preload; as=stylesheet", false);
//header("link: </vendors/jquery-ui-1.12.1/jquery-ui.theme.min.css>; rel=preload; as=stylesheet", false);

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels nice to relax.
|
*/

require __DIR__.'/../bootstrap/autoload.php';

/*
|-------------------

-------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

$kernel = $app->make('Illuminate\Contracts\Http\Kernel');

$response = $kernel->handle(
	$request = Illuminate\Http\Request::capture()
);

$response->send();

$kernel->terminate($request, $response);
