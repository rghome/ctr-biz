$(window).load(function () {
    // NProgress.done();
	preloaderHide();
});

var filterRowsCache = {};

$(function () {
	/**
	 * ON: Search functional
	 */
	$.ajaxSetup({
		headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')}
	});

	$('form[name="search-filter"]').on('submit', function (e) {
		e.preventDefault();

		var form = $(this);

		sendSearchForm(form, form.serialize());
	});

	$('button[type="reset"]').on('click', function (e) {
		e.preventDefault();

		var form = $(this).closest('form').get(0);

		sendSearchForm($(form), {});

		if (Object.keys(filterRowsCache).length) {
			$.each(filterRowsCache, function (name, data) {
				var select = $(form).find('select[name="' + name + '"]');

				select.empty().select2({data: data, placeholder: translates.select});
			});
		}

		$(form).find('input.date').val('');
		$(form).find('input[name="search[start-date]"]').val('');
		$(form).find('input[name="search[end-date]"]').val('');

		if (window.hasOwnProperty('dateRangeStart') && window.hasOwnProperty('dateRangeEnd')) {
			$(form).find('a.period').text(window['dateRangeStart'] + ' - ' + window['dateRangeEnd'])
		}
	});

	/**
	 * @param form JQuery объект формы
	 * @param data объект данных
	 */
	var sendSearchForm = function (form, data) {
		preloaderShow();
		var request = $.post(form.attr('action'), data);
		var wreapper = $('#' + form.data('wrapper'));

		request.done(function (data) {
            wreapper.html(data);

            if (window['dataTableCallback'] !== undefined) {
            	window.dataTableCallback();
			}

			preloaderHide();
		});

		// @TODO: допилить валидацию ошибок
		request.fail(function (response) {
            wreapper.html(jQuery.parseJSON(response.responseText));

			if (response.status === 404) {
				// showError('По вашему запросу ничего не найденно');
			}
			preloaderHide();
		});
	};
	/**
	 * END: Search functional
	 */

	/**
	 * ON: Filter functional
	 */

	renderFilters($("form select.search-filter"));
	/**
	 * END: Filter functional
	 */

});

var renderFilters = function (selects, cached) {
	var options = {
		placeholder: translates.select,
		// "language": {
		// 	"noResults": function() {
		// 		return 'Не найдено';
		// 	}
		// },
		// escapeMarkup: function (markup) {
		// 	return markup;
		// }
	};

	var searchForm = selects.parents('form[name="search-filter"]');

	if (searchForm.data('cached') || selects.parents('form').data('cached')) {
		var filterCache = function(option) {
			return {
				id: option.value,
				text: option.innerText
			};
		};

		$.each(selects, function () {
			var select = $(this);
			if (!filterRowsCache[select.attr('name')]) {
				var options = select.find('option');
				filterRowsCache[select.attr('name')] = $.map(options, filterCache);
			}
		});
	}

	selects.select2(options);

	selects.on('change', function () {
		var select = $(this);

		var filter = {
			url: select.data('filter-url'),
			model: select.data('filter-model'),
			key: select.data('filter-key'),
			to: select.data('filter-to')
		};

		if (filter.url === undefined) return true;

		var value = select.val();

		preloaderShow();
		var request = $.post(filter.url, $.extend(filter, {value: value}));
		request.done(function (data) {
			preloaderHide();
			var cache = [{ id: '' }];
			if (Object.keys(data).length) {
				$.each(data, function (key, data) {
					cache.push({
						id: data.id,
						text: data.name || data[$('#' + filter.to).data('filter-text-name')]
					});
				});
			}

			$(selects).slice(selects.index(select) + 1).empty();

			var options = {
				data: cache,
				placeholder: translates.select
			};

			$('#' + filter.to).empty().select2(options);
		});
		request.fail(displayJsonResponseErrors)
	});
};

var displayJsonResponseErrors = function (e) {
	preloaderHide();
	if (Object.keys(e.responseJSON).length) {
		$.each(e.responseJSON, function (key, data) {
			return showError(data[0]);
		});
	}
};

function showError(text) {
	new PNotify({
		title: "Ошибка!",
		type: "error",
		text: text,
		nonblock: {
			nonblock: true
		},
		addclass: 'dark',
		styling: 'bootstrap3',
		hide: false,
		before_close: function (PNotify) {
			PNotify.update({
				title: PNotify.options.title + " - Enjoy your Stay",
				before_close: null
			});
			PNotify.queueRemove();
			return false;
		}
	});

	setTimeout(function() {
		hideErrors();
	}, 5000);
}

function showSuccess(text) {
    new PNotify({
        title: text,
        type: "success",
        // text: ,
        nonblock: {
            nonblock: true
        },
        addclass: 'dark',
        styling: 'bootstrap3',
        hide: false,
        before_close: function (PNotify) {
            PNotify.update({
                title: PNotify.options.title + " - Enjoy your Stay",
                before_close: null
            });
            PNotify.queueRemove();
            return false;
        }
    });

    setTimeout(function() {
        hideErrors();
    }, 5000);
}

function hideErrors()
{
	PNotify.removeAll();
}

// $('.js-phone-mask').mask('+7(000) 000-00-00', {placeholder: "+7(___) ___-__-__"});
$('.js-path-mask').mask('/var/www/AAAAAAAAAAAAAAAAAAAAA', {placeholder: "/var/www/"});

function preloaderShow()
{
	$('#preloader').fadeOut('slow', function () {
		$(this).show();
	});

}
function preloaderHide()
{
	$('#preloader').fadeOut('slow', function () {
		$('#preloader').hide();
	});
}

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

$('#statistic-filter-modal button[type="submit"]').on('click', function () {
	var modal = $(this).parents('#statistic-filter-modal');
	var searchForm = $('form[name="search-filter"]');

	var rows = modal.find('*').serializeObject();
	var labelStr = [];
	$.each(rows, function (key, value) {
		var input = searchForm.find('#by-date input[name="search[' + key + ']"]');
		input.val(value);

		labelStr.push(value);
    });

    searchForm.find('#by-date a.period').text(labelStr.join(' - '));
    modal.modal('hide');

    searchForm.submit();
});


var notificationsList = $('#notifications_list').find('li[data-id].list-group-item');
if (notificationsList.length) {
	var readNotificationUrl = '/dashboard/user/readNotification';
	var processRead = function (event) {
        var el = $(this);

        if (event.type === 'click') {
        	event.preventDefault();
		}

        el.removeClass('list-group-item list-group-item-info');
        $.post(readNotificationUrl, {id: el.data('id')});
        el.off(event.type);

        updateCounter();

        if (event.type === 'click') {
        	window.location = el.find('a').attr('href');
        }
    };

	var updateCounter = function () {
		var counterElement = $('.info-number').find('span.badge');
		var count = parseInt(counterElement.text()) - 1;

		if (count == 0) {
            counterElement.hide();
		} else {
            counterElement.text(count);
		}
    };

	$.each(notificationsList, function (key, element) {
		$(element).off('mouseleave').on('mouseleave', processRead);
		$(element).off('click').on('click', processRead);
    });
}

function createCookie(name, value, days) {
    var date, expires;
    if (days) {
        date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires="+date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name+"="+value+expires+"; path=/";
}

$('.close-sunday_notify').on('click', function () {
	$(this).parent('div.sunday_notify').hide();

    $.post('/dashboard/user/hideSundayNotification');
});