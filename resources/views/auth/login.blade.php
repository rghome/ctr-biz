@extends('layouts.clear')

@section('content')
	<div class="container">
		<div class="login-holder">
			<div class="panel panel-default">
				<div class="panel-heading">{{ trans('ui.auth') }}</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
						{!! csrf_field() !!}

						<div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
							<label class="col-md-4 col-xs-3 control-label">{{ trans('ui.login') }}</label>

							<div class="col-md-6 col-xs-9">
								<input type="text" class="form-control" name="login" value="{{ old('login') }}">

								@if ($errors->has('login'))
									<span class="help-block">
                                    <strong>{{ $errors->first('login') }}</strong>
                                </span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label class="col-md-4 col-xs-3 control-label">{{ trans('ui.password') }}</label>

							<div class="col-md-6 col-xs-9">
								<input type="password" class="form-control" name="password">

								@if ($errors->has('password'))
									<span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
								@endif
							</div>
						</div>

						{{--<div class="form-group">--}}
							{{--<div class="col-md-6 col-md-offset-4">--}}
								{{--<div class="checkbox">--}}
									{{--<label>--}}
										{{--<input type="checkbox" name="remember"> Remember Me--}}
									{{--</label>--}}
								{{--</div>--}}
							{{--</div>--}}
						{{--</div>--}}

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-success">
									<i class="fa fa-btn fa-sign-in"></i> {{ trans('ui.enter') }}
								</button>

								{{--<a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your--}}
									{{--Password?</a>--}}
							</div>
						</div>
					</form>
					@if(count($errors) >0)
						<div class="alert alert-danger">
							<ul>
								@foreach($errors->all() as $error)
									<li> {{$error}}</li>
								@endforeach
							</ul>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection
