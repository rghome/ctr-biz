<ul class="nav side-menu" style="">
	@if(RoleVerifier::can('report.index'))
		<li>
			<a href="{{ url('/dashboard/reports/total') }}">
				<i class="fa fa-list-alt" aria-hidden="true"></i>
				{{ trans('ui.menu_report') }}
			</a>
		</li>
	@endif

	<li>
		<a href="{{ url('/dashboard/statistic') }}">
			<i class="fa fa-bar-chart"></i>
			{{ trans('ui.menu_statistic') }}
		</a>
	</li>

	<li>
		<a href="{{ url('/dashboard/news') }}">
			<i class="fa fa-rss"></i>
			{{ trans('ui.menu_news') }}
		</a>
	</li>

	@if(RoleVerifier::can('payment.getwithdrawal'))
		<span style="display:none;">{{ $waitingCount = (new \App\Repository\PaymentRepository())->getCountWaiting() }}</span>
	<li>
		<a>
			<i class="fa fa-money"></i>
			{{ trans('ui.menu_payments') }} @if ($waitingCount) <span class="label label-danger pull-right">{{$waitingCount}}</span> @endif
		</a>
		<ul class="nav child_menu">
			<li>
				<a href="{{ url('/dashboard/payments') }}">{{ trans('ui.menu_payments') }}</a>
			</li>
			<li>
				<a href="{{ url('/dashboard/payments/withdrawals') }}">{{ trans('ui.menu_payments_manager') }} @if ($waitingCount) <span class="label label-danger pull-right">{{$waitingCount}}</span> @endif</a>
			</li>
		</ul>
	</li>
	@else
		<li>
			<a href="{{ url('/dashboard/payments') }}">
				<i class="fa fa-money"></i>
				{{ trans('ui.menu_payments') }}
			</a>
		</li>
	@endif

	<li>
		<a href="{{ url('/dashboard/offers') }}">
			<i class="fa fa-shopping-cart"></i>
			{{ trans('ui.menu_offers') }}
		</a>
	</li>

	<li>
		<a href="{{ url('/dashboard/domains') }}">
			<i class="fa fa-server"></i>
			{{ trans('ui.menu_domains') }}
		</a>
	</li>

	@if(RoleVerifier::can('user.index'))
	<li>
		<a>
			<i class="fa fa-users"></i>
			{{ trans('ui.menu_users') }}
		</a>
		<ul class="nav child_menu">
			<li>
				<a href="{{ url('/dashboard/users') }}">{{ trans('ui.menu_users') }}</a>
			</li>
			<li>
				<a href="{{ url('/dashboard/users/invites') }}">{{ trans('ui.menu_users_invites') }}</a>
			</li>
		</ul>
	</li>
	@endif

	@if(RoleVerifier::can('settings.index'))
		<li>
			<a>
				<i class="fa fa-cogs"></i>
				{{ trans('ui.menu_settings') }}
			</a>
			<ul class="nav child_menu">
				<li>
					<a href="{{ url('/dashboard/regions') }}">{{ trans('ui.menu_regions') }}</a>
				</li>
				<li>
					<a href="{{ url('/dashboard/tokens') }}">{{ trans('ui.menu_tokens') }}</a>
				</li>
				<li>
					<a href="{{ url('/logs') }}">Логи</a>
				</li>
			</ul>
		</li>
	@endif
</ul>