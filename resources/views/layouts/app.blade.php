<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<link rel="shortcut icon" href="{{  asset('/favicon.ico') }}" />
		
	<link rel="stylesheet" href="{{ asset('libs/font-awesome-4.2.0/css/font-awesome.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('/libs/fancybox/jquery.fancybox.css') }}" />
	<link rel="stylesheet" href="{{ asset('/libs/owl-carousel/owl.carousel.css') }}" />
	<link rel="stylesheet" href="{{ asset('/libs/countdown/jquery.countdown.css') }}" />
	<link rel="stylesheet" href="{{ asset('/vendors/bootstrap/dist/css/bootstrap.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('/vendors/themes/gentelella/css/custom.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('/css/fonts.css') }}" />
	<link rel="stylesheet" href="{{ asset('/css/main.css') }}" />
	<link rel="stylesheet" href="{{ asset('/css/media.css') }}" />
	<title>Login</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar"></nav>

	@yield('content')

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

	<script src="/js/jquery.easypiechart.min.js"></script>
	<script>
		$('.chart').easyPieChart({
			lineWidth:10,
			size:70,
			barColor:'#dd0',
			scaleColor:"#eee"
		});

	</script>
	
	<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->
	
	<script src="{{ asset('/libs/jquery-mousewheel/jquery.mousewheel.min.js') }}"></script>
	<script src="{{ asset('/libs/fancybox/jquery.fancybox.pack.js') }}"></script>
	<script src="{{ asset('/libs/waypoints/waypoints-1.6.2.min.js') }}"></script>
	<script src="{{ asset('/libs/scrollto/jquery.scrollTo.min.js') }}"></script>
	<script src="{{ asset('/libs/owl-carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('/libs/countdown/jquery.plugin.js') }}"></script>
	<script src="{{ asset('/libs/countdown/jquery.countdown.min.js') }}"></script>
	<script src="{{ asset('/libs/countdown/jquery.countdown-ru.js') }}"></script>
	<script src="{{ asset('/libs/landing-nav/navigation.js') }}"></script>
	<script src="{{ asset('/js/common.js') }}"></script>
	<!-- Yandex.Metrika counter --><!-- /Yandex.Metrika counter -->
	<!-- Google Analytics counter --><!-- /Google Analytics counter -->
</body>
</html>
