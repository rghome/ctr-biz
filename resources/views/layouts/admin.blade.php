<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="">

	{{--<script src="http://ricostacruz.com/nprogress/nprogress.js"></script>--}}
	{{--<script>NProgress.start();</script>--}}

	<!-- <link rel="stylesheet" href="/css/main.css"> -->
	<link rel="stylesheet" href="/vendors/normalize-css/normalize.css">
	<link rel="stylesheet" href="/vendors/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/vendors/bootstrap/dist/css/bootstrap.min.css">
	<!-- select2 -->
	<link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
	<link rel="stylesheet" href="/vendors/themes/gentelella/css/custom.min.css">
	<link rel="stylesheet" href="/vendors/jquery-ui-1.12.1/jquery-ui.min.css">
	<link rel="stylesheet" href="/vendors/jquery-ui-1.12.1/jquery-ui.theme.min.css">
	{{--<link rel="stylesheet" href="http://ricostacruz.com/nprogress/nprogress.css">--}}

	<!-- iCheck -->
	<link rel="stylesheet" href="/vendors/iCheck/skins/flat/green.css">
	<!-- Switchery -->
	<link href="/vendors/switchery/dist/switchery.min.css" rel="stylesheet">

	<link rel="stylesheet" href="/css/main.css?ver={{time()}}">

	<link rel="stylesheet" href="/css/media.css?ver={{time()}}">


	<!-- PNotify -->
	<link href="/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
	<link href="/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
	<link href="/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
	<link href="/vendors/bootstrap-languages-master/languages.min.css" rel="stylesheet">


	<title>Private CPA</title>


	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<script>
        var translates = {
            select: '{{trans('ui.select')}}',
        };

	</script>
</head>
<body class="nav-md"> {{--class="nav-sm"--}}
<div id="preloader">
	<img src="/img/content/preloader.svg" alt="">
</div>
<div class="container body">
	<div class="main_container">

		<div class="col-md-3 left_col">
			<div class="left_col scroll-view">

				<div class="navbar nav_title" style="border: 0;">
					<a href="@if(RoleVerifier::canViewMenu('statistic')){{ url('/') }}@else{{ url('/dashboard') }}@endif" class="site_title">
						<i class="fa fa-briefcase"></i>
						<span style="position: absolute; left: 52px; top: -7px;">Private CPA<p style="font-size: 11px; position: absolute; top: 17px;">Приватная Партнерка</p></span>

					</a>
				</div>

				<div class="clearfix"></div>

				<!-- sidebar menu -->
				<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
					<div class="menu_section">
						<ul class="nav side-menu" style="">
							<li>
								<a href="{{ url('/dashboard/payments') }}">
									{{--{{(new App\Services\CpaAdapter())->getTotalFreeWrapper()}}--}}
									Зачислено: {{(new App\Services\CpaAdapter())->getTotalHoldWrapper()}} руб. <br/>
									Доступно к выводу: {{(new App\Services\CpaAdapter())->getTotalFreeWrapper()}} руб. <br/>
								</a>
							</li>
						</ul>
					</div>
					<div class="menu_section">
						@include('_common.admin_menu')
					</div>
				</div>
				<!-- /sidebar menu -->
			</div>

		</div>

		<!-- top navigation -->
		<div class="top_nav">
			@if (((int) date("w") === 6 || (int) date("w") === 0) && !Cookie::has('hide_sunday_notify'))
				<div class="nav_menu sunday_notify">
					<p>В понеледьник день выплат, подайте заявку на выплату до 07:00 понедельника</p>
					<a class="close-sunday_notify"><i class="fa fa-close"></i></a>
				</div>
			@endif
			<div class="nav_menu">
				<nav class="" role="navigation">
					<div class="nav toggle">
						<a id="menu_toggle">
							<i class="fa fa-bars"></i>
						</a>
					</div>

					<ul class="nav navbar-nav navbar-right">
						<li class="">
							<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								{{Auth::user()->name}}
								<span class=" fa fa-angle-down"></span>
							</a>
							<ul class="dropdown-menu dropdown-usermenu pull-right">
								<li><a href="/dashboard/user/profile"> Профиль</a></li>
								<li><a href="/logout"><i class="fa fa-sign-out pull-right"></i> Выход</a></li>
							</ul>
						</li>

						<li role="presentation" class="dropdown">
							<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
								<i class="fa fa-bell-o"></i>
								@if (count(Auth::user()->unreadNotifications))
									<span class="badge bg-green">{{count(Auth::user()->unreadNotifications)}}</span>
								@endif
							</a>
							<ul id="notifications_list" class="dropdown-menu list-unstyled msg_list notifications_list" role="menu">
								@forelse(Auth::user()->notifications as $notification)
									<li @if ($notification->read_at === null) class="list-group-item list-group-item-info" @endif data-id="{{$notification->id}}">
										<a href="{{$notification->data['link']}}">
											<span class="image">
												<i class="fa fa-{{$notification->data['icon']}}"></i>
											</span>
											<span class="details">
												<span class="author">{{$notification->data['author']}}</span>
												<span class="time">{{$notification->created_at->diffForHumans()}}</span>
											</span>
											<span class="message">
												{{str_limit($notification->data['message'], 77, '...')}}
											</span>
										</a>
									</li>

									{{--@if(Auth::user()->notifications->last() == $notification)--}}
										{{--<li>--}}
											{{--<div class="text-center">--}}
												{{--<a>--}}
													{{--<strong>Все уведомления</strong>--}}
													{{--<i class="fa fa-angle-right"></i>--}}
												{{--</a>--}}
											{{--</div>--}}
										{{--</li>--}}
									{{--@endif--}}
								@empty
									<li class="none-notification">
										<div class="text-center">
											<strong>Уведомления отсутствуют</strong>
										</div>
									</li>
								@endforelse
							</ul>
						</li>
					</ul>

				</nav>
			</div>
		</div>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main" style="min-height: 945px;">
			<div class="page-title">
				<div class="title_left">
					<h3>@yield('pageTitle')</h3>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="row">
				@yield('content')
			</div>
		</div>
	</div>
</div>

<!-- Scripts -->
<script src="/vendors/jquery/dist/jquery.min.js"></script>
<script src="/vendors/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/vendors/themes/gentelella/js/custom.min.js"></script>
<script src="/vendors/validator/validator.js"></script>
<script src="/vendors/iCheck/icheck.min.js"></script>
<script src="/vendors/switchery/dist/switchery.min.js"></script>
<script src="/vendors/jquery-validator/jquery.validate.js"></script>

<!-- PNotify -->
<script src="/vendors/pnotify/dist/pnotify.js"></script>
<script src="/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="/vendors/pnotify/dist/pnotify.nonblock.js"></script>

<!-- select2 -->
<script src="/vendors/select2/dist/js/select2.full.min.js"></script>

<!-- PNotify -->
<!-- <script>
  $(document).ready(function() {
    new PNotify({
      title: "PNotify Warning /PNotify + init required!!!",
      type: "info",
      text: "Welcome. Try hovering over me. You can click things behind me, because I'm non-blocking.",
      nonblock: {
          nonblock: true
      },
      addclass: 'dark',
      styling: 'bootstrap3',
      hide: false,
      before_close: function(PNotify) {
        PNotify.update({
          title: PNotify.options.title + " - Enjoy your Stay",
          before_close: null
        });

        PNotify.queueRemove();

        return false;
      }
    });

  });
</script> -->
<!-- /PNotify -->

<!-- select2_single init end-->
<script>
    $(document).ready(function() {
        $('.select2_group').each(function() {
            $(this).select2({
                placeholder: "Select a state",
                allowClear: true
            });

            $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
                $(".select2_group").select2({
                    matcher: oldMatcher(
                        function (term, text, option) {
                            if (typeof option.element.dataset.search != 'undefined'){
                                return option.element.dataset.search.toUpperCase().indexOf(term.toUpperCase())>=0;
                            } else {
                                return text.toUpperCase().indexOf(term.toUpperCase())>=0;
							}
                        }
                    )
                })
            });

            $(".select2_multiple").select2({
                maximumSelectionLength: 4,
                placeholder: "With Max Selection limit 4",
                allowClear: true
            });
        });
    });
</script>
<!-- select2_single init-->

<!-- filters toggle -->
<script>
    $('#filtersbtn').on('click', function() {
        $('#members-filters').toggleClass('open');
        $(this).toggleClass('open');
    });
</script>
<!-- filters toggle end-->

{{-- @TODO пока сюда потом на ту страницу куда надо --}}
<script type="text/javascript" src="/vendors/jquery.mask.min.js"></script>
{{--@TODO: Сделать уведомления о ошибках --}}
<script src="/js/main.js?ver={{time()}}"></script>

@if ($errors->any())
	@include('_common.error_info', ['errorMessages' =>  $errors->all(':message')]);
@endif


@yield('bottomScript')

<!-- Start SiteHeart code -->
{{--<script>--}}
    {{--(function(){--}}
        {{--var widget_id = 872356;--}}
        {{--_shcp =[{widget_id : widget_id}];--}}
        {{--var lang =(navigator.language || navigator.systemLanguage--}}
        {{--|| navigator.userLanguage ||"en")--}}
            {{--.substr(0,2).toLowerCase();--}}
        {{--var url ="widget.siteheart.com/widget/sh/"+ widget_id +"/"+ lang +"/widget.js";--}}
        {{--var hcc = document.createElement("script");--}}
        {{--hcc.type ="text/javascript";--}}
        {{--hcc.async =true;--}}
        {{--hcc.src =("https:"== document.location.protocol ?"https":"http")--}}
            {{--+"://"+ url;--}}
        {{--var s = document.getElementsByTagName("script")[0];--}}
        {{--s.parentNode.insertBefore(hcc, s.nextSibling);--}}
    {{--})();--}}
{{--</script>--}}
<!-- End SiteHeart code -->


<!-- Yandex.Metrika counter -->
{{--<script type="text/javascript">--}}
    {{--(function (d, w, c) {--}}
        {{--(w[c] = w[c] || []).push(function() {--}}
            {{--try {--}}
                {{--w.yaCounter44210764 = new Ya.Metrika({--}}
                    {{--id:44210764,--}}
                    {{--clickmap:true,--}}
                    {{--trackLinks:true,--}}
                    {{--accurateTrackBounce:true,--}}
                    {{--webvisor:true,--}}
                    {{--ut:"noindex"--}}
                {{--});--}}
            {{--} catch(e) { }--}}
        {{--});--}}

        {{--var n = d.getElementsByTagName("script")[0],--}}
            {{--s = d.createElement("script"),--}}
            {{--f = function () { n.parentNode.insertBefore(s, n); };--}}
        {{--s.type = "text/javascript";--}}
        {{--s.async = true;--}}
        {{--s.src = "https://mc.yandex.ru/metrika/watch.js";--}}

        {{--if (w.opera == "[object Opera]") {--}}
            {{--d.addEventListener("DOMContentLoaded", f, false);--}}
        {{--} else { f(); }--}}
    {{--})(document, window, "yandex_metrika_callbacks");--}}
{{--</script>--}}
{{--<noscript><div><img src="https://mc.yandex.ru/watch/44210764?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div></noscript>--}}
<!-- /Yandex.Metrika counter -->

</body>
</html>
