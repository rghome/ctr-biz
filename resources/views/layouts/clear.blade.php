<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<link rel="shortcut icon" href="{{  asset('/favicon.ico') }}" />
		
	<link rel="stylesheet" href="{{ asset('/vendors/bootstrap/dist/css/bootstrap.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('/vendors/themes/gentelella/css/custom.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('/css/main.css') }}" />
	<link rel="stylesheet" href="{{ asset('/css/media.css') }}" />
	<title>Login</title>


	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>


</head>
<body>
	<nav class="navbar"></nav>

	@yield('content')

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>


</body>
</html>
