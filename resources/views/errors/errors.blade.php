@if(count($errors) >0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li> {{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

@if( isset($error)  )
    <div class="alert alert-danger">
        <ul>
            <li>  {{$error}}</li>
        </ul>
    </div>
@endif

@if( isset($success)  )
    <div class="alert alert-success">
        <ul>
            <li>  {{$success}}</li>
        </ul>
    </div>
@endif