

@extends('case_place.head-footer')

@section('content')

    <script type="text/javascript" src="{{ asset('case_place/libs/nicEdit/nicEdit.js') }}"></script>
    <script type="text/javascript">
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
    </script>

    <textarea name="area2" style="width: 100%;">
	        Some Initial Content was in this textarea
    </textarea>

@endsection
