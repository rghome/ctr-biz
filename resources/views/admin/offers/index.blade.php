@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.available_offers') }}
@endsection

@section('bottomScript')
    <script type="application/javascript" src="/vendors/clipboard.js-master/dist/clipboard.min.js"></script>

    <script>
        var clipboard = new Clipboard('button.clipboard');

        clipboard.on('success', function(e) {
            console.info('Action:', e.action);
            console.info('Text:', e.text);
            console.info('Trigger:', e.trigger);

            e.clearSelection();

            showSuccess('Скопированно!');
        });

        $('.delete_offer').on('click', function(e) {
            e.preventDefault();

            var self = $(this);

            if(confirm('Удалить оффер?')) {
                location.href = '/dashboard/offer/delete/' + self.data('offer-id');
            }
        });

        $(document).ready(function () {
            $("#select-offer-modal").on("show.bs.modal", function(e) {
                preloaderShow();
                var link = $(e.relatedTarget);

                var body = $(this).find(".modal-body");
                body.load(link.attr("href"), function () {
//                    body.find('select').select2();

                    $('select#landing').off('change').on('change', function () {
                        var self = $(this);
                        $('table.landing-utm').hide();
                        $('table#landing-utm-' + self.val()).show();
                    });

                    body.find('input[type=text]').off('keyup').on('keyup', function () {
                        var self = $(this);

                        if (self.val().indexOf('.') !== -1) {
                            self.val(self.val().replace('.', '-'));

                            showSuccess('Пожалуйста, не используйте точку в метках. При использовании, система автоматически заменяет точку на тире.');
                        }
                    });

                    preloaderHide();
                });

                $(this).find(".modal-footer #link").val('');
            });

            $('#get-redirect-link').on('click', function () {
                var modalContent = $(this).parents('.modal-content');
                var modalBody = modalContent.find('.modal-body');
                var domainSelect = modalBody.find('select#domain');
                var landingSelect = modalBody.find('select#landing');
                var offerInput = modalBody.find('input#offer');
                var urlInput = modalContent.find('input#link');

                var utm = {
                    utm_source: modalBody.find('.utm-table:visible input#utm_source').val(),
                    utm_medium: modalBody.find('.utm-table:visible input#utm_medium').val(),
                    utm_campaign: modalBody.find('.utm-table:visible #utm_campaign').val(),
                    utm_term: modalBody.find('.utm-table:visible input#utm_term').val(),
                    utm_content: modalBody.find('.utm-table:visible input#utm_content').val()
                };

                if (!domainSelect.val()) {
                    return showError('Выберите домен.');
                }

                var params = {
                    offer: offerInput.val(),
                    domain: domainSelect.val(),
                    landing: landingSelect.val(),
                    utm: utm
                };

                var request = $.post('{{ url('/dashboard/offer/select') }}', params);
                request.done(function (response) {
                    var domain = response.data.domain;
                    var token = response.data.token;
                    var url = 'http://' + domain.name + '/' + token;

                    $('button.clipboard').show();

                    urlInput.val(url).focus();
                });
                request.error(function (response) {
                    return showError(response.responseJSON.message);
                });

            });
        });

        var bindUtmParams = function (utm) {
            var cache = [];

            $.each(utm, function (key, value) {
                if (value) {
                    cache.push(key + '=' + value);
                }
            });

            return cache;
        };
    </script>
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <ul class="list-inline top-btns">
                @if(RoleVerifier::can('offer.create'))
                    <li>
                        <a class="btn btn-block btn-success " href="/dashboard/offer/create">{{ trans('ui.create') }}</a>
                    </li>
                @endif
            </ul>
            <div class="x_content">
                {{--@include('admin.production.filter')--}}
                <div class="scroll-holder" id="offers-list-wrap">
                    @include('admin.offers.offers-list')
                </div>
            </div>
        </div>
    </div>
@endsection
