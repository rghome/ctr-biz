<table class="table table-bordered table-hover">
    <thead>
        <tr role="row">
            <th>{{ trans('ui.offer_name') }}</th>
            <th width="15%">{{ trans('ui.offer_domain') }}</th>
            <th width="20%">{{ trans('ui.offer_price') }}</th>
            <th width="15%">{{ trans('ui.act') }}</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($landings as $c)
            <tr role="row" class="odd">
                <td><strong>{{$c->name}}</strong></td>
                <td><a class="domain-link" target="_blank" href="http://{{$c->hostname}}/">{{$c->hostname}}</a> <i class="fa fa-fw fa-external-link"></i></td>
                <td class="table-price-list">
                    <ul>
                        @foreach($c->prices as $price)
                            <li><span class="lang-sm" lang="{{$price->region->code}}"></span> <span>{{$price->region->name}}</span> <span><strong>{{$price->price}} {{$price->region->currency}}</strong></span></li>
                        @endforeach
                    </ul>
                </td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-primary" href="/dashboard/offer/edit-landing/{{$c->id}}" data-remote="false" data-toggle="modal" data-target="#edit-landing-modal">
                            <i class="fa fa-edit" aria-hidden="true"></i>
                        </a>
                        <a class="btn btn-danger delete_offer" title="Удалить" data-offer-id="{{ $c->id }}"><i class="fa fa-fw fa-remove"></i></a>
                    </div>
                </td>
            </tr>
        @empty
            <tr role="row">
                <td colspan="9" style="text-align: center;">{{trans('ui.not_found')}}</td>
            </tr>
        @endforelse
    </tbody>
</table>