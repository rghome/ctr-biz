<form action="" method="post" id="add-landing-form" novalidate>
    <input required class="form-control" type="hidden" name="form[offer_id]" value="{{$offer->id}}">
    @if (isset($landing))
        <input required class="form-control" type="hidden" name="form[landing_id]" value="{{$landing->id}}">
    @endif

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#mainContentTab" aria-controls="mainContentTab" role="tab" data-toggle="tab">Общая информация</a></li>
        <li role="presentation"><a href="#utmContentTab" aria-controls="utmContentTab" role="tab" data-toggle="tab">UTM</a></li>
        <li role="presentation" @if (!isset($landing)) class="disabled" @endif><a href="#metrikaContentTab" aria-controls="metrikaContentTab" role="tab" @if (isset($landing)) data-toggle="tab" @endif>Metrika @if (!isset($landing)) (Будет доступна после сохранения лендинга) @endif</a></li>
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="mainContentTab">
            <table class="table table-bordered table-hover dataTable" role="grid">
                <tbody>
                    <tr>
                        <td width="19%">{{ trans('ui.offer_name') }}</td>
                        <td>
                            <input title="{{ trans('ui.offer_name') }}" required class="form-control" type="text" name="form[name]" @if (isset($landing) && isset($landing->name))value="{{$landing->name}}"@endif>
                        </td>
                    </tr>
                    <tr>
                        <td width="19%">{{ trans('ui.offer_domain') }}</td>
                        <td>
                            <input id="hostname" title="{{ trans('ui.offer_domain') }}" required class="form-control" type="text" name="form[hostname]" @if (isset($landing) && isset($landing->hostname))value="{{$landing->hostname}}"@endif>
                        </td>
                    </tr>
                    <tr>
                        <td width="19%">{{ trans('ui.offer_params') }}</td>
                        <td>
                            <input id="params" title="{{ trans('ui.offer_params') }}" required class="form-control" type="text" name="form[params]" @if (isset($landing) && isset($landing->params))value="{{$landing->params}}"@endif>
                        </td>
                    </tr>
                    <tr>
                        <td width="19%">{{ trans('ui.offer_price') }}</td>
                        <td>
                            <div class="region-rows" id="region-prices">
                                @if (isset($landing) && !empty($landing->prices))
                                    @foreach($landing->prices as $key => $price)
                                        <div class="region-row">
                                            <select style="width: 200px;" required class="form-control select2_group" id="server" name="form[prices][region_id][]">
                                                <option value="">{{ trans('ui.select') }}</option>
                                                @if (isset($regions))
                                                    @foreach ($regions as $region)
                                                        <option value="{{ $region->id }}"
                                                                @if ($price->region_id == $region->id)
                                                                selected
                                                                @endif
                                                        >{{ $region->name }}, {{ $region->currency }}</option>
                                                    @endforeach
                                                @endif
                                            </select>

                                            <input required class="form-control offer-region-price" type="number" placeholder="100" min="1" name="form[prices][price][]" value="{{$price->price}}">

                                            <a class="price-add-btn control-label btn btn-primary" id="add-street"><i class="fa fa-plus" aria-hidden="true"></i></a>

                                            @if ($key != 0)
                                                <a class="price-remove-btn control-label btn btn-danger" id="add-street"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                            @endif
                                        </div>
                                    @endforeach
                                @else
                                    <div class="region-row">
                                        <select style="width: 200px;" required class="form-control select2_group" id="server" name="form[prices][region_id][]">
                                            <option value="">{{ trans('ui.select') }}</option>
                                            @if (isset($regions))
                                                @foreach ($regions as $region)
                                                    <option value="{{ $region->id }}">{{ $region->name }}, {{ $region->currency }}</option>
                                                @endforeach
                                            @endif
                                        </select>

                                        <input required class="form-control offer-region-price" type="number" placeholder="100" min="1" name="form[prices][price][]" @if (isset($landing) && isset($landing->name))value="{{$offer->name}}"@endif>

                                        <a class="price-add-btn control-label btn btn-primary" id="add-street"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </div>
                                @endif
                                <div class="region-row-hidden" style="display: none;">
                                    <select style="width: 200px;" required class="form-control" id="server" name="form[prices][region_id][]">
                                        <option value="">{{ trans('ui.select') }}</option>
                                        @if (isset($regions))
                                            @foreach ($regions as $region)
                                                <option value="{{ $region->id }}">{{ $region->name }}, {{ $region->currency }}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                    <input required class="form-control offer-region-price" type="number" placeholder="100" min="1" name="form[prices][price][]" @if (isset($landing) && isset($landing->name))value="{{$offer->name}}"@endif>

                                    <a class="price-add-btn control-label btn btn-primary" id="add-street"><i class="fa fa-plus" aria-hidden="true"></i></a>

                                    <a class="price-remove-btn control-label btn btn-danger" id="add-street"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div role="tabpanel" class="tab-pane" id="utmContentTab">
            <table class="table table-bordered table-hover dataTable" role="grid">
                <tbody>
                    <tr>
                        <td width="19.3%">UTM метки</td>
                        <td>
                            <div class="utm-rows">
                                @if (isset($landing) && count($landing->utm))
                                    @foreach($landing->utm as $key => $utm)
                                        <div class="utm-row">

                                            <div class="form-group col-sm-3">
                                                <input required class="form-control offer-utm-name" type="text" placeholder="utm_medium" name="form[utm][name][]" value="{{$utm->name}}">
                                            </div>
                                            <div class="form-group col-sm-3">
                                                <input required class="form-control offer-utm-value" type="text" name="form[utm][value][]" value="{{$utm->value}}">
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <select required class="form-control offer-utm-disabled" name="form[utm][disabled][]">
                                                    <option value="0">{{ trans('ui.select') }}</option>
                                                    <option @if ($utm->disabled == true) selected @endif value="1">true</option>
                                                    <option @if ($utm->disabled == false) selected @endif value="0">false</option>
                                                </select>
                                            </div>

                                            <div class="form-group col-sm-2">
                                                <a class="utm-add-btn control-label btn btn-primary" id="add-street"><i class="fa fa-plus" aria-hidden="true"></i></a>

                                                @if ($key != 0)
                                                    <a class="utm-remove-btn control-label btn btn-danger" id="add-street"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="utm-row">
                                        <div class="form-group col-sm-3">
                                            <input required class="form-control offer-utm-name" type="text" placeholder="utm_medium" name="form[utm][name][]">
                                        </div>
                                        <div class="form-group col-sm-3">
                                            <input required class="form-control offer-utm-value" type="text" name="form[utm][value][]">
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <select required class="form-control offer-utm-disabled" name="form[utm][disabled][]">
                                                <option value="0">{{ trans('ui.select') }}</option>
                                                <option value="1">true</option>
                                                <option value="0">false</option>
                                            </select>
                                        </div>

                                        <div class="form-group col-sm-2">
                                            <a class="utm-add-btn control-label btn btn-primary" id="add-street"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                @endif
                                <div class="utm-row-hidden" style="display: none;">
                                    <div class="form-group col-sm-3">
                                        <input required class="form-control offer-utm-name" type="text" placeholder="utm_medium" name="form[utm][name][]">
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <input required class="form-control offer-utm-value" type="text" name="form[utm][value][]">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <select style="width: 192px;" required class="form-control offer-utm-disabled" name="form[utm][disabled][]">
                                            <option value="0">{{ trans('ui.select') }}</option>
                                            <option value="1">true</option>
                                            <option value="0">false</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-sm-2">
                                        <a class="utm-add-btn control-label btn btn-primary" id="add-street"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                        <a class="utm-remove-btn control-label btn btn-danger" id="add-street"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div role="tabpanel" class="tab-pane" id="metrikaContentTab">
            <table class="table table-bordered table-hover dataTable" role="grid">
                <tbody>
                    <tr>
                        <td width="26.7%">ID счетчика</td>
                        <td>
                            <input required class="form-control" type="text" name="form[metrika_counter_id]" @if (isset($landing) && isset($landing->metrika_counter_id))value="{{$landing->metrika_counter_id}}"@endif>
                        </td>
                    </tr>
                    <tr>
                        <td width="26.7%">ID Цели визитов</td>
                        <td>
                            <input required class="form-control" type="text" name="form[metrika_goal_main_id]" @if (isset($landing) && isset($landing->metrika_goal_main_id))value="{{$landing->metrika_goal_main_id}}"@endif>
                        </td>
                    </tr>
                    <tr>
                        <td width="26.7%">ID Цели "Страница активации"</td>
                        <td>
                            <input required class="form-control" type="text" name="form[metrika_goal_activation_id]" @if (isset($landing) && isset($landing->metrika_goal_activation_id))value="{{$landing->metrika_goal_activation_id}}"@endif>
                        </td>
                    </tr>
                    <tr>
                        <td width="26.7%">UTM партнерки</td>
                        <td>
                            <select style="width: 250px;" class="form-control select2_group" name="form[metrika_utm_panel]">
                                @if (isset($landing))
                                    @foreach($landing->utm as $key => $utm)
                                        @if ($utm->value)
                                            <option value="{{$utm->name}}" @if (isset($landing) && isset($landing->metrika_utm_panel) && ($landing->metrika_utm_panel == $utm->name)) selected @endif>{{$utm->name}}: {{$utm->value}}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="26.7%">UTM WM</td>
                        <td>
                            <select style="width: 250px;" class="form-control select2_group" name="form[metrika_utm_wm]">
                                @if (isset($landing))
                                    @foreach($landing->utm as $key => $utm)
                                        @if ($utm->value)
                                            <option value="{{$utm->name}}"  @if (isset($landing) && isset($landing->metrika_utm_wm) && ($landing->metrika_utm_wm == $utm->name)) selected @endif>{{$utm->name}}: {{$utm->value}}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</form>