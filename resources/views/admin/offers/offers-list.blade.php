<table id="example2" class="table members-table table-bordered table-hover dataTable">
    <thead>
        <tr  role="row">
            {{--{{ trans('ui.offer_price') }}--}}
            <th style="text-align: center" width="30%">{{ trans('ui.offer_name') }}</th>
            <th style="text-align: center" width="15%">Детали оффера</th> {{--{{ trans('ui.offer_price') }}--}}
            <th style="text-align: center" width="25%">Доступные ЛП и преленды</th> {{--{{ trans('ui.offer_domain') }}--}}
            <th style="text-align: center">{{ trans('ui.act') }}</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($data as $c)
            <tr role="row" class="odd" @if ($c->status != \App\Models\Offer::STATUS_OPEN) style="background: rgba(244, 100, 95, 0.12);" @endif>
                {{--<td class="sorting_1">{{$c->id}}</td>--}}
                {{--<td><img src="{{$c->logo}}" width="50" height="50"></td>--}}
                <td style="text-align: center;">
                    <strong style="font-size: 17px">{{$c->name}}</strong> <br/><br/>
                    <img src="{{$c->logo}}" width="250" height="150">
                </td>
                <td style="vertical-align: middle;" class="table-price-list">
                    <div style="margin-left: 15px;">
                        <span>Таргетинг (ГЕО):</span>
                        <ul>
                            <li><strong>Все страны</strong></li>
                        </ul>
                    </div>

                    {{--<ul>--}}
                        {{--@foreach($c->prices as $price)--}}
                            {{--<li><span class="lang-sm" lang="{{$price->region->code}}"></span> <span>{{$price->region->name}}</span> <span><strong>{{$price->price}} {{$price->region->currency}}</strong></span></li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                </td>
                <td style="vertical-align: middle; text-align: center;" class="table-landings-list">
                    @if ((int) $c->multilanding == 1 && count($c->landings))
                        <ul>
                            @foreach($c->landings as $landing)
                                <li style="clear: both"><span>{{$landing->name}} ЛП</span> <span><a class="domain-link" target="_blank" href="http://{{$landing->hostname}}/@if ($landing->params)?{{$landing->params}} @endif">Смотреть</a></span></li>
                            @endforeach
                        </ul>
                    @else
                        <ul>
                            <li style="clear: both"><span>Основной ЛП</span> <span><a class="domain-link" target="_blank" href="http://{{$c->hostname}}/@if ($c->params)?{{$c->params}} @endif">Смотреть</a></span></li>
                        </ul>
                    @endif
                </td>
                <td style="vertical-align: middle; text-align: center;" class="table-action-list">
                    @if ((int) $c->multilanding == 1 && count($c->landings))
                        <ul>
                            @foreach($c->landings as $landing)
                                @foreach($landing->prices as $price)
                                    <li style="clear: both"><span>{{$landing->name}} ЛП</span> <span><strong>{{$price->price}} {{$price->region->currency}}</strong></span></li>
                                @endforeach
                            @endforeach
                        </ul>
                    @else
                        <ul>
                            @foreach($c->prices as $price)
                                <li style="clear: both"><span>Основной ЛП</span> <span><strong>{{$price->price}} {{$price->region->currency}}</strong></span></li>
                            @endforeach
                        </ul>
                    @endif
                    <br/>
                    <br/>
                    <a class="btn btn-primary" href="/dashboard/offer/select/{{$c->id}}" data-remote="false" data-toggle="modal" title="Использовать этот оффер" data-target="#select-offer-modal">
                        ПОЛУЧИТЬ ССЫЛКУ
                    </a>
                    @if(RoleVerifier::can('offer.edit'))
                        <a class="btn btn-success " title="Редактировать" href="/dashboard/offer/edit/{{ $c->id }}">РЕДАКТИРОВАТЬ</a>
                    @endif
                        {{--@if(RoleVerifier::can('offer.delete'))--}}
                            {{--<a class="btn btn-danger delete_offer" title="Удалить" data-offer-id="{{ $c->id }}"><i class="fa fa-fw fa-remove"></i></a>--}}
                        {{--@endif--}}
                </td>
            </tr>
        @empty
            <tr role="row">
                <td colspan="8" style="text-align: center;">{{trans('ui.not_found')}}</td>
            </tr>
        @endforelse
    </tbody>
</table>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="select-offer-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content live-form">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">{{ trans('ui.select_modal_title') }}</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer ">
                <div class="pull-left">
                    <input type="text" class="form-control" name="link" id="link" placeholder="Ссылка редиректа" style="width: 500px; float: left">
                    <button class="btn btn-default clipboard" style="display: none" title="Скопировать" data-clipboard-target="#link">
                        <i class="fa fa-clipboard" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="pull-right">
                    <button class="btn btn-primary " id="get-redirect-link">{{ trans('ui.select_modal_title') }}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('ui.cancel') }}</button>
                </div>
            </div>

        </div>
    </div>
</div>

{{ $data->links() }}