<div class="panel-body">
    <table class="table table-bordered table-hover">
        <tbody>
            <tr>
                <td width="40%">
                    <label for="domain">Домен:</label>
                </td>
                <td>
                    <select class="form-control select2_group" id="domain">
                        <option value="">{{ trans('ui.select') }}</option>
                        <option value="{{$defaultDomain}}">free-private-cpa.ru</option>
                        @if (isset($domains))
                            @foreach ($domains as $domain)
                                <option value="{{ $domain->id }}">{{ $domain->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </td>
            </tr>

            @if ((int) $offer->multilanding === 1 && count($offer->landings))
                <tr>
                    <td width="40%">
                        <label for="domain">Лендинг:</label>
                    </td>
                    <td>
                        <select class="form-control select2_group" id="landing">
                            @foreach ($offer->landings as $landing)
                                <option value="{{ $landing->id }}">{{ $landing->name }}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>

    @if ($offer->note)
        <div class="panel panel-default">
            <div class="panel-heading">
                <span style="font-size: 110%;" class="label label-danger">Примечание!</span>
            </div>
            <div class="panel-body">
                {!! $ShortcodeParser->offer($offer->note, $offer) !!}
            </div>
        </div>
    @endif

    <table @if ((int) $offer->multilanding === 1) style="display: none;" @endif class="table table-bordered table-hover utm-table">
        <tbody>
            @foreach ($offer->utm as $utm)
                <tr @if ($utm->name == 'utm_source' || $utm->name == 'utm_medium' || $utm->name == 'utm_campaign') style="display: none;" @endif>
                    <td width="40%">
                        <label for="{{$utm->name}}">
                            @if ($utm->name === 'utm_term')
                                Поток:
                            @elseif ($utm->name === 'utm_content')
                                Источник трафика:
                            @endif
                        </label>
                    </td>
                    <td>
                        @if ($options = $ShortcodeParser->checkOption($utm->value))
                            <select class="form-control select2_group" name="{{$utm->name}}" id="{{$utm->name}}">
                                @foreach($options as $key => $option)
                                    <option @if ($key === 0) selected @endif value="{{$option}}">{{$option}}</option>
                                @endforeach
                            </select>
                        @else
                            <input id="{{$utm->name}}" class="form-control" type="text" name="{{$utm->name}}" value="{{$ShortcodeParser->simple($utm->value)}}" @if ($utm->disabled == true) readonly @endif />
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    @if (count($offer->landings))
        @foreach ($offer->landings as $key => $landing)
            <table @if ($key !== 0) style="display: none;" @endif class="table table-bordered table-hover landing-utm utm-table" id="landing-utm-{{$landing->id}}">
                <tbody>
                    @foreach ($landing->utm as $utm)
                        <tr @if ($utm->name == 'utm_source' || $utm->name == 'utm_medium' || $utm->name == 'utm_campaign') style="display: none;" @endif>
                            <td width="40%">
                                <label for="{{$utm->name}}">
                                    @if ($utm->name === 'utm_term')
                                        Поток:
                                    @elseif ($utm->name === 'utm_content')
                                        Источник трафика:
                                    @endif
                                </label>
                            </td>
                            <td>
                                @if ($options = $ShortcodeParser->checkOption($utm->value))
                                    <select class="form-control select2_group" name="{{$utm->name}}" id="{{$utm->name}}">
                                        @foreach($options as $key => $option)
                                            <option @if ($key === 0) selected @endif value="{{$option}}">{{$option}}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <input id="{{$utm->name}}" class="form-control" type="text" name="{{$utm->name}}" value="{{$ShortcodeParser->simple($utm->value)}}" @if ($utm->disabled == true) readonly @endif />
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endforeach
    @endif

    <input type="hidden" name="offer" id="offer" value="{{$offer->id}}">
</div>