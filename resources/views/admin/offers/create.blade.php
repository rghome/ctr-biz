@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.offers') }}
@endsection

@section('content')
    <div class="col-xs-12">
        <form action="" method="post" id="offer-form" novalidate>

            <input required class="form-control" type="hidden" name="form[path]" @if (isset($offer) && isset($offer->path))value="{{$offer->path}}"@else value="/var/www/fake" @endif>
            <input required class="form-control" type="hidden" name="form[server_id]" @if (isset($offer) && isset($offer->server->server_id))value="{{$offer->server->server_id}}"@else value="1" @endif>

            <div class="box-header">
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9 text-right">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="x_panel">
                <div class="list-inline top-btns" style="padding-bottom: 20px;">
                    <li>
                        <button class="btn btn-block btn-success">{{ trans('ui.save') }}</button>
                    </li>
                </div>

                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#firstContentTab" aria-controls="firstContentTab" role="tab" data-toggle="tab">Общая информация</a></li>
                    <li role="presentation" @if (!isset($offer)) class="disabled" @endif @if (isset($offer) && ($offer->multilanding == 1)) style="display: block;" @else style="display: none;" @endif><a href="#landingContentTab" id="landingContentControl" aria-controls="landingContentTab" role="tab" @if (isset($offer)) data-toggle="tab" @endif>Лендинги @if (!isset($offer)) (Будет доступна после сохранения оффера) @endif</a></li>
                    <li role="presentation" @if (isset($offer) && ($offer->multilanding == 1)) style="display: none;" @endif ><a href="#secondContentTab" aria-controls="secondContentTab" id="utmContentControl" role="tab" data-toggle="tab">UTM</a></li>
                    <li role="presentation" @if (isset($offer) && ($offer->multilanding == 1)) style="display: none;" @endif @if (!isset($offer)) class="disabled" @endif><a href="#thirdContentTab" id="metrikaContentControl" aria-controls="thirdContentTab" role="tab" @if (isset($offer)) data-toggle="tab" @endif>Metrika @if (!isset($offer)) (Будет доступна после сохранения оффера) @endif</a></li>
                </ul>

                <div class="x_content">
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="firstContentTab">
                                    <table class="table table-bordered table-hover dataTable" role="grid">
                                        <tbody>
                                            <tr>
                                                <td>{{ trans('ui.offer_name') }}</td>
                                                <td>
                                                    <input required class="form-control" type="text" name="form[name]" @if (isset($offer) && isset($offer->name))value="{{$offer->name}}"@endif>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>{{ trans('ui.offer_status') }}</td>
                                                <td>
                                                    <select required class="form-control select2_group offer-utm-disabled" name="form[status]">
                                                        <option @if (isset($offer) && ($offer->status == \App\Models\Offer::STATUS_OPEN)) selected @endif value="{{\App\Models\Offer::STATUS_OPEN}}">Открыт</option>
                                                        <option @if (isset($offer) && ($offer->status == \App\Models\Offer::STATUS_HIDE)) selected @endif value="{{\App\Models\Offer::STATUS_HIDE}}">Закрыт</option>
                                                    </select>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>{{ trans('ui.offer_multi_landing') }}</td>
                                                <td style="height: 51px;">
                                                    <div class="material-switch pull-left" style="margin-top: 7px;">
                                                        <input id="multilanding" name="form[multilanding]" type="checkbox" @if (isset($offer) && ($offer->multilanding == 1)) checked @endif />
                                                        <label for="multilanding" class="label-primary"></label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>{{ trans('ui.offer_logo') }}</td>
                                                <td>
                                                    <input required class="form-control" type="text" placeholder="50x50" name="form[logo]" @if (isset($offer) && isset($offer->logo))value="{{$offer->logo}}"@endif>
                                                </td>
                                            </tr>

                                            {{--<tr>--}}
                                                {{--<td>{{ trans('ui.server') }}</td>--}}
                                                {{--<td>--}}
                                                    {{--<select required class="form-control select2_group" id="server"--}}
                                                            {{--name="form[server_id]">--}}
                                                        {{--<option value="">{{ trans('ui.select') }}</option>--}}
                                                        {{--@if (isset($servers))--}}
                                                            {{--@foreach ($servers as $server)--}}
                                                                {{--<option value="{{ $server->id }}"--}}
                                                                        {{--@if (isset($offer) && isset($offer->server->server_id) && ($offer->server->server_id == $server->id))--}}
                                                                        {{--selected--}}
                                                                        {{--@endif--}}
                                                                {{-->{{ $server->name }}</option>--}}
                                                            {{--@endforeach--}}
                                                        {{--@endif--}}
                                                    {{--</select>--}}
                                                {{--</td>--}}
                                            {{--</tr>--}}

                                            @if ((isset($offer) && ($offer->multilanding == 0)) || !isset($offer))
                                            <tr>
                                                <td>{{ trans('ui.offer_domain') }}</td>
                                                <td>
                                                    <input id="hostname" title="{{ trans('ui.offer_domain') }}" required class="form-control" type="text" name="form[hostname]" @if (isset($offer) && isset($offer->hostname))value="{{$offer->hostname}}"@endif>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>{{ trans('ui.offer_params') }}</td>
                                                <td>
                                                    <input id="params" title="{{ trans('ui.offer_params') }}" required class="form-control" type="text" name="form[params]" @if (isset($offer) && isset($offer->params))value="{{$offer->params}}"@endif>
                                                </td>
                                            </tr>

                                            {{--<tr>--}}
                                                {{--<td>{{ trans('ui.offer_path') }}</td>--}}
                                                {{--<td>--}}
                                                    {{--<input required class="form-control js-path-mask" type="text" name="form[path]" @if (isset($offer) && isset($offer->path))value="{{$offer->path}}"@endif>--}}
                                                {{--</td>--}}
                                            {{--</tr>--}}

                                            <tr>
                                                <td>{{ trans('ui.offer_price') }}</td>
                                                <td>
                                                    <div class="region-rows" id="region-prices">
                                                        @if (isset($offer) && !empty($offer->prices))
                                                            @foreach($offer->prices as $key => $price)
                                                                <div class="region-row">
                                                                    <select style="width: 200px;" required class="form-control select2_group" id="server" name="form[prices][region_id][]">
                                                                        <option value="">{{ trans('ui.select') }}</option>
                                                                        @if (isset($regions))
                                                                            @foreach ($regions as $region)
                                                                                <option value="{{ $region->id }}"
                                                                                    @if ($price->region_id == $region->id)
                                                                                    selected
                                                                                    @endif
                                                                                >{{ $region->name }}, {{ $region->currency }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>

                                                                    <input required class="form-control offer-region-price" type="number" placeholder="100" min="1" name="form[prices][price][]" value="{{$price->price}}">

                                                                    <a class="price-add-btn control-label btn btn-primary" id="add-street"><i class="fa fa-plus" aria-hidden="true"></i></a>

                                                                    @if ($key != 0)
                                                                        <a class="price-remove-btn control-label btn btn-danger" id="add-street"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                                    @endif
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="region-row">
                                                                <select style="width: 200px;" required class="form-control select2_group" id="server" name="form[prices][region_id][]">
                                                                    <option value="">{{ trans('ui.select') }}</option>
                                                                    @if (isset($regions))
                                                                        @foreach ($regions as $region)
                                                                            <option value="{{ $region->id }}">{{ $region->name }}, {{ $region->currency }}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>

                                                                <input required class="form-control offer-region-price" type="number" placeholder="100" min="1" name="form[prices][price][]" @if (isset($offer) && isset($offer->name))value="{{$offer->name}}"@endif>

                                                                <a class="price-add-btn control-label btn btn-primary" id="add-street"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                                            </div>
                                                        @endif
                                                        <div class="region-row-hidden" style="display: none;">
                                                            <select style="width: 200px;" required class="form-control" id="server" name="form[prices][region_id][]">
                                                                <option value="">{{ trans('ui.select') }}</option>
                                                                @if (isset($regions))
                                                                    @foreach ($regions as $region)
                                                                        <option value="{{ $region->id }}">{{ $region->name }}, {{ $region->currency }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>

                                                            <input required class="form-control offer-region-price" type="number" placeholder="100" min="1" name="form[prices][price][]" @if (isset($offer) && isset($offer->name))value="{{$offer->name}}"@endif>

                                                            <a class="price-add-btn control-label btn btn-primary" id="add-street"><i class="fa fa-plus" aria-hidden="true"></i></a>

                                                            <a class="price-remove-btn control-label btn btn-danger" id="add-street"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endif

                                            <tr>
                                                <td>{{ trans('ui.offer_note') }}</td>
                                                <td>
                                                    <textarea required class="form-control" name="form[note]">@if (isset($offer) && isset($offer->note)){{$offer->note}}@endif</textarea>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="landingContentTab">
                                    @if (isset($offer))
                                        <a class="btn btn-success" href="/dashboard/offer/add-landing/{{$offer->id}}" data-remote="false" data-toggle="modal" data-target="#add-landing-modal">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </a>
                                        <div id="landings-list-wrapper">
                                            @include('admin.offers.block.landings-list')
                                        </div>
                                    @endif
                                </div>

                                @if ((isset($offer) && ($offer->multilanding == 0)) || !isset($offer))
                                    <div role="tabpanel" class="tab-pane" id="secondContentTab">
                                        <table class="table table-bordered table-hover dataTable" role="grid">
                                            <tbody>
                                                <tr>
                                                    <td>UTM метки</td>
                                                    <td width="80%">
                                                        <div class="utm-rows">
                                                            @if (isset($offer) && count($offer->utm))
                                                                @foreach($offer->utm as $key => $utm)
                                                                    <div class="utm-row">

                                                                        <div class="form-group col-sm-2">
                                                                            <input required class="form-control offer-utm-name" type="text" placeholder="utm_medium" name="form[utm][name][]" value="{{$utm->name}}">
                                                                        </div>
                                                                        <div class="form-group col-sm-3">
                                                                            <input required class="form-control offer-utm-value" type="text" name="form[utm][value][]" value="{{$utm->value}}">
                                                                        </div>
                                                                        <div class="form-group col-sm-4">
                                                                            <select required class="form-control select2_group offer-utm-disabled" name="form[utm][disabled][]">
                                                                                <option value="0">{{ trans('ui.select') }}</option>
                                                                                <option @if ($utm->disabled == true) selected @endif value="1">true</option>
                                                                                <option @if ($utm->disabled == false) selected @endif value="0">false</option>
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group col-sm-2">
                                                                            <a class="utm-add-btn control-label btn btn-primary" id="add-street"><i class="fa fa-plus" aria-hidden="true"></i></a>

                                                                            @if ($key != 0)
                                                                                <a class="utm-remove-btn control-label btn btn-danger" id="add-street"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            @else
                                                                <div class="utm-row">
                                                                    <div class="form-group col-sm-2">
                                                                        <input required class="form-control offer-utm-name" type="text" placeholder="utm_medium" name="form[utm][name][]">
                                                                    </div>
                                                                    <div class="form-group col-sm-3">
                                                                        <input required class="form-control offer-utm-value" type="text" name="form[utm][value][]">
                                                                    </div>
                                                                    <div class="form-group col-sm-4">
                                                                        <select required class="form-control select2_group offer-utm-disabled" name="form[utm][disabled][]">
                                                                            <option value="0">{{ trans('ui.select') }}</option>
                                                                            <option value="1">true</option>
                                                                            <option value="0">false</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="form-group col-sm-2">
                                                                        <a class="utm-add-btn control-label btn btn-primary" id="add-street"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            <div class="utm-row-hidden" style="display: none;">
                                                                <div class="form-group col-sm-2">
                                                                    <input required class="form-control offer-utm-name" type="text" placeholder="utm_medium" name="form[utm][name][]">
                                                                </div>
                                                                <div class="form-group col-sm-3">
                                                                    <input required class="form-control offer-utm-value" type="text" name="form[utm][value][]">
                                                                </div>
                                                                <div class="form-group col-sm-4">
                                                                    <select style="width: 192px;" required class="form-control offer-utm-disabled" name="form[utm][disabled][]">
                                                                        <option value="0">{{ trans('ui.select') }}</option>
                                                                        <option value="1">true</option>
                                                                        <option value="0">false</option>
                                                                    </select>
                                                                </div>

                                                                <div class="form-group col-sm-2">
                                                                    <a class="utm-add-btn control-label btn btn-primary" id="add-street"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                                                    <a class="utm-remove-btn control-label btn btn-danger" id="add-street"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div role="tabpanel" class="tab-pane" id="thirdContentTab">
                                        <table class="table table-bordered table-hover dataTable" role="grid">
                                            <tbody>
                                                <tr>
                                                    <td>ID счетчика</td>
                                                    <td>
                                                        <input required class="form-control" type="text" name="form[metrika_counter_id]" @if (isset($offer) && isset($offer->metrika_counter_id))value="{{$offer->metrika_counter_id}}"@endif>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>ID Цели "Страница активации"</td>
                                                    <td>
                                                        <input required class="form-control" type="text" name="form[metrika_goal_activation_id]" @if (isset($offer) && isset($offer->metrika_goal_activation_id))value="{{$offer->metrika_goal_activation_id}}"@endif>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>UTM партнерки</td>
                                                    <td>
                                                        <select style="width: 250px;" class="form-control select2_group" name="form[metrika_utm_panel]">
                                                            @if (isset($offer))
                                                                @foreach($offer->utm as $key => $utm)
                                                                    @if ($utm->value)
                                                                        <option value="{{$utm->name}}" @if (isset($offer) && isset($offer->metrika_utm_panel) && ($offer->metrika_utm_panel == $utm->name)) selected @endif>{{$utm->name}}: {{$utm->value}}</option>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>UTM WM</td>
                                                    <td>
                                                        <select style="width: 250px;" class="form-control select2_group" name="form[metrika_utm_wm]">
                                                            @if (isset($offer))
                                                                @foreach($offer->utm as $key => $utm)
                                                                    @if ($utm->value)
                                                                        <option value="{{$utm->name}}"  @if (isset($offer) && isset($offer->metrika_utm_wm) && ($offer->metrika_utm_wm == $utm->name)) selected @endif>{{$utm->name}}: {{$utm->value}}</option>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <hr />
                            <h5>UTM shortcode's:</h5><br/>
                            <ul>
                                <li><strong>user_id</strong> – ID пользователя, генерирующего метку;</li>
                                <li><strong>username</strong> – Username пользователя, генерирующего метку;</li>
                                <li><strong>offername</strong> – {{ trans('ui.offer_name') }};</li>
                                <li><strong>offer_id</strong> – Offer ID;</li>
                                <li><strong>option</strong> – Элемент списка</li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </form>
    </div>


    @if (isset($offer))
        <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="add-landing-modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content live-form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">{{ trans('ui.add_landing') }}</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer ">
                        <div class="pull-right">
                            <button type="button" class="btn btn-primary" id="save-landing">{{ trans('ui.save') }}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('ui.cancel') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="edit-landing-modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content live-form">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">{{ trans('ui.edit_landing') }}</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer ">
                        <div class="pull-right">
                            <button type="button" class="btn btn-primary" id="edit-landing">{{ trans('ui.save') }}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('ui.cancel') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('bottomScript')
    <script>
        var addLandingModal = $('#add-landing-modal'),
            editLandingModal = $('#edit-landing-modal')
        ;

        $('#save-landing').on('click', function () {
            var self = $(this),
                form = $('form#add-landing-form')
            ;

            preloaderShow();

            $.post('/dashboard/offer/add-landing/', form.serialize())
                .done(function (data) {
                    $('#landings-list-wrapper').html(data);

                    addLandingModal.modal('hide');
                    preloaderHide();
                })
                .fail(preloaderHide)
            ;
        });

        $('#edit-landing').on('click', function () {
            var self = $(this),
                form = $('form#add-landing-form')
            ;

            preloaderShow();

            $.post('/dashboard/offer/edit-landing/', form.serialize())
                .done(function (data) {
                    $('#landings-list-wrapper').html(data);

                    editLandingModal.modal('hide');
                    preloaderHide();
                })
                .fail(preloaderHide)
            ;
        });

        [addLandingModal, editLandingModal].forEach(function(modal) {
            modal.on('show.bs.modal', function(e) {
                preloaderShow();
                var link = $(e.relatedTarget);
                var body = $(this).find('.modal-body');
                body.load(link.attr('href'), function () {
                    body.find('select.select2_group').select2();

                    preloaderHide();
                });
            });
        });

        $('#multilanding').on('change', function () {
            var fadeSpeed = 300;

            var el = $(this),
                checked = el.is(':checked');

            var landingContentControl = $('#landingContentControl').parent('li'),
                utmContentControl = $('#utmContentControl').parent('li'),
                metrikaContentControl = $('#metrikaContentControl').parent('li')
            ;

            var mainElements = [$('#region-prices'), $('#hostname'), $('#params')];

            if (checked) {
                landingContentControl.show();
                utmContentControl.hide();
                metrikaContentControl.hide();

                $.each(mainElements, function (i, el) {
                    el.parent('td').parent('tr').hide();
                });
            } else {
                landingContentControl.hide();
                utmContentControl.show();
                metrikaContentControl.show();

                $.each(mainElements, function (i, el) {
                    el.parent('td').parent('tr').show();
                });
            }
        });

        $(document).on('click', '.price-add-btn', function() {
            var self = $(this);

            var parentDiv = self.parent('.region-row'),
                regionSelect = parentDiv.find('select'),
                regionPrice = parentDiv.find('input.offer-region-price');

            var regionsWrapper = $('.region-rows'),
                regionHidden = $('.region-row-hidden');

            if (regionPrice.val() != '' && regionSelect.val() != '') {
                var newRow = regionHidden.clone();

                newRow.removeClass('region-row-hidden').addClass('region-row').show();
                newRow.find('select').select2();
                regionsWrapper.append(newRow);
            } else {
                showError('Заполните предыдущие поля');
            }
        });

        $(document).on('click', '.utm-add-btn', function() {
            var self = $(this);

            var parentDiv = self.parent('.form-group').parent('.utm-row'),
                utmName = parentDiv.find('input.offer-utm-name'),
                utmValue = parentDiv.find('input.offer-utm-value'),
                utmDisabled = parentDiv.find('select.offer-utm-disabled');

            var utmWrapper = $('.utm-rows'),
                utmHidden = $('.utm-row-hidden');

            if (utmName.val() != '') {
                var newRow = utmHidden.clone();

                newRow.removeClass('utm-row-hidden').addClass('utm-row').show();
                newRow.find('select').select2();

                utmWrapper.append(newRow);
            } else {
                showError('Заполните предыдущие поля');
            }
        });

        $(document).on('click', '.price-remove-btn', function() {
            $(this).parent('.region-row').remove();
        });

        $(document).on('click', '.utm-remove-btn', function() {
            $(this).parent('.form-group').parent('.utm-row').remove();
        });
    </script>
@endsection