@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.menu_report_total') }}
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_header">
                <div class="row tile_count">
                    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-user"></i> Всего апрувов</span>
                        <div class="count">{{$totalApproves}}</div>
                        <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>44% </i> С прошлой недели</span>
                        {{--{{$lastWeekApproves}}--}}
                    </div>
                    <div style="opacity: 0.2">
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                            <span class="count_top"><i class="fa fa-clock-o"></i> Всего в ожидании</span>
                            <div class="count">0</div>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>3% </i> С прошлой недели</span>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                            <span class="count_top"><i class="fa fa-user"></i> Total Males</span>
                            <div class="count green">2,500</div>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                            <span class="count_top"><i class="fa fa-user"></i> Total Females</span>
                            <div class="count">4,567</div>
                            <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                            <span class="count_top"><i class="fa fa-user"></i> Total Collections</span>
                            <div class="count">2,315</div>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                            <span class="count_top"><i class="fa fa-user"></i> Total Connections</span>
                            <div class="count">7,325</div>
                            <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <form class="form-inline" role="form" name="search-filter" data-wrapper="statistic-list-wrap" data-cached="true" action="{{ url('/dashboard/reports/total/search') }}">
                <div class="districs custom-filters">
                    <div class="form-group" style="width: 160px; height: 63px;" id="by-date">
                        <label class="filter-col">{{ trans('ui.by_date') }}:</label>
                        <a class="period" data-toggle="modal" data-target="#statistic-filter-modal">{{$startDate}} - {{$endDate}}</a>
                        <input type="hidden" name="search[start-date]" value="">
                        <input type="hidden" name="search[end-date]" value="">
                    </div>

                    <div class="inline-el">

                        <button type="submit" class="margin-clear btn btn-primary">
                            <span class="glyphicon glyphicon-search"></span> {{ trans('ui.search') }}
                        </button>

                        <button type="reset" class="margin-clear btn btn-primary">
                            <span class="glyphicon glyphicon-reset"></span> {{ trans('ui.reset') }}

                        </button>
                    </div>
                </div>
            </form>

            @include('admin.statistic.date-filter', ['location' => 'report'])

            <div class="x_content">
                <div id="statistic-list-wrap">
                    @include('admin.report.total-statistic-list')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('bottomScript')
    <script type="application/javascript" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


    <script>
        var cols;

        cols = [1, 2, 4, 5];

        $(document).ready(function() {
            var currentTable = $('#statistic-list-wrap table');

            $('.js-datepicker').datepicker({
                dateFormat: 'dd.mm.yy',
                maxDate: new Date()
            });

            currentTable.DataTable({
                "bFilter" : false,
                "bLengthChange": false,
                language: {
                    paginate: {
                        next: '&#8594;', // or '→'
                        previous: '&#8592;', // or '←'
                    },
                },
                bInfo: false,
                dom: 'Bfrtip',
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    cols.forEach(function (colIndex) {
                        // Total over all pages
                        var total = api
                            .column( colIndex )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

//                        // Total over this page
//                        pageTotal = api
//                            .column( colIndex, { page: 'current'} )
//                            .data()
//                            .reduce( function (a, b) {
//                                return intVal(a) + intVal(b);
//                            }, 0 );

                        // Update footer
                        $( api.column( colIndex ).footer() ).html(total);
                    });
                }
            });
        });

        window.dataTableCallback = function () {
            $('#statistic-list-wrap table').DataTable().destroy();
            $('#statistic-list-wrap table').DataTable({
                "bFilter" : false,
                "bLengthChange": false,
                dom: 'Bfrtip',
                language: {
                    paginate: {
                        next: '&#8594;', // or '→'
                        previous: '&#8592;' // or '←'
                    }
                },
                bInfo: false,
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    cols.forEach(function (colIndex) {
                        // Total over all pages
                        var total = api
                            .column( colIndex )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

//                        // Total over this page
//                        pageTotal = api
//                            .column( colIndex, { page: 'current'} )
//                            .data()
//                            .reduce( function (a, b) {
//                                return intVal(a) + intVal(b);
//                            }, 0 );

                        // Update footer
                        $( api.column( colIndex ).footer() ).html(total);
                    });
                }
            });
        }
    </script>
@endsection