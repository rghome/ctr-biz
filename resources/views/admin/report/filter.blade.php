<form class="form-inline" role="form" name="search-filter" data-wrapper="statistic-list-wrap" data-cached="true" action="{{ url('/dashboard/reports/search') }}?sort={{$sort}}">
	<div class="districs custom-filters">
		<div class="form-group" style="width: 160px; height: 63px;" id="by-date">
			<label class="filter-col">{{ trans('ui.by_date') }}:</label>
			<a class="period" data-toggle="modal" data-target="#statistic-filter-modal">{{$startDate}} - {{$endDate}}</a>
			<input type="hidden" name="search[start-date]" value="">
			<input type="hidden" name="search[end-date]" value="">
		</div>

		<div class="form-group" style="width: 200px;">
			<label class="filter-col">{{ trans('ui.by_user') }}:</label>
			<select class="form-control search-filter" name="search[by_user]">
				<option value="0">{{ trans('ui.select') }}</option>
				@foreach ($users as $user)
					<option value="{{ $user->id }}">{{ $user->id }} | {{ $user->name }}</option>
				@endforeach
			</select>
		</div>

		{{--<div class="form-group">--}}
			{{--<label class="filter-col">{{ trans('ui.by_offer') }}:</label>--}}
			{{--<select class="form-control search-filter" id="statistic_by_date" name="search[by_offer]">--}}
				{{--<option value="0">{{ trans('ui.select') }}</option>--}}
				{{--@foreach ($offers as $offer)--}}
					{{--<option value="{{ $offer->id }}">{{ $offer->name }}</option>--}}
				{{--@endforeach--}}
			{{--</select>--}}
		{{--</div>--}}

		<div class="inline-el">

			<button type="submit" class="margin-clear btn btn-primary">
				<span class="glyphicon glyphicon-search"></span> {{ trans('ui.search') }}
			</button>

			<button type="reset" class="margin-clear btn btn-primary">
				<span class="glyphicon glyphicon-reset"></span> {{ trans('ui.reset') }}

			</button>
		</div>
	</div>
</form>

@include('admin.statistic.date-filter', ['location' => 'report'])
