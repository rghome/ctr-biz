<table id="example2"  cellspacing="0" class="table table-header-center table-row-center  table-striped table-bordered">
    <thead>
    <tr role="row">
        <th>Оффер</th>

        <th>{{ trans('ui.hits') }}</th>
        <th>{{ trans('ui.hosts') }}</th>
        <th>{{ trans('ui.cr') }}%</th>
        <th>{{ trans('ui.views_thanks') }}</th>
        {{--<th>В обработке, руб</th>--}}
        <th>Сумма дохода, руб</th>
    </tr>
    </thead>
    <tfoot>
    <tr role="row">
        <th>ИТОГО</th>
        <th>0</th>
        <th>0</th>
        <th colspan="1">-</th>
        <th>0</th>
        {{--<th>0</th>--}}
        <th>0</th>
    </tr>
    </tfoot>
    <tbody>
    @forelse ($totalByOffer as $k => $c)
        <tr role="row" @if ($c['status'] != \App\Models\Offer::STATUS_OPEN) style="background: rgba(244, 100, 95, 0.12);" @endif>
            <td>{{$c['name']}}</td>

            <td>{{$c['total_users']}}</td>
            <td>{{$c['new_users']}}</td>
            <td>@if ($c['new_users']) {{round($c['total_approves']/$c['new_users']*100, 1)}} @else 0 @endif</td>
            <td>{{$c['total_approves']}}</td>
            {{--<td>{{$c['payment_hold']}}</td>--}}
            <td>{{$c['payment_free']}}</td>
        </tr>
    @empty
        <tr role="row">
            <td colspan="6" style="text-align: center;">{{trans('ui.not_found')}}</td>
        </tr>
    @endforelse
    </tbody>
</table>