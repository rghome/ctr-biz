<table id="example2"  cellspacing="0" class="table table-header-center table-row-center  table-striped table-bordered">
	<thead>
		<tr role="row">
{{--			<th width="15%">{{ trans('ui.domain') }}</th>--}}
			<th>@if ($sort === 'landings') Лендинги @elseif ($sort === 'days') Дата @else {{ trans('ui.offers') }} @endif</th>

			<th>{{ trans('ui.hits') }}</th>
			<th>{{ trans('ui.hosts') }}</th>
			<th>{{ trans('ui.cr') }}%</th>
			{{--<th>{{ trans('ui.views_activation') }}</th>--}}
			<th>{{ trans('ui.views_thanks') }}</th>
			{{--<th>В обработке, руб</th>--}}
			<th>Сумма дохода, руб</th>
			{{--<th>{{ trans('ui.district') }}</th>--}}
			{{--<th>{{ trans('ui.gorod') }}</th>--}}
			{{--<th>{{ trans('ui.street') }}</th>--}}
			{{--<th>{{ trans('ui.home') }}</th>--}}
			{{--<th>{{ trans('ui.act') }}</th>--}}
		</tr>
	</thead>
	<tfoot>
		<tr role="row">
			{{--<th width="15%">{{ trans('ui.domain') }}</th>--}}

			<th>ИТОГО</th>
			<th>0</th>
			<th>0</th>
			<th colspan="1">-</th>
			{{--<th>0</th>--}}
			<th>0</th>
			{{--<th>0</th>--}}
			<th>0</th>
			{{--<th>{{ trans('ui.district') }}</th>--}}
			{{--<th>{{ trans('ui.gorod') }}</th>--}}
			{{--<th>{{ trans('ui.street') }}</th>--}}
			{{--<th>{{ trans('ui.home') }}</th>--}}
			{{--<th>{{ trans('ui.act') }}</th>--}}
		</tr>
	</tfoot>
	<tbody>
		@forelse ($data as $k => $c)
			<tr role="row">
				{{--<td>{{$c->domain->name or 'Domain deleted!'}}</td>--}}
				<td>{{$c['name']}}</td>


				<td>{{$c['total_users']}}</td>
				<td>{{$c['new_users']}}</td>
				<td>@if ($c['new_users']) {{round($c['total_approves']/$c['new_users']*100, 1)}} @else 0 @endif</td>
				{{--<td>{{$c->page2_count}}</td>--}}
				{{--<td>{{$c['page2_unique']}}</td>--}}
				{{--<td>{{$c->page3_count}}</td>--}}
				<td>{{$c['total_approves']}}</td>
				{{--<td>{{$c['payment_hold']}}</td>--}}
				<td>{{$c['payment_free']}}</td>
			</tr>
		@empty
			<tr role="row">
				<td colspan="6" style="text-align: center;">{{trans('ui.not_found')}}</td>
			</tr>
		@endforelse
	</tbody>
</table>

{{--{{ $data->links() }}--}}