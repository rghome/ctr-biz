<table id="example2" class="table members-table table-bordered table-hover dataTable">
    <thead>

    <tr role="row">
        <th  width="2%">#</th>
        <th>{{ trans('ui.name') }}</th>
        <th>{{ trans('ui.code') }}</th>
        <th>{{ trans('ui.currency') }}</th>
        <th width="10%">{{ trans('ui.act') }}</th>
    </tr>
    </thead>
    <tbody>
    @forelse ($data as $c)
        <tr role="row" class="odd">
            <td class="sorting_1">{{$c->id}}</td>
            <td>{{$c->name}}</td>
            <td>{{$c->code}}</td>
            <td>{{$c->currency}}</td>
            <td>
                <div class="btn-group">
                    @if(RoleVerifier::can('region.edit'))
                        <a class="btn  btn-primary " href="/dashboard/region/edit/{{ $c->id }}"><i class="fa fa-fw fa-edit"></i></a>
                    @endif
                    @if(RoleVerifier::can('region.delete'))
                        <a class="btn btn-danger delete_region" data-region-id="{{ $c->id }}"><i class="fa fa-fw fa-remove"></i></a>
                    @endif
                </div>
            </td>
        </tr>
    @empty
        <tr role="row">
            <td colspan="4" style="text-align: center;">{{trans('ui.not_found')}}</td>
        </tr>
    @endforelse
    </tbody>
</table>

{{ $data->links() }}