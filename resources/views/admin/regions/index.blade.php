@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.regions') }}
@endsection

@section('bottomScript')
    <script>
        $('.delete_region').on('click', function(e) {
            e.preventDefault();

            var self = $(this);

            if(confirm('Удалить регион?')) {
                location.href = '/dashboard/region/delete/' + self.data('region-id');
            }
        });
    </script>
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <ul class="list-inline top-btns">
                @if(RoleVerifier::can('region.create'))
                    <li>
                        <a class="btn btn-block btn-success " href="/dashboard/region/create">{{ trans('ui.create') }}</a>
                    </li>
                @endif
            </ul>
            <div class="x_content">
                {{--@include('admin.production.filter')--}}
                <div class="scroll-holder" id="regions-list-wrap">
                    @include('admin.regions.regions-list')
                </div>
            </div>
        </div>
    </div>
@endsection