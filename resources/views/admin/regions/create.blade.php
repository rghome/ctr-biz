@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.regions') }}
@endsection

@section('content')
    <div class="col-xs-12">
        <form action="" method="post" id="region-form">
            <div class="box-header">
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9 text-right">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="x_panel">
                <div class="list-inline top-btns">
                    <li>
                        <button class="btn btn-block btn-success">{{ trans('ui.save') }}</button>
                    </li>
                </div>
                <div class="x_content">
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div>
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <tbody>
                                        <tr>
                                            <td>{{ trans('ui.name') }}</td>
                                            <td>
                                                <input required class="form-control" type="text" name="form[name]" @if (isset($region) && isset($region->name))value="{{$region->name}}"@endif>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>{{ trans('ui.code') }}</td>
                                            <td>
                                                <input required class="form-control" type="text" name="form[code]" @if (isset($region) && isset($region->code))value="{{$region->code}}"@endif>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>{{ trans('ui.currency') }}</td>
                                            <td>
                                                <input required class="form-control" type="text" name="form[currency]" @if (isset($region) && isset($region->currency))value="{{$region->currency}}"@endif>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </form>
    </div>
@endsection

@section('bottomScript')

@endsection