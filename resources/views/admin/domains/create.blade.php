@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.domains') }}
@endsection

@section('content')
    <div class="col-xs-12">
        <form action="" method="post" id="domain-form" novalidate>
            <div class="box-header">
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9 text-right">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="x_panel">
                <div class="list-inline top-btns">
                    <li>
                        <button class="btn btn-block btn-success">{{ trans('ui.save') }}</button>
                    </li>
                </div>
                <div class="x_content">
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div>
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <tbody>
                                        <tr>
                                            <td>{{ trans('ui.domain') }}</td>
                                            <td>
                                                <input required class="form-control" type="text" name="form[name]" @if (isset($domain) && isset($domain->name))value="{{$domain->name}}"@endif>
                                            </td>
                                        </tr>

                                        @if (\Illuminate\Support\Facades\Auth::user()->role_id == 1)
                                            <tr>
                                                <td>{{ trans('ui.moderated') }}</td>
                                                <td>
                                                    <input required type="checkbox" name="form[moderate]" @if (isset($domain) && $domain->moderate == 1)checked @endif>
                                                </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <label class="label label-primary" style="font-size: 16px;">Домен будет припаркован в течении 24-х часов.</label><br/>
                            <hr />
                            <label>Для добавления домена редиректа, укажите в настройках наши CNAME записи.</label><br/>
                            <label>Это можно сделать в DNS настройках вашего хостера.</label><br/><br/>
                            <ul style="font-weight: bold;">
                                <li>ns1.digitalocean.com</li>
                                <li>ns2.digitalocean.com</li>
                                <li>ns3.digitalocean.com</li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </form>
    </div>
@endsection

@section('bottomScript')
    <script>
        $(document).ready(function () {
            {{--$('input[name="form[name]"]').mask('http://AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/', {--}}
                {{--translation: {--}}
                    {{--A: {--}}
                        {{--pattern: /[\w@\-.+]/,--}}
                        {{--recursive: true--}}
                    {{--}--}}
                {{--},--}}
                {{--placeholder: "http://"--}}
            {{--});--}}
        });
    </script>
@endsection