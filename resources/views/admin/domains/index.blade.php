@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.domains') }}
@endsection

@section('bottomScript')
    <script>
        $('.delete_domain').on('click', function(e) {
            e.preventDefault();

            var self = $(this);

            if(confirm('Удалить домен?')) {
                location.href = '/dashboard/domain/delete/' + self.data('domain-id');
            }
        });
    </script>
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <ul class="list-inline top-btns">
                @if(RoleVerifier::can('domain.create'))
                    <li>
                        <a class="btn btn-block btn-success " href="/dashboard/domain/create">{{ trans('ui.create') }}</a>
                    </li>
                @endif
            </ul>
            <div class="x_content">
                {{--@include('admin.production.filter')--}}
                <div class="scroll-holder" id="domains-list-wrap">
                    @include('admin.domains.domains-list')
                </div>
            </div>
        </div>
    </div>
@endsection