<table id="example2" class="table members-table table-bordered table-hover dataTable">
    <thead>

    <tr role="row">
        <th width="2%">#</th>
        <th>{{ trans('ui.name') }}</th>
        <th>{{ trans('ui.domain_status') }}</th>
        {{--<th>{{ trans('ui.offer') }}</th>--}}
        @if(RoleVerifier::can('domain.edit') || RoleVerifier::can('domain.delete'))<th width="10%">{{ trans('ui.act') }}</th>@endif
    </tr>
    </thead>
    <tbody>
    @forelse ($data as $c)
        <tr role="row" class="odd">
            <td class="sorting_1">{{$c->id}}</td>
            <td><a class="domain-link" target="_blank" href="http://{{$c->name}}/">{{$c->name}}</a> <i class="fa fa-fw fa-external-link"></i></td>
            <td>
                @if($c->moderate === 1)
                    <label style="font-size: 13px;" class="label label-success">Проверен</label>
                @elseif ($c->moderate === 0)
                    <label style="font-size: 13px;" class="label label-primary">В ожидании</label>
                @elseif ($c->moderate === 2)
                    <label style="font-size: 13px;" class="label label-warning">Ошибка:</label> @if (isset($c->comment)) {{$c->comment}} @endif
                @endif
            </td>
            {{--<td>{{$c->offer->name}}</td>--}}
            @if(RoleVerifier::can('domain.edit') || RoleVerifier::can('domain.delete'))
            <td>
                <div class="btn-group">
                    @if(RoleVerifier::can('domain.edit'))
                        <a class="btn  btn-primary " href="/dashboard/domain/edit/{{ $c->id }}"><i class="fa fa-fw fa-edit"></i></a>
                    @endif
                    @if(RoleVerifier::can('domain.delete'))
                        <a class="btn btn-danger delete_domain" data-domain-id="{{ $c->id }}"><i class="fa fa-fw fa-remove"></i></a>
                    @endif
                </div>
            </td>
            @endif
        </tr>
    @empty
        <tr role="row">
            <td colspan="4" style="text-align: center;">{{trans('ui.not_found')}}</td>
        </tr>
    @endforelse
    </tbody>
</table>

{{ $data->links() }}