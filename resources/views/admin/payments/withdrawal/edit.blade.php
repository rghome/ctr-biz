@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.offers') }}
@endsection

@section('content')
    <div class="col-xs-12">
        <form action="" method="post" id="offer-form" novalidate>
            <div class="box-header">
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9 text-right">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="x_panel">
                <div class="list-inline top-btns">
                    <li>
                        <button class="btn btn-block btn-success">{{ trans('ui.save') }}</button>
                    </li>
                </div>
                <div class="x_content">
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div>
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <tbody>
                                    <tr>
                                        <td>{{trans('ui.payment_status')}}</td>
                                        <td>
                                            <select title="{{trans('ui.payment_status')}}" required class="form-control select2_group" name="form[status]">
                                                <option value="">{{ trans('ui.select') }}</option>
                                                <option @if ((isset($payment) && isset($payment->status)) && ($payment->status == 1)) selected @endif value="1">В ожидании</option>
                                                <option @if ((isset($payment) && isset($payment->status)) && ($payment->status == 2)) selected @endif value="2">Оплаченно</option>
                                                <option @if ((isset($payment) && isset($payment->status)) && ($payment->status == 3)) selected @endif value="3">Отмененно</option>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>{{ trans('ui.comment') }}</td>
                                        <td>
                                            <textarea class="form-control" title="{{ trans('ui.comment') }}" name="form[comment]"> @if (isset($payment) && isset($payment->comment)){{$payment->comment}}@endif</textarea>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </form>
    </div>
@endsection

@section('bottomScript')
@endsection