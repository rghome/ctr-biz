@extends('layouts.admin')
@section('pageTitle')
    Запросы
@endsection

@section('bottomScript')

@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="x_panel">
            <div class="x_content">
                <div class="scroll-holder" id="payments-list-wrap">
                    <table id="example2" class="table members-table table-bordered table-hover dataTable">
                        <thead>

                        <tr role="row">
                            <th width="5%">User ID</th>
                            <th>{{ trans('ui.user') }}</th>
                            <th>{{ trans('ui.sum') }}</th>
                            <th>{{ trans('ui.payment_status') }}</th>
                            <th>{{ trans('ui.output_method') }}</th>
                            <th>{{ trans('ui.comment') }}</th>
                            <th>{{ trans('ui.date') }}</th>
                            <th>{{ trans('ui.act') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($allWaiting as $c)
                            <tr role="row" class="odd">
                                <td class="sorting_1">{{$c->user->id}}</td>
                                <td>@if ($c->user->warnings)<span style="font-size: 13px; font-weight: normal;" class="label label-danger">{{$c->user->name}} | Warnings: {{$c->user->warnings}}</span> @else {{$c->user->name}} @endif</td>
                                <td>{{$c->total}}</td>
                                <td><span class="label label-@if ($c->status == 1)primary @elseif($c->status == 2)success @elseif($c->status == 3)danger @endif payment-status">@if ($c->status == 1)В ожидании@elseif($c->status == 2)Оплаченно@elseif($c->status == 3)Отмененно@endif</span></td>
                                <td>{{$c->output}}</td>
                                <td>{{$c->comment}}</td>
                                <td>{{$c->created_at->format('d-m-Y')}}</td>
                                <td>
                                    <div class="btn-group">
                                        @if(RoleVerifier::can('payment.edit'))
                                            <a class="btn  btn-primary " title="Редактировать" href="/dashboard/payments/withdrawals/edit/{{ $c->id }}"><i class="fa fa-fw fa-edit"></i></a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr role="row">
                                <td colspan="8" style="text-align: center;">{{trans('ui.not_found')}}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    {{ $allWaiting->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
