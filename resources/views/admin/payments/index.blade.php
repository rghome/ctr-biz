@extends('layouts.admin')
@section('pageTitle')
    Доступно к выводу: {{(new App\Services\CpaAdapter())->getTotalFreeWrapper()}} руб.
@endsection

@section('bottomScript')
    <script>
        $('#withdrawal').submit(function (e) {
            e.preventDefault();
            var form = $(this);

            var request = $.post('{{ url('/dashboard/payment/withdrawal') }}', form.serialize());

            request.error(function (response) {
                return showError(response.responseJSON.message);
            });

            request.done(function (response) {
                var payment = response.data.payment;

                showSuccess('Запрос на сумму ' + payment.total + ' руб. отправлен на рассмотрение администратору.');

                setTimeout(function () {
                    location.reload();
                }, 2000);
            });

        });

        $('.delete_payment').on('click', function(e) {
            e.preventDefault();

            var self = $(this);

            if(confirm('Удалить запрос?')) {
                location.href = '/dashboard/payment/delete/' + self.data('payment-id');
            }
        });
    </script>
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <form class="form-inline" id="withdrawal">
                    <div class="form-group" style="height: 39px;">
                        <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                        <div class="input-group">
                            {{--<div class="input-group-addon">$</div>--}}
                            <input type="text" class="form-control" id="exampleInputAmount" name="amount" placeholder="Сумма">
                            <div class="input-group-addon">pуб.</div>
                        </div>
                    </div>
                    <div class="inline-el">
                        <button type="submit" class="btn btn-primary">Запросить вывод</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="x_panel">
            <div class="x_title">
                <h5><i class="fa fa-list-alt"></i> История</h5>
            </div>
            <div class="x_content">
                <div class="scroll-holder" id="payments-list-wrap">
                    @include('admin.payments.payments-list')
                </div>
            </div>
        </div>
    </div>
@endsection
