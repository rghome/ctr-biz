<table id="example2" class="table members-table table-bordered table-hover dataTable">
    <thead>

    <tr role="row">
        {{--<th width="2%">#</th>--}}
        <th>{{ trans('ui.date') }}</th>
        <th>{{ trans('ui.payment_status') }}</th>
        <th>{{ trans('ui.output_method') }}</th>
        <th width="50%">{{ trans('ui.comment') }}</th>
        <th>{{ trans('ui.sum') }}</th>
        <th>{{ trans('ui.act') }}</th>
    </tr>
    </thead>
    <tbody>
    @forelse ($data as $c)
        <tr role="row" class="odd">
            {{--<td class="sorting_1">{{$c->id}}</td>--}}
            <td>{{$c->created_at->format('d-m-Y')}}</td>
            <td><span class="label label-@if ($c->status == 1)primary @elseif($c->status == 2)success @elseif($c->status == 3)danger @endif payment-status">@if ($c->status == 1)В ожидании@elseif($c->status == 2)Оплаченно@elseif($c->status == 3)Отмененно@endif</span></td>
            <td>{{$c->output}}</td>
            <td>{{$c->comment}}</td>
            <td><strong>{{$c->total}}</strong></td>
            <td>
                <a class="btn btn-danger delete_payment @if ($c->status == 2) disabled @endif" data-payment-id="{{ $c->id }}" ><i class="fa fa-fw fa-remove"></i></a>
            </td>
        </tr>
    @empty
        <tr role="row">
            <td colspan="6" style="text-align: center;">{{trans('ui.not_found')}}</td>
        </tr>
    @endforelse
    </tbody>
</table>

{{ $data->links() }}