@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.menu_news') }}
@endsection

@section('bottomScript')

    <script>
        $('.delete_news').on('click', function(e) {
            e.preventDefault();

            var self = $(this);

            if(confirm('Удалить новость?')) {
                location.href = '/dashboard/news/delete/' + self.data('news-id');
            }
        });
    </script>
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <ul class="list-inline top-btns">
                @if(RoleVerifier::can('news.create'))
                    <li>
                        <a class="btn btn-block btn-success " href="/dashboard/news/create">{{ trans('ui.create') }}</a>
                    </li>
                @endif
            </ul>
            <div class="x_content">
                <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">

                        @forelse ($data as $c)
                        <li>
                            <div class="block">
                                <div class="block_content">
                                    <h2 class="title">
                                        <a><h3>{{$c->title}}</h3></a>
                                    </h2>
                                    <div class="byline">
                                        <span>{{$c->created_at->diffForHumans()}}</span> <a>Admin</a>
                                    </div>
                                    {{$c->text}}
                                </div>
                            </div>
                        </li>
                        @empty
                            <li>
                                <div class="block">
                                    <div class="block_content">
                                        <h2 class="title">
                                            <a>Новость</a>
                                        </h2>
                                        <div class="byline">
                                            <span>13 hours ago</span> <a>Admin</a>
                                        </div>
                                        Скоро будет
                                    </div>
                                </div>
                            </li>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
