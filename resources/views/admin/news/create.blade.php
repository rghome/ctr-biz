@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.menu_news') }}
@endsection

@section('content')
    <div class="col-xs-12">
        <form action="" method="post" id="news-form" novalidate>
            <div class="box-header">
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9 text-right">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="x_panel">
                <div class="list-inline top-btns">
                    <li>
                        <button class="btn btn-block btn-success">{{ trans('ui.save') }}</button>
                    </li>
                </div>
                <div class="x_content">
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div>
                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <tbody>
                                        <tr>
                                            <td>{{ trans('ui.offer_name') }}</td>
                                            <td>
                                                <input required class="form-control" type="text" name="form[title]" @if (isset($news) && isset($news->title))value="{{$news->title}}"@endif>
                                            </td>
                                        </tr>

                                        {{--<tr>--}}
                                            {{--<td>{{ trans('ui.offer_logo') }}</td>--}}
                                            {{--<td>--}}
                                                {{--<input required class="form-control" type="text" name="form[image]" @if (isset($news) && isset($news->image))value="{{$news->image}}"@endif>--}}
                                            {{--</td>--}}
                                        {{--</tr>--}}

                                        <tr>
                                            <td>{{ trans('ui.description') }}</td>
                                            <td>
                                                <textarea required class="form-control" name="form[text]">@if (isset($news) && isset($news->text)){{$news->text}}@endif</textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div><!-- /.box-body -->
            </div>
        </form>
    </div>
@endsection