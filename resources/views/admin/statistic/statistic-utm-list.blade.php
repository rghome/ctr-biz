<table id="example2"  cellspacing="0" class="table table-header-center table-row-center  table-striped table-bordered">
	<thead>
	<tr role="row">
		<th>{{ trans('ui.offer') }}</th>
		<th>@if ($sort === 'utm_term') Потоки @elseif($sort === 'utm_content') Источники трафика @endif</th>

		<th>{{ trans('ui.hits') }}</th>
		<th>{{ trans('ui.hosts') }}</th>
		<th>{{ trans('ui.cr') }}%</th>
		{{--<th>{{ trans('ui.views_activation') }}</th>--}}
		<th>{{ trans('ui.views_thanks') }}</th>
		{{--<th>В обработке, руб</th>--}}
		{{--<th>Сумма дохода, руб</th>--}}
		{{--<th>{{ trans('ui.district') }}</th>--}}
		{{--<th>{{ trans('ui.gorod') }}</th>--}}
		{{--<th>{{ trans('ui.street') }}</th>--}}
		{{--<th>{{ trans('ui.home') }}</th>--}}
		{{--<th>{{ trans('ui.act') }}</th>--}}
	</tr>
	</thead>
	<tfoot>
	<tr role="row">
		{{--<th width="15%">{{ trans('ui.domain') }}</th>--}}

		<th>ИТОГО</th>
		<th>-</th>
		<th>0</th>
		<th>0</th>
		<th colspan="1">-</th>
		{{--<th>0</th>--}}
		<th>0</th>
		{{--<th>0</th>--}}
		{{--<th>0</th>--}}
		{{--<th>{{ trans('ui.district') }}</th>--}}
		{{--<th>{{ trans('ui.gorod') }}</th>--}}
		{{--<th>{{ trans('ui.street') }}</th>--}}
		{{--<th>{{ trans('ui.home') }}</th>--}}
		{{--<th>{{ trans('ui.act') }}</th>--}}
	</tr>
	</tfoot>
	<tbody>
	@forelse ($data as $k => $c)
		<tr role="row">
			{{--<td>{{$c->domain->name or 'Domain deleted!'}}</td>--}}
			<td>{{$c['offer_name']}}</td>
			<td>{{$c['name']}}</td>

			<td>{{$c['total_users']}}</td>
			<td>{{$c['new_users']}}</td>
			<td>@if ($c['new_users']) {{round($c['total_approves']/$c['new_users']*100, 1)}} @else 0 @endif</td>
			{{--<td>{{$c->page2_count}}</td>--}}
			{{--<td>{{$c['page2_unique']}}</td>--}}
			{{--<td>{{$c->page3_count}}</td>--}}
			<td>{{$c['total_approves']}}</td>
			{{--<td>{{$c['payment_hold']}}</td>--}}
			{{--<td>{{$c['payment_free']}}</td>--}}
		</tr>
	@empty
		<tr role="row">
			<td colspan="6" style="text-align: center;">{{trans('ui.not_found')}}</td>
		</tr>
	@endforelse
	</tbody>
</table>

{{--{{ $data->links() }}--}}

<script>
    var data = {!! $chart !!};

    if (!Object.keys(data).length) {
        document.getElementById('chartdiv').hidden = true;
    } else {
        document.getElementById('chartdiv').hidden = false;

        var graphs = [];
        if (data.hasOwnProperty('graphs')) {

            Object.keys(data.graphs).map(function(objectKey, index) {
                var value = data.graphs[objectKey];

                graphs.push({
                    "balloonText": "[[title]]:[[value]]",
                    "fillAlphas": 1,
                    "id": "AmGraph-" + objectKey,
                    "title": value['name'],
                    "type": "column",
                    "valueField": value['id']
                });
            });
        }

        if (chart !== undefined) {
            chart.clear();
        }
        var chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "categoryField": "category",
            "startDuration": 2,
            "categoryAxis": {
                "gridPosition": "start"
            },
            "trendLines": [],
            "graphs": graphs,
            "guides": [],
            "allLabels": [],
            "balloon": {},
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "title": "Достижения цели"
                }
            ],
            "legend": {
                "enabled": true,
                "useGraphSettings": true
            },
            "dataProvider": [data.data]
        });

        chart.addListener("rendered", zoomChart);

        zoomChart();

        function zoomChart() {
            chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);

            var els = document.querySelectorAll("a[href='http://www.amcharts.com/javascript-charts/']");
            if (els.length) {
                els[0].style.display = 'none';
            }
        }
    }

</script>
