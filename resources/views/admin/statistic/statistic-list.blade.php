<table id="example2"  cellspacing="0" class="table table-header-center table-row-center  table-striped table-bordered">
	<thead>
		<tr role="row">
            @if ($sort === 'landings')
			    <th>{{ trans('ui.offer') }}</th>
            @endif
			<th>@if ($sort === 'landings') Лендинги @elseif ($sort === 'days') Дата @else {{ trans('ui.offers') }} @endif</th>

			<th>{{ trans('ui.hits') }}</th>
			<th>{{ trans('ui.hosts') }}</th>
			<th>{{ trans('ui.cr') }}%</th>
			<th>{{ trans('ui.views_thanks') }}</th>
			<th>Сумма дохода, руб</th>
		</tr>
	</thead>
	<tfoot>
		<tr role="row">
			<th>ИТОГО</th>
            @if ($sort === 'landings')
                <th>-</th>
            @endif
			<th>0</th>
			<th>0</th>
			<th colspan="1">-</th>
			<th>0</th>
			<th>0</th>
		</tr>
	</tfoot>
	<tbody>
		@forelse ($data as $k => $c)
			<tr role="row">
                @if ($sort === 'landings')
                    <td>{{$c['offer_name']}}</td>
                @endif
				<td>{{$c['name']}}</td>

				<td>{{$c['total_users']}}</td>
				<td>{{$c['new_users']}}</td>
				<td>@if ($c['new_users']) {{round($c['total_approves']/$c['new_users']*100, 1)}} @else 0 @endif</td>
				<td>{{$c['total_approves']}}</td>
				<td>{{$c['payment_free']}}</td>
			</tr>
		@empty
			<tr role="row">
				<td colspan="@if ($sort === 'landings') 7 @else 6 @endif" style="text-align: center;">{{trans('ui.not_found')}}</td>
			</tr>
		@endforelse
	</tbody>
</table>

{{--{{ $data->links() }}--}}

<script>
    var data = {!! $chart !!};

    @if ($sort === 'days')
        if (!data.length) {
            document.getElementById('chartdiv').hidden = true;
        } else {
            document.getElementById('chartdiv').hidden = false;
        }

        var chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            //            "marginRight": 40,
            //            "marginLeft": 40,
            //            "autoMarginOffset": 20,
            "dataDateFormat": "YYYY-MM-DD",
            "valueAxes": [{
                "id": "v1",
                "axisAlpha": 0,
                "position": "left",
                "ignoreAxisWidth":true
            }],
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "graphs": [{
                "id": "g1",
                "balloon":{
                    "drop":true,
                    "adjustBorderColor":false,
                    "color":"#ffffff"
                },
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "bulletSize": 5,
                "hideBulletsCount": 50,
                "lineThickness": 2,
                "title": "red line",
                "useLineColorForBulletBorder": true,
                "valueField": "total_approves",
                "balloonText": "<p><span style='font-size:18px;'>[[total_approves]]</span></p>"
            }],
            "chartScrollbar": {
                //                "graph": "g1",
                //                "oppositeAxis":false,
                //                "offset":30,
                "scrollbarHeight": 30,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount":true,
                "color":"#AAAAAA"
            },
            "chartCursor": {
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha":1,
                "cursorColor":"#258cbb",
                "limitToGraph":"g1",
                "valueLineAlpha":0.2,
                "valueZoomable":true
            },
            "valueScrollbar":{
                "oppositeAxis":false,
                "offset":50,
                "scrollbarHeight":5
            },
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true,
                "dashLength": 1,
                "minorGridEnabled": true
            },
            "export": {
                "enabled": false
            },
            "dataProvider": data
        });

        chart.addListener("rendered", zoomChart);

        zoomChart();
    @elseif($sort === 'offers' || $sort === 'landings')
        if (!Object.keys(data).length) {
            document.getElementById('chartdiv').hidden = true;
        } else {
            document.getElementById('chartdiv').hidden = false;

            var graphs = [];
            if (data.hasOwnProperty('graphs')) {

                Object.keys(data.graphs).map(function(objectKey, index) {
                    var value = data.graphs[objectKey];

                    graphs.push({
                        "balloonText": "[[title]]:[[value]]",
                        "fillAlphas": 1,
                        "id": "AmGraph-" + objectKey,
                        "title": value['name'],
                        "type": "column",
                        "valueField": value['id']
                    });
                });
            }

            if (chart !== undefined) {
                chart.clear();
            }
            var chart = AmCharts.makeChart("chartdiv", {
                "type": "serial",
                "categoryField": "category",
                "startDuration": 2,
                "categoryAxis": {
                    "gridPosition": "start"
                },
                "trendLines": [],
                "graphs": graphs,
                "guides": [],
                "allLabels": [],
                "balloon": {},
                "valueAxes": [
                    {
                        "id": "ValueAxis-1",
                        "title": "Достижения цели"
                    }
                ],
                "legend": {
                    "enabled": true,
                    "useGraphSettings": true
                },
                "dataProvider": [data.data]
            });

            chart.addListener("rendered", zoomChart);

            zoomChart();
        }
    @endif

    function zoomChart() {
        chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);

        var els = document.querySelectorAll("a[href='http://www.amcharts.com/javascript-charts/']");
        if (els.length) {
            els[0].style.display = 'none';
        }
    }
</script>
