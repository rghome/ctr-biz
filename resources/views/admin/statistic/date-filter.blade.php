<script>
    window['dateRangeStart'] = '{{$startDate}}';
    window['dateRangeEnd'] = '{{$endDate}}';
</script>

<div class="modal fade" tabindex="-1" role="dialog" data-location="{{$location}}" id="statistic-filter-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content live-form">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">По дате</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <table class="table table-bordered table-hover dataTable">
                        <tbody>
                            <tr>
                                <td>
                                    <label for="birthday">Начало пероида:</label>
                                </td>
                                <td>
                                    <input type="text" class="form-control js-datepicker" name="start-date" value="{{$startDate}}" required="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="insemination">Конец периода:</label>
                                </td>
                                <td>
                                    <input type="text" class="form-control js-datepicker" name="end-date" value="{{$endDate}}" required="">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-rigth">
                    <button class="btn btn-primary" type="submit">{{ trans('ui.search') }}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('ui.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>