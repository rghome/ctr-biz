@extends('layouts.admin')
@section('pageTitle')
	{{ trans('ui.statistic') }}
@endsection

@section('content')
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
        <div class="x_header">
            <ul class="nav  nav-tabs">
                <li @if ($sort == 'days') class="active" @endif><a href="{{ url('/dashboard/statistic') }}?sort=days">По дням</a></li>
                {{--<li @if ($sort == 'offers') class="active" @endif><a href="{{ url('/dashboard/statistic') }}?sort=offers">По офферам</a></li>--}}
                <li @if ($sort == 'landings') class="active" @endif><a href="{{ url('/dashboard/statistic') }}?sort=landings">По лендингам</a></li>
                {{--<li @if ($sort == 'utm_source') class="active" @endif><a href="{{ url('/dashboard/statistic') }}?sort=utm_source">По utm_source</a></li>--}}
                {{--<li @if ($sort == 'utm_medium') class="active" @endif><a href="{{ url('/dashboard/statistic') }}?sort=utm_medium">По utm_medium</a></li>--}}
                {{--<li @if ($sort == 'utm_campaign') class="active" @endif><a href="{{ url('/dashboard/statistic') }}?sort=utm_campaign">По utm_campaign</a></li>--}}
                <li @if ($sort == 'utm_term') class="active" @endif><a href="{{ url('/dashboard/statistic') }}?sort=utm_term">По потокам</a></li>
                <li @if ($sort == 'utm_content') class="active" @endif><a href="{{ url('/dashboard/statistic') }}?sort=utm_content">По источникам трафика</a></li>

            </ul>
        </div>
		<div class="x_content">
            @include('admin.statistic.chart')

			@include('admin.statistic.filter')
			<div id="statistic-list-wrap">
                @if ($sort == 'utm_source' || $sort == 'utm_medium' || $sort == 'utm_campaign' || $sort == 'utm_term' || $sort == 'utm_content')
                    @include('admin.statistic.statistic-utm-list')
                @else
                    @include('admin.statistic.statistic-list')
                @endif
			</div>
		</div>
	</div>
</div>
@endsection

@section('bottomScript')
	<script type="application/javascript" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

	<script>
        var cols;

        @if ($sort == 'utm_source' || $sort == 'utm_medium' || $sort == 'utm_campaign' || $sort == 'utm_term' || $sort == 'utm_content')
            cols = [1, 2, 4];
        @else
            cols = [1, 2, 4, 5];
        @endif

        $(document).ready(function() {
            var currentTable = $('#statistic-list-wrap table');

            $('.js-datepicker').datepicker({
                dateFormat: 'dd.mm.yy',
                maxDate: new Date()
            });

            currentTable.DataTable({
                "bFilter" : false,
                "bLengthChange": false,
                dom: 'Bfrtip',
                "pageLength": 50,
//                "aaSorting" : [[]],
                "bSort" : false,
                language: {
                    paginate: {
                        next: '&#8594;', // or '→'
                        previous: '&#8592;' // or '←'
                    }
                },
                bInfo: false,
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    cols.forEach(function (colIndex) {
                        // Total over all pages
                        var total = api
                            .column( colIndex )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

//                        // Total over this page
//                        pageTotal = api
//                            .column( colIndex, { page: 'current'} )
//                            .data()
//                            .reduce( function (a, b) {
//                                return intVal(a) + intVal(b);
//                            }, 0 );

                        // Update footer
                        $( api.column( colIndex ).footer() ).html(total);
                    });
                }
			});
        });

        window.dataTableCallback = function () {
            $('#statistic-list-wrap table').DataTable().destroy();
            $('#statistic-list-wrap table').DataTable({
                "bFilter" : false,
                "bLengthChange": false,
                "pageLength": 50,
                dom: 'Bfrtip',
                "bSort" : false,

                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    cols.forEach(function (colIndex) {
                        // Total over all pages
                        var total = api
                            .column( colIndex )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

//                        // Total over this page
//                        pageTotal = api
//                            .column( colIndex, { page: 'current'} )
//                            .data()
//                            .reduce( function (a, b) {
//                                return intVal(a) + intVal(b);
//                            }, 0 );

                        // Update footer
                        $( api.column( colIndex ).footer() ).html(total);
                    });
                }
            });
        }
	</script>
@endsection