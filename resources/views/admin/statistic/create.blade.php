@extends('layouts.admin')
@section('pageTitle')
	{{ trans('ui.address') }}
@endsection


@section('content')
	<div class="col-xs-12">
		<form action="" method="post" id="address-form">
			<div class="x_panel">
				<ul class="list-inline top-btns">
					<li>
						<button class="btn btn-block btn-success">{{ trans('ui.save') }}</button>
					</li>
				</ul>
				<div class="x_content">
				</div>
				<div class="x_content">
					<div id="example2_wrapper" class="dataTables_wrapper  dt-bootstrap">
						<div class="row">
							<div class="col-sm-6"></div>
							<div class="col-sm-6"></div>
							<div class="col-sm-6"></div>
						</div>
						<div class="row">
							<div class="col-md-9 col-sm-12 col-xs-12">

								@include('admin.address.create-table')
								
							</div>
						</div>
					</div>
				</div><!-- /.box-body -->
			</div>
		</form>
	</div>

@endsection

@section('bottomScript')
	<script type="text/javascript">
		$( "#address-form" ).validate({
			rules: {
				'form[house]': {
					required: true
				}
			},
			errorPlacement: function(error,element) {
				return true;
			},
			invalidHandler: function(event, validator) {

				var errors = validator.numberOfInvalids();

				if (errors) {
					$.each(validator.errorMap, function(index, value) {
						showError(value);
					});
				} else {
					hideErrors();
				}
			}
		});
	</script>
@endsection