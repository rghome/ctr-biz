<div class="scroll-holder">
	<table id="example2" class="table table-bordered table-min-450 table-hover dataTable" role="grid" aria-describedby="example2_info">
		<tbody>
		<tr>
			<td width="25%">{{ trans('ui.city') }}</td>
			<td>
				<select class="form-control search-filter"
						data-filter-url="/manager/address/filter"
						data-filter-model="District"
						data-filter-key="region_id"
						data-filter-to="address_district_id"
						name="form[region_id]"
				>
					<option>{{ trans('ui.select') }}</option>
					@foreach ($regions as $region)

						<option value="{{ $region->id }}"
								@if (isset($address->cities->district->region->id) && $address->cities->district->region->id == $region->id)
								selected
								@elseif(old('form.region_id') && (old('form.region_id') == $region->id))
								selected
								@endif
						>{{ $region->name }}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr>
			<td>{{ trans('ui.district') }}</td>
			<td>
				<select class="form-control search-filter" id="address_district_id"
						data-filter-url="/manager/address/filter"
						data-filter-model="City"
						data-filter-key="district_id"
						data-filter-to="address_city_id"
						name="form[district_id]">
					<option>{{ trans('ui.select') }}</option>
					@foreach ($districts as $district)
						<option value="{{ $district->id }}"
								@if (isset($address->cities->district->id) && $address->cities->district->id == $district->id)
								selected
								@elseif(old('form.district_id') && (old('form.district_id') == $district->id))
								selected
								@endif

						>{{ $district->name }}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr>
			<td>{{ trans('ui.title') }}</td>
			<td>
				<select class="form-control search-filter" id="address_city_id"
						data-filter-url="/manager/address/filter"
						data-filter-model="Street"
						data-filter-key="city_id"
						data-filter-to="address_street_id"
						name="form[city_id]"
				>
					<option>{{ trans('ui.select') }}</option>
					@foreach ($cities as $r)
						<option value="{{ $r->id }}"
								@if (isset($address) && isset($address->city_id) && ($address->city_id == $r->id))
								selected
								@elseif(old('form.city_id') && (old('form.city_id') == $r->id))
								selected
								@endif
						>{{ $r->name }}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr>
			<td>{{ trans('ui.street') }}</td>
			<td>
				<select class="form-control search-filter" id="address_street_id" name="form[street]">
					<option>{{ trans('ui.select') }}</option>
					@foreach ($streets as $r)
						<option value="{{ $r->id }}"
								@if (isset($address) && isset($address->street_id) && ($address->street_id == $r->id))
								selected
								@elseif(old('form.street') && (old('form.street') == $r->id))
								selected
								@endif
						>{{ $r->name }}</option>
					@endforeach
				</select>

				{{--<input class="form-control" type="text" name='form[street]'--}}
				{{--@if (isset($address) && isset($address->streets->name))--}}
				{{--value="{{$address->streets->name}}"--}}
				{{--@endif--}}
				{{-->--}}
			</td>
		</tr>
		<tr>
			<td>{{ trans('ui.home') }}</td>
			<td>
				<input class="form-control" type="text" name='form[house]'
					   @if (isset($address) && isset($address->house))
					    value="{{$address->house}}"
					   @elseif(old('form.house'))
					   	value="{{ old('form.house') }}"
					   @endif
				>
			</td>
		</tr>
		</tbody>
	</table>
</div>