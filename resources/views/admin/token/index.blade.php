@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.menu_tokens') }}
@endsection

@section('bottomScript')
    <script>
        $('.delete_token').on('click', function(e) {
            e.preventDefault();

            var self = $(this);

            if(confirm('Удалить токен?')) {
                location.href = '/dashboard/token/delete/' + self.data('token-id');
            }
        });
    </script>
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <ul class="list-inline top-btns">
                    <li>
                        <a class="btn btn-block btn-success " href="/dashboard/token/create">Новый</a>
                    </li>
                </ul>
            </div>
            <div class="x_content">
                <div class="scroll-holder" id="tokens-list-wrap">
                    <table id="example2" class="table members-table table-bordered table-hover dataTable">
                        <thead>

                        <tr role="row">
                            {{--<th width="2%">#</th>--}}
                            <th>{{ trans('ui.email') }}</th>
                            <th>{{ trans('ui.token') }}</th>
                            <th>{{ trans('ui.offers') }}</th>
                            <th>{{ trans('ui.count') }}</th>
                            <th>{{ trans('ui.act') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($data as $c)
                            <tr role="row" class="odd">
                                {{--<td class="sorting_1">{{$c->id}}</td>--}}
                                <td>{{$c->email}}</td>
                                <td>{{$c->token}}</td>
                                <td>
                                    <ul>
                                    @foreach($c->offers as $tokenOffer)
                                        <li>{{$tokenOffer->offer->name}}</li>
                                    @endforeach
                                    </ul>
                                </td>
                                <td>{{$c->count}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn  btn-primary " href="/dashboard/token/edit/{{ $c->id }}"><i class="fa fa-fw fa-edit"></i></a>
                                        <a class="btn btn-danger delete_token" data-token-id="{{ $c->id }}" ><i class="fa fa-fw fa-remove"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr role="row">
                                <td colspan="5" style="text-align: center;">{{trans('ui.not_found')}}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    {{--{{ $data->links() }}--}}
                </div>
            </div>
        </div>
    </div>
@endsection
