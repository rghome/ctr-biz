@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.menu_tokens') }}
@endsection

@section('content')
    <div class="col-xs-12">
        <form action="" method="post" id="domain-form" novalidate>
            <div class="box-header">
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9 text-right">
                        <div class="btn-group">
                        </div>
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="x_panel">
                <div class="list-inline top-btns">
                    <li>
                        <button class="btn btn-block btn-success">{{ trans('ui.save') }}</button>
                    </li>
                </div>
                <div class="x_content">
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6"></div>
                        </div>
                        <div class="row">
                            <div>
                                <table class="table table-bordered table-hover" role="grid">
                                    <tbody>
                                        <tr>
                                            <td>{{ trans('ui.email') }}</td>
                                            <td>
                                                <input required class="form-control" type="text" name="form[email]" @if (isset($token) && isset($token->email))value="{{$token->email}}"@endif>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>{{ trans('ui.ym_token') }}</td>
                                            <td>
                                                <input required class="form-control" type="text" name="form[token]" @if (isset($token) && isset($token->token))value="{{$token->token}}"@endif>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>{{ trans('ui.menu_offers') }}</td>
                                            <td>
                                                <select class="select2_group" style="width: 300px;" multiple name="form[offers][]">
                                                    @foreach($offers as $offer)
                                                        <option value="{{$offer->id}}">{{$offer->name}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </form>
    </div>
@endsection

@section('bottomScript')
    <script>
        $(document).ready(function () {
            {{--$('input[name="form[name]"]').mask('http://AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/', {--}}
            {{--translation: {--}}
            {{--A: {--}}
            {{--pattern: /[\w@\-.+]/,--}}
            {{--recursive: true--}}
            {{--}--}}
            {{--},--}}
            {{--placeholder: "http://"--}}
            {{--});--}}
        });
    </script>
@endsection