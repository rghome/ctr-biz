@extends('layouts.admin')


@section('content')
    @if(count($errors) >0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li> {{$error}}</li>
                @endforeach
            </ul>

        </div>
    @endif

    @if( isset($error)  )
        <div class="alert alert-danger">
            <ul>
                <li>  {{$error}}</li>
            </ul>
        </div>

    @endif

    @if( isset($success)  )
        <div class="alert alert-success">
            <ul>
                <li>  {{$success}}</li>
            </ul>
        </div>

        @endif

        <div class="col-xs-12">

            <div class="page-title">
                <div class="title_left">
                    <h3 class="box-title">{{ trans('ui.edit_user') }}: {{$user['name']}}</h3>
                </div>
            </div>
            <div class="x_panel">
                @if (!isset($profile_page))
                    <form class="form-horizontal" method="post" action="/dashboard/users/edit/{{$user['id']}}">
                @else
                    <form class="form-horizontal" method="post" action="/dashboard/user/profile">
                @endif
                    <ul class="list-inline top-btns">
                        <li>
                            <button type="submit" class="btn btn-success">{{ trans('ui.save') }}</button>
                        </li>
                    </ul>
                    <div class="x_content">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap">
                            <div class="col-md-6 col-sm-9 col-xs-12">
                                {{--<div class="form-group">--}}
                                {{--<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Имя:</label>--}}
                                {{--<div class="col-md-9 col-sm-9 col-xs-12">--}}
                                {{--<input type="text" class="form-control" id="name" placeholder="Введите имя" value="{{$user['name']}}" name="name" required>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">{{ trans('ui.name') }}:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" class="form-control" id="name" placeholder="{{ trans('ui.name') }}"
                                               value="{{$user['name']}}" name="name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                           for="email">{{ trans('ui.email') }}:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" class="form-control" id="email"
                                               placeholder="{{ trans('ui.email') }}" value="{{$user['email']}}"
                                               name="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">{{ trans('ui.phone') }}:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" class="form-control js-phone-mask" id="phone"
                                               placeholder="Введите телефон" value="{{$user['phone']}}" name="phone">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">{{ trans('ui.address') }}:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" class="form-control" id="address" placeholder="{{ trans('ui.address') }}"
                                               value="{{$user['address']}}" name="address">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="login">{{ trans('ui.login') }}:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" class="form-control" id="login" placeholder="Enter login"
                                               value="{{$user['login']}}" name="login">
                                    </div>
                                </div>
                                @if (Auth::user()->role_id == 1)
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">{{ trans('ui.role') }}:</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <select class="form-control select2_group" name="role_id">
                                                <option value="">{{ trans('ui.select') }}</option>
                                                @foreach ($roles as $r)
                                                    <option value="{{ $r->id }}"
                                                            @if (isset($user) && isset($user['role_id']) && ($user['role_id'] == $r->id))
                                                            selected
                                                            @endif
                                                    >{{ $r->description }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="login">{{ trans('ui.warnings') }}:</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="number" class="form-control" id="warnings" placeholder="{{ trans('ui.warnings') }}"
                                                   value="{{$user['warnings']}}" name="warnings">
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="login">{{ trans('ui.webmoney') }}:</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" class="form-control" id="webmoney" placeholder="{{ trans('ui.webmoney') }}"
                                               value="{{$user['webmoney']}}" name="webmoney">
                                    </div>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<label class="control-label col-md-3 col-sm-3 col-xs-12" for="yandexmondey">{{ trans('ui.yandexmondey') }}:</label>--}}
                                    {{--<div class="col-md-9 col-sm-9 col-xs-12">--}}
                                        {{--<input type="text" class="form-control" id="yandexmondey" placeholder="{{ trans('ui.yandexmondey') }}"--}}
                                               {{--value="{{$user['yandexmondey']}}" name="yandexmondey">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" ></label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="button" class="form-control" id="button_change_password" value="{{ trans('ui.change_password') }}">
                                    </div>
                                </div>
                                <div id="change_password" style="display: none;">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="newpass">{{ trans('ui.new_pass') }}:</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="password" class="form-control" id="newpass" name="newpass">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="newpass2">{{ trans('ui.password_confirmation') }}:</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="password" class="form-control" id="newpass" name="newpass2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.box-body -->
        </div><!-- /.box -->
        </div>

        </div>
@endsection

@section('bottomScript')
    <script>
        $('#button_change_password').on('click', function(e) {
            e.preventDefault();
            $('#change_password').toggle();
        });
    </script>
@endsection



