@extends('layouts.admin')

@section('content')

    @if(count($errors) >0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li> {{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if( isset($success)  )
        <div class="alert alert-success">
            <ul>
                <li>  {{$success}}</li>
            </ul>
        </div>
    @endif

    <div class="col-xs-12">

        <div class="page-title">
            <div class="title_left">
                <h3 class="box-title">{{ trans('ui.create_user') }}</h3>
            </div>
        </div>
        <div class="x_panel">
            <form class="form-horizontal" method="post" action="{{ url('/dashboard/user/create') }}">
                <ul class="list-inline top-btns">
                    <li>
                        <button type="submit" class="btn btn-success">{{ trans('ui.save') }}</button>
                    </li>
                </ul>
                <div class="x_content">
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap">
                        <div class="col-md-6 col-sm-9 col-xs-12">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">{{ trans('ui.name') }}:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" id="name" placeholder="{{ trans('ui.name') }}"
                                           value="@if(isset($user->name)){{ $user->name }}@endif" name="name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="login">{{ trans('ui.login') }}:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" id="text" placeholder="{{ trans('ui.enter_login') }}" value="@if(isset($user->login)){{ $user->login }}@endif"
                                           name="login">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                       for="email">{{ trans('ui.email') }}:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" id="email"
                                           placeholder="{{ trans('ui.email') }}" value="@if(isset($user->email)){{ $user->email }}@endif" name="email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">{{ trans('ui.phone') }}:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control js-phone-mask" id="phone" placeholder="{{ trans('ui.pass_phone') }}"
                                           value="@if(isset($user->phone)){{ $user->phone }}@endif" name="phone">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">{{ trans('ui.address') }}:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" id="address" placeholder="{{ trans('ui.address') }}"
                                           value="@if(isset($user->address)){{ $user->address }}@endif" name="address">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">{{trans('ui.group')}}:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control select2_group" name="role_id">
                                        <option value="">{{ trans('ui.select') }}</option>
                                        @foreach ($roles as $role)
                                            <option value="{{ $role->id }}">{{ $role->description }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="login">{{ trans('ui.webmoney') }}:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" id="text" placeholder="{{ trans('ui.webmoney') }}" value="@if(isset($user->webmoney)){{ $user->webmoney }}@endif"
                                           name="webmoney">
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label class="control-label col-md-3 col-sm-3 col-xs-12" for="yandexmondey">{{ trans('ui.yandexmondey') }}:</label>--}}
                                {{--<div class="col-md-9 col-sm-9 col-xs-12">--}}
                                    {{--<input type="text" class="form-control" id="text" placeholder="{{ trans('ui.yandexmondey') }}" value="@if(isset($user->yandexmondey)){{ $user->yandexmondey }}@endif"--}}
                                           {{--name="yandexmondey">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="newpass">{{ trans('ui.new_pass') }}:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="password" class="form-control" id="newpass" name="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="newpass2">{{ trans('ui.password_confirmation') }}:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="password" class="form-control" id="newpass" name="password_confirm"
                                           required>
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!-- /.box-body -->
            </form>
        </div><!-- /.box -->
    </div>

    </div>
@endsection



