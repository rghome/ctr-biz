@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.menu_users') }}
@endsection

@section('content')
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="list-inline top-btns">
                @if(RoleVerifier::can('user.create'))
                    <div class="btn-group">
                        <a class="btn btn-block btn-success " href="{{ url('/dashboard/user/create') }}">{{ trans('ui.create') }}</a>
                    </div>
                @endif
            </div>
            <div class="x_content">
                <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6"></div>
                    </div><div class="row">
                        <div>
                            <div class="scroll-holder">
                                <table id="example2" class="table table-bordered table-hover dataTable members-table" role="grid" aria-describedby="example2_info">
                                    <thead>

                                    <tr role="row">
                                        <th width="2%">#</th>
                                        <th>{{ trans('ui.name') }}</th>
                                        <th>{{ trans('ui.login') }}</th>
                                        <th>{{ trans('ui.email') }}</th>
                                        <th>{{ trans('ui.group') }}</th>
                                        <th>{{ trans('ui.webmoney') }}</th>
                                        {{--<th>{{ trans('ui.yandexmondey') }}</th>--}}
                                        <th>{{ trans('ui.createdate') }}</th>
                                        <th>{{ trans('ui.dateupdate') }}</th>
                                        <th width="10%">{{ trans('ui.act') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($users as $user)
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{$user->id}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->login}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->role->name}}</td>
                                            <td>{{$user->webmoney}}</td>
                                            {{--<td>{{$user->yandexmondey}}</td>--}}
                                            <td>{{$user->created_at}}</td>
                                            <td>{{$user->updated_at}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    @if(RoleVerifier::can('user.edit'))
                                                        <a class="btn  btn-primary " href="/dashboard/users/{{ $user->id }}"><i class="fa fa-fw fa-edit"></i></a>
                                                    @endif
                                                    @if(RoleVerifier::can('user.delete'))
                                                        <a class="btn btn-danger delete_user" data-user-id="{{ $user->id }}"><i class="fa fa-fw fa-remove"></i></a>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-7">
                            <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                                {!! $users->render() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
        @endsection

        @section('bottomScript')
            <script>
                $('.delete_user').on('click', function(e) {
                    e.preventDefault();

                    var self = $(this);

                    if(confirm('Удалить пользователя ?')) {
                        location.href = '/dashboard/users/delete/' + self.data('user-id');
                    }
                });
            </script>
@endsection