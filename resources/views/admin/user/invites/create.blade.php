@extends('layouts.admin')

@section('content')

    @if(count($errors) >0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li> {{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if( isset($success)  )
        <div class="alert alert-success">
            <ul>
                <li>  {{$success}}</li>
            </ul>
        </div>
    @endif

    <div class="col-xs-12">

        <div class="page-title">
            <div class="title_left">
                <h3 class="box-title">{{ trans('ui.create_invite') }}</h3>
            </div>
        </div>
        <div class="x_panel">
            <form class="form-horizontal" method="post" action="{{ url('/dashboard/user/invite/create') }}">
                <ul class="list-inline top-btns">
                    <li>
                        <button type="submit" class="btn btn-success">{{ trans('ui.save') }}</button>
                    </li>
                </ul>
                <div class="x_content">
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap">
                        <div class="col-md-6 col-sm-9 col-xs-12">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="firstname">{{ trans('ui.login') }}:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" id="name" placeholder="{{ trans('ui.login') }}"
                                           value="@if(isset($invite->name)){{ $invite->name }}@endif" name="name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                       for="secondname">{{ trans('ui.email') }}:</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" id="email"
                                           placeholder="{{ trans('ui.email') }}" value="@if(isset($invite->email)){{ $invite->email }}@endif" name="email" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </form>
        </div><!-- /.box -->
    </div>

    </div>
@endsection



