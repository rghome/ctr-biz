@extends('layouts.admin')
@section('pageTitle')
    {{ trans('ui.menu_users_invites') }}
@endsection

@section('content')
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="list-inline top-btns">
                @if(RoleVerifier::can('user.create'))
                    <div class="btn-group">
                        <a class="btn btn-block btn-success " href="{{ url('/dashboard/user/invite/create') }}">{{ trans('ui.create') }}</a>
                    </div>
                @endif
            </div>
            <div class="x_content">
                <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6"></div>
                    </div><div class="row">
                        <div>
                            <div class="scroll-holder">
                                <table id="example2" class="table table-bordered table-hover dataTable members-table" role="grid" aria-describedby="example2_info">
                                    <thead>

                                    <tr role="row">
                                        <th width="2%">#</th>
                                        <th>Имя</th>
                                        <th>Email</th>
                                        <th>Дата приглашения</th>
                                        <th>Дата регистрации</th>
                                        <th>Статус</th>
                                        <th width="10%">{{ trans('ui.act') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($invites as $invite)
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{$invite->id}}</td>
                                            <td>{{$invite->name}}</td>
                                            <td>{{$invite->email}}</td>
                                            <td>{{$invite->created_at}}</td>
                                            <td>{{$invite->updated_at}}</td>
                                            <td>@if ($invite->accepted == 0)<span class="label label-primary payment-status">В ожидании</span>@elseif($invite->accepted == 1) <span class="label label-success payment-status">Зарегистрирован</span>@endif</td>
                                            <td>
                                                <div class="btn-group">
                                                    @if(RoleVerifier::can('user.edit'))
                                                        <a class="btn btn-info" data-remote="false" data-toggle="modal" data-target="#invite-show-link" href="/dashboard/user/invite/show/{{ $invite->id }}"><i data-toggle="tooltip" title="Показать ссылку" class="fa fa-fw fa-link"></i></a>
                                                    @endif
                                                    @if(RoleVerifier::can('user.delete'))
                                                        <a class="btn btn-success send-invite-link"  href="/dashboard/user/invite/send/{{ $invite->id }}"><i data-toggle="tooltip" title="Отправить приглашение на email" class="fa fa-fw fa-envelope-o"></i></a>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr role="row">
                                            <td colspan="7" style="text-align: center;">{{trans('ui.not_found')}}</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-7">
                            <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                                {!! $invites->render() !!}
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
@endsection




@section('bottomScript')
    <div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="invite-show-link">
        <div class="modal-dialog modal-lg">
            <div class="modal-content live-form">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2">{{ trans('ui.invite_modal_title') }}</h4>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

<script>
    $("#invite-show-link").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
        $(this).find(".modal-footer #link").val('');
    });

    $('.send-invite-link').on('click', function (e) {
        e.preventDefault();
        var link = $(this);

        preloaderShow();
        $.get(link.attr('href'))
            .always(function () {
               preloaderHide();
               showSuccess('Email отправлен')
            });
    })
</script>
@endsection