<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="">

	<link rel="stylesheet" href="/vendors/normalize-css/normalize.css">
	<link rel="stylesheet" href="/vendors/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/vendors/bootstrap/dist/css/bootstrap.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="/vendors/iCheck/skins/flat/green.css">
	<!-- Switchery -->
	<link href="/vendors/switchery/dist/switchery.min.css" rel="stylesheet">

	<!-- PNotify -->
	<link href="/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
	<link href="/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
	<link href="/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

	<!-- jquery-ui-1.12.1 -->
	<link href="/vendors/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">

	<link rel="stylesheet" href="/vendors/themes/gentelella/css/custom.min.css">

	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/media.css">


	<title>Dashboard</title>


	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

<div class="container">
	<div class="stat-panel-holder">
		<div class="stat-panel">
			<div class="panel panel-default">
				<div class="panel-heading">
					<span class="stat">{{ trans('dashboard.statistic') }}</span>
					<a href="{{ url('manager/members') }}" class="btn btn-success to-dash">{{ trans('ui.tomenu') }}</a>
					<ul class="list-inline list-unstyled lang-panel">
						<li>
							<a href="{{ url('language/ua') }}">ua</a>
						</li>
						<li>
							<a href="{{ url('language/ru') }}">ru</a>
						</li>
						<li>
							<a href="{{ url('language/en') }}">en</a>
						</li>
					</ul>
					<ul id="per-list" class="list-inline">
						<li>
							<a href="#" class="active data-sv" data-val="year">{{ trans('dashboard.year') }}</a>
						</li>
						<li>
							<a href="#" class="data-sv"  data-val="month">{{ trans('dashboard.month') }}</a>
						</li>
						<li>
							<a href="#" class="data-sv"  data-val="day">{{ trans('dashboard.day') }}</a>
						</li>
					</ul>
				</div>
				<div class="panel-body">
					<div class="clearfix">
						<div class="all-blocks">
							<div class="x_panel">
								<div class="x_title">
									<h5><i class="fa fa-globe"></i>{{ trans('dashboard.oblastey') }}</h5>
								</div>
								<p class="x_count">
									24
								</p>
								<div class="x_content">
										<span id="stats" class="chart" data-percent="29">
											<span class="percent-top"> 7</span>
											<span class="percent"></span>
										</span>
								</div>
							</div>
							<div class="x_panel">
								<div class="x_title">
									<h5><i class="fa fa-dot-circle-o"></i>{{ trans('dashboard.raenov') }}</h5>
								</div>
								<p class="x_count">
									75
								</p>
								<div class="x_content">
										<span id="areas" class="chart" data-percent="84">
											<span class="percent-top">63</span>
											<span class="percent"></span>
										</span>
								</div>
							</div>
							<div class="x_panel">
								<div class="x_title">
									<h5><i class="fa fa-circle-thin"></i>{{ trans('dashboard.naspunktov') }}</h5>
								</div>
								<p class="x_count">
									840
								</p>
								<div class="x_content">
										<span id="cities" class="chart" data-percent="90  ">
											<span class="percent-top">758</span>
											<span class="percent"></span>
										</span>
								</div>
							</div>
							<div class="x_panel">
								<div class="x_title">
									<h5><i class="fa fa-male"></i>{{ trans('dashboard.chlenovkoop') }}</h5>
								</div>
								<p class="x_count">

								</p>
								<div class="x_content">
										<span id="members"  class="chart" data-percent="63">
											<span class="percent-top"> 12 500</span>
											<span class="percent"></span>
										</span>
								</div>
							</div>
							<div class="x_panel">
								<div class="x_title">
									<h5><i class="fa fa-sitemap"></i></i>{{ trans('dashboard.dvorov') }}</h5>
								</div>
								<p class="x_count">
									20 000
								</p>
								<div class="x_content">
										<span id="dvors" class="chart" data-percent="63">
											<span class="percent-top"> 12 500</span>
											<span class="percent"></span>
										</span>
								</div>
							</div>
							<div class="x_panel">
								<div class="x_title">
									<h5><i class="fa fa-github-alt"></i></i>{{ trans('dashboard.cows') }}</h5>
								</div>
								<p class="x_count"></p>
								<div class="x_content">
										<span id="faces" class="chart" data-percent="55">
											<span class="percent-top"> 12 500</span>
											<span class="percent"></span>
										</span>
								</div>
							</div>
						</div>
						<div class="products-block">
							<div class="x_panel">
								<div class="x_title">
									<select id="prod-type" class="form-control">
										@foreach($products as $p)
											<option value="product-{{$p->id}}">{{$p->name}}, {{$p->unit->code}}</option>
										@endforeach
									</select>
								</div>
								<p class="x_count">

								</p>
								<div class="x_content">
										<span  id="products" class=" chart-resources" data-percent="10299">
											<span class="percent nop">10 299</span>
										</span>
								</div>
							</div>
							<div class="x_panel">
								<div class="x_title">
									<h5>{{ trans('dashboard.icon_cur') }} {{ trans('dashboard.dohod') }}
										<span class="units">{{ trans('dashboard.tus_hrn') }}</span>
									</h5>
								</div>
								<p class="x_count">

								</p>
								<div class="x_content">
										<span id="income" class="chart-resources" data-percent="12863">
											<span class="percent nop">47,13</span>
										</span>
								</div>
							</div>
							<div class="x_panel">
								<div class="x_title">
									<h5>{{ trans('dashboard.icon_cur') }} {{ trans('dashboard.zakupka') }}
										<span class="units">{{ trans('dashboard.tus_hrn') }}</span>
									</h5>
								</div>
								<p class="x_count">

								</p>
								<div class="x_content">
										<span id="summ-buy" class="chart-resources" data-percent="47131">
											<span class="percent nop">59,96</span>
										</span>
								</div>
							</div>
							<div class="x_panel">
								<div class="x_title">
									<h5>{{ trans('dashboard.icon_cur') }} {{ trans('dashboard.prodaja') }}
										<span class="units">{{ trans('dashboard.tus_hrn') }}</span>
									</h5>
								</div>
								<p class="x_count">

								</p>
								<div class="x_content">
										<span id="summ-sell" class="chart-resources" data-percent="59968">
											<span class="percent nop">12,83</span>
										</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Scripts -->
<script src="/vendors/jquery/dist/jquery.min.js"></script>
<script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/vendors/validator/validator.js"></script>
<script src="/vendors/iCheck/icheck.min.js"></script>
<script src="/vendors/switchery/dist/switchery.min.js"></script>
<!-- PNotify -->
<script src="/vendors/pnotify/dist/pnotify.js"></script>
<script src="/vendors/pnotify/dist/pnotify.buttons.js"></script>
<script src="/vendors/pnotify/dist/pnotify.nonblock.js"></script>

<script src="/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>

<!-- jquery-ui-1.12.1 -->
<script src="/vendors/jquery-ui-1.12.1/jquery-ui.min.js"></script>

<script src="/vendors/themes/gentelella/js/custom.min.js"></script>



<script>
	var dataCharts2 = JSON.parse('{!! $data	!!}');

	var dataCharts = {
			lastYear: {
				obl: {
					count: 7,
					percent: 29,
					plan: 24
				},
				rans: {
					count: 54,
					percent: 69,
					plan: 78
				},
				cities: {
					count: 1241,
					percent: 55,
					plan: 2256
				},
				users: {
					count: 26542,
					percent: 33,
					plan: 80000
				},
				dvors: {
					count: 26542,
					percent: 33,
					plan: 80000
				},
				faces: {
					count: 26933,
					percent: 34,
					plan: ''
				},
				exchangeRates: '{{ trans('dashboard.exchangeRates') }}',
				period: 365,
				product: {
					milk: {
						count: "10 299",
						buy: 47131566.35,
						sell: 59968532.59,
						income: 12836966.24,
						units: '{{ trans('dashboard.mln_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_l') }}'
					},
					nuts: {
						count: "212",
						buy: 24298506.21,
						sell: 33745529.51,
						income: 9447023.3,
						units: '{{ trans('dashboard.mln_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					},
					beans: {
						count: "548",
						buy: 11614054.37,
						sell: 15165367.14,
						income: 3551312.768,
						units: '{{ trans('dashboard.mln_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					},
					apples: {
						count: '4 820',
						buy: 6367440.574,
						sell: 15288974.53,
						income: 8921533.956,
						units: '{{ trans('dashboard.mln_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					},
					bulls: {
						count: '321',
						buy: 9288867.596,
						sell: 18153226.16,
						income: 8864358.568	,
						units: '{{ trans('dashboard.mln_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					}
				}
			},
			lastMonth: {
				obl: {
					count: 1,
					percent: 100,
					plan: 1
				},
				rans: {
					count: 4,
					percent: 57,
					plan: 7
				},
				cities: {
					count: 108,
					percent: 57,
					plan: 188
				},
				users: {
					count: 2318,
					percent: 35,
					plan: 6600
				},
				dvors: {
					count: 2318,
					percent: 35,
					plan: 6600
				},
				faces: {
					count: 2354,
					percent: 36,
					plan: 6500
				},
				exchangeRates: '{{ trans('dashboard.exchangeRates') }}',
				period: 30,
				product: {
					milk: {
						count: '858.25',
						buy: 3927630.529,
						sell: 4997377.716,
						income: 1069747.187,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_l') }}'
					},
					nuts: {
						count: '17.66',
						buy: 2024875.518,
						sell: 2812127.459,
						income: 787251.9417,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					},
					beans: {
						count: '45.66',
						buy: 967837.8642,
						sell: 1263780.595,
						income: 295942.7307,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					},
					apples: {
						count: '401.66',
						buy: 530620.0478,
						sell: 1274081.211,
						income: 743461.163,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					},
					bulls: {
						count: "26.75",
						buy: 774072.2997,
						sell: 1512768.847,
						income: 738696.5473,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					}
				}
			},
			lastDay: {
				obl: {
					count: 1,
					percent: 100,
					plan: 1
				},
				rans: {
					count: 1,
					percent: 50,
					plan: 2
				},
				cities: {
					count: 3,
					percent: 100,
					plan: 3
				},
				users: {
					count: 17,
					percent: 94,
					plan: 18
				},
				dvors: {
					count: 17,
					percent: 94,
					plan: 18
				},
				faces: {
					count: 16,
					percent: 94,
					plan: 17
				},
				exchangeRates: '{{ trans('dashboard.exchangeRates') }}',
				period: 1,
				product: {
					milk: {
						count: '28.21',
						buy: 129127.579,
						sell: 164297.3496,
						income: 35169.77052,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_l') }}'
					},
					nuts: {
						count: '0.58',
						buy: 66571.24989,
						sell: 92453.50551,
						income: 25882.25562,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					},
					beans: {
						count: '1.5',
						buy: 31819.32704,
						sell: 41548.95107,
						income: 9729.624022,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					},
					apples: {
						count: '13.2',
						buy: 17445.04267,
						sell: 41887.60145,
						income: 24442.55878,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					},
					bulls: {
						count: '0.87',
						buy: 25448.95232,
						sell: 49734.86619,
						income: 24285.91388,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					}
				}
			},
			lastDay1: {
				obl: {
					count: 1,
					percent: 100,
					plan: 1
				},
				rans: {
					count: 1,
					percent: 50,
					plan: 2
				},
				cities: {
					count: 2,
					percent: 67,
					plan: 3
				},
				users: {
					count: 13,
					percent: 72,
					plan: 18
				},
				dvors: {
					count: 13,
					percent: 72,
					plan: 18
				},
				faces: {
					count: 11,
					percent: 64,
					plan: 17
				},
				exchangeRates: '{{ trans('dashboard.tus_l') }}',
				period: 1,
				product: {
					milk: {
						count: '28.21',
						buy: 129127.579,
						sell: 164297.3496,
						income: 35169.77052,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_l') }}'
					},
					nuts: {
						count: '0.58',
						buy: 66571.24989,
						sell: 92453.50551,
						income: 25882.25562,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					},
					beans: {
						count: '1.5',
						buy: 31819.32704,
						sell: 41548.95107,
						income: 9729.624022,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					},
					apples: {
						count: '13.2',
						buy: 17445.04267,
						sell: 41887.60145,
						income: 24442.55878,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					},
					bulls: {
						count: '0.87',
						buy: 25448.95232,
						sell: 49734.86619,
						income: 24285.91388,
						units: '{{ trans('dashboard.tus_hrn') }}',
						pUnits: '{{ trans('dashboard.tus_kg') }}'
					}
				}
			}
		};

		var curentData = {},
			curentVal = "milk";

			makeNumber = function(num, obj) {

			    return num;

				var exchangeRates = parseFloat(obj.exchangeRates),
						delimiter = '';


				if(exchangeRates > 1) {
					num = num / exchangeRates;

					num = Math.ceil(num);
				};

				if(obj.period == 365 && exchangeRates <= 1) {
					num = num / 1000000;

					num = num.toFixed(2);

					numDel = num.toString().split('.');

					lengthNum = numDel.length;
				} else {
					num = num / 1000;

					num = Math.ceil(num).toString();
					lengthNum = num.toString().length;
				}

				switch(lengthNum) {
					case 4:
					delimiter = num.substr(0,1);
					num = num.slice(1);
					break;

					case 5:
					delimiter = num.substr(0,2);
					num = num.slice(2);
					break;
					case 6:
					delimiter = num.substr(0,3);
					num = num.slice(3);
					break;
				}

				return delimiter + ' ' + num.toString();
			};


			$(document).on('ready', function() {

				curentData = dataCharts2.lastYear;

				$(function() {
					$('.chart').easyPieChart({
						lineWidth: '6',
						barColor: '#4fe2c3',
						trackColor: '#34495e',
						lineWidth: 20,
						trackWidth: 3,
						scaleColor: false,
						lineCap: 'butt',
						onStep: function(from, to, percent) {
							$(this.el).find('.percent').text(Math.round(percent));
						}
					});
				});

				$('.chart-resources').easyPieChart({
					lineWidth: '6',
					barColor: '#34495e',
					trackColor: '#34495e',
					lineWidth: 3,
					trackWidth: 3,
					scaleColor: false,
					lineCap: 'butt'
				});

				$('#prod-type').on('change', function() {
					var $thisVal = $(this).val().split('-')[1];
					var prodData = curentData.products[$thisVal];

					if (prodData === undefined) {
                        prodData = {
                            count: 0,
							units: '{{ trans('dashboard.tus_hrn') }}',
							buy: 0,
							sell: 0,
							income: 0
						};
					}
					$('#products').closest('.x_panel').find('.percent').text(prodData.count ? prodData.count : 0);
					$('#products').closest('.x_panel').find('.units').text(prodData.units);

					$('#summ-buy').closest('.x_panel').find('.percent').text(makeNumber(prodData.buy, curentData));
					$('#summ-buy').closest('.x_panel').find('.units').text(prodData.units);

					$('#summ-sell').closest('.x_panel').find('.percent').text(makeNumber(prodData.sell, curentData));
					$('#summ-sell').closest('.x_panel').find('.units').text(prodData.units);

					$('#income').closest('.x_panel').find('.percent').text(makeNumber(prodData.income, curentData));
					$('#income').closest('.x_panel').find('.units').text(prodData.units);


				});

				updateMainCharts = function() {

					$('#stats').data('easyPieChart').update(parseInt(curentData.obl.percent));
					$('#stats').find('.percent-top').text(curentData.obl.count);
					$('#stats').closest('.x_panel').find('.x_count').text(curentData.obl.plan);

					$('#areas').data('easyPieChart').update(parseInt(curentData.rans.percent));
					$('#areas').find('.percent-top').text(curentData.rans.count);
					$('#areas').closest('.x_panel').find('.x_count').text(curentData.rans.plan);

					$('#cities').data('easyPieChart').update(parseInt(curentData.cities.percent));
					$('#cities').find('.percent-top').text(curentData.cities.count);
					$('#cities').closest('.x_panel').find('.x_count').text(curentData.cities.plan);

					$('#members').data('easyPieChart').update(parseInt(curentData.users.percent));
					$('#members').find('.percent-top').text(curentData.users.count);
					$('#members').closest('.x_panel').find('.x_count').text(curentData.users.plan);

					$('#dvors').data('easyPieChart').update(parseInt(curentData.dvors.percent));
					$('#dvors').find('.percent-top').text(curentData.dvors.count);
					$('#dvors').closest('.x_panel').find('.x_count').text(curentData.dvors.plan);

					$('#faces').data('easyPieChart').update(parseInt(curentData.faces.percent));
					$('#faces').find('.percent-top').text(curentData.faces.count);
					$('#faces').closest('.x_panel').find('.x_count').text(curentData.faces.plan);

					$('#products').data('easyPieChart').update(parseInt(curentData.products[2].count));
					$('#summ-buy').data('easyPieChart').update(parseInt(curentData.products[2].buy));
					$('#summ-sell').data('easyPieChart').update(parseInt(curentData.products[2].sell));
					$('#income').data('easyPieChart').update(parseInt(curentData.products[2].income));

					{{--var optionEL = '<option value="milk">'+"{{ trans("dashboard.milk") }}"+', '+curentData.product[2].pUnits+'</option><option value="nuts">'+"{{ trans("dashboard.nuts") }}"+', '+curentData.product.nuts.pUnits+'</option><option value="beans">'+"{{ trans("dashboard.beans") }}"+'  , '+curentData.product.beans.pUnits+'</option><option value="apples">'+"{{ trans("dashboard.apples") }}"+', '+curentData.product.apples.pUnits+'</option><option value="bulls">'+"{{ trans("dashboard.bulls") }}"+', '+curentData.product.bulls.pUnits+'</option>';--}}
					{{--$('#prod-type').html(optionEL);--}}

					$('#prod-type').val(curentVal);
					$('#prod-type').trigger('change');


				};


				$('.data-sv').each(function() {
					$(this).on('click', function() {

						var thisAttr = $(this).attr('data-val');

							curentVal = $('#prod-type').val();


							switch (thisAttr) {

								case 'year':
									curentData = dataCharts2.lastYear;
									break;

								case 'month':
									curentData = dataCharts2.lastMonth;
									break;

								case 'day':
									curentData = dataCharts2.lastDay;
									break;
							}

					$('.data-sv').removeClass('active');
					$(this).addClass('active');



					updateMainCharts();

					})
				});

				// updateMainCharts();
				// $('#prod-type').html();
				$('#prod-type').trigger('change');
				$('.data-sv.active').trigger('click');
			});
</script>



</body>
</html>
