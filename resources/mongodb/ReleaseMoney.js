var filter = {
    "$and": [
        // {
        //     user_id: 1
        // },
        {
            payment_status: 'hold'
        },
        {
            date: {
                $lte: new ISODate()
            }
        }
    ]
};

db.getCollection('statistic_log').update(filter, {$set: { "payment_status": 'free' } }, { multi: true });