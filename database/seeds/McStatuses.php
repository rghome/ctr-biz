<?php

use Illuminate\Database\Seeder;

class McStatuses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('mc_statuses')->insert(['name' => 'Ассоциируемый']);
	    DB::table('mc_statuses')->insert(['name' => 'Полный']);
    }
}
