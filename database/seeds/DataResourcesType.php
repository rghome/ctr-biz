<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class DataResourcesType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resources_type')->insert(['name' => 'Недвижимость']);
        DB::table('resources_type')->insert(['name' => 'Техника']);
        DB::table('resources_type')->insert(['name' => 'Земля']);
        DB::table('resources_type')->insert(['name' => 'Животноводство']);
        DB::table('resources_type')->insert(['name' => 'Рослинництво']);
        DB::table('resources_type')->insert(['name' => 'Садовничество']);
        DB::table('resources_type')->insert(['name' => 'Другое']);
    }
}
