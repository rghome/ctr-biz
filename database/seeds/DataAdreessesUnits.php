<?php

use Illuminate\Database\Seeder;

class DataAdreessesUnits extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('address_units')->insert(['name' => 'улица']);
        DB::table('address_units')->insert(['name' => 'проспект']);
    }
}
