<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Cases;
use App\User as User;
use App\Role;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use Database\RegionsTableSeeder;
use Database\CitiesTableSeeder;


class DatabaseSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$this->call('RoleTableSeeder');
		$this->call('UserTableSeeder');
	}

}

class UserTableSeeder extends Seeder
{

	public function run()
	{
		User::truncate();

		User::create([
			'email' => '1@1.com',
			'password' => Hash::make('123456'),
			'name' => 'admin',
			'role_id' => 1,
		]);

		User::create([
			'email' => 'monitoring@1.com',
			'password' => Hash::make('123456'),
			'name' => 'admin',
			'role_id' => 4,
		]);
	}
}

class RoleTableSeeder extends Seeder
{
	public function run()
	{
		if (App::environment() === 'production') {
			exit('I just stopped you getting fired. Love, Amo.');
		}
		DB::table('role')->truncate();
		Role::create([
			'id' => 1,
			'name' => 'Root',
			'description' => 'Супер пользователь'
		]);
		Role::create([
			'id' => 2,
			'name' => 'Administrator',
			'description' => 'Администратор'
		]);
		Role::create([
			'id' => 3,
			'name' => 'Manager',
			'description' => 'Менеджер'
		]);
		Role::create([
			'id' => 4,
			'name' => 'Monitoring',
			'description' => 'Мониторинг'
		]);
		Role::create([
			'id' => 5,
			'name' => 'User',
			'description' => 'Пользователь'
		]);
	}
}