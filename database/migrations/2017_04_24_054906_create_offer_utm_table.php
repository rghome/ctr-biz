<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferUtmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offer_utm', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('offer_id')
                ->nullable()
                ->unsigned()
                ->index()
            ;

            $table->foreign('offer_id')
                ->references('id')
                ->on('offers')
            ;

            $table->string('name');
            $table->string('value');


            $table->boolean('disabled');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offer_utm');
    }
}
