<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingsUtmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_utm', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('landing_id')
                ->nullable()
                ->unsigned()
                ->index()
            ;

            $table->foreign('landing_id')
                ->references('id')
                ->on('landings')
            ;

            $table->string('name');
            $table->string('value');


            $table->boolean('disabled');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('landing_utm');
    }
}
