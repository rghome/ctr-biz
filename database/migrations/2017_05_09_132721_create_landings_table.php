<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landings', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('offer_id')
                ->nullable()
                ->index()
            ;

            $table->string('name');
            $table->string('hostname');
            $table->string('params');
            $table->integer('metrika_counter_id');
            $table->integer('metrika_goal_activation_id');
            $table->integer('metrika_goal_main_id');
            $table->string('metrika_utm_panel');
            $table->string('metrika_utm_wm');

            $table->timestamps();

            $table->foreign('offer_id')
                ->references('id')
                ->on('offers')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('landings');
    }
}
