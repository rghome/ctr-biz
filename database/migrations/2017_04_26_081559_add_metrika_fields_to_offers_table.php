<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMetrikaFieldsToOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->integer('metrika_counter_id');
            $table->integer('metrika_goal_activation_id');
            $table->string('metrika_utm_wm');
            $table->string('metrika_utm_panel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->removeColumn('metrika_counter_id');
            $table->removeColumn('metrika_goal_activation_id');
            $table->removeColumn('metrika_utm_wm');
            $table->removeColumn('metrika_utm_panel');
        });
    }
}
