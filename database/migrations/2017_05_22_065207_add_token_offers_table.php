<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTokenOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('token_offers', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('token_id')
                ->nullable()
                ->unsigned()
                ->index()
            ;

            $table->foreign('token_id')
                ->references('id')
                ->on('tokens')
            ;

            $table->integer('offer_id')
                ->nullable()
                ->unsigned()
                ->index()
            ;

            $table->foreign('offer_id')
                ->references('id')
                ->on('offers')
            ;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('token_offers');
    }
}
