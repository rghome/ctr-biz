<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingsRegionPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_region_price', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('landing_id')
                ->nullable()
                ->unsigned()
                ->index()
            ;

            $table->foreign('landing_id')
                ->references('id')
                ->on('landings')
            ;

            $table->integer('region_id')
                ->nullable()
                ->unsigned()
                ->index()
            ;

            $table->foreign('region_id')
                ->references('id')
                ->on('regions')
            ;

            $table->float('price');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('landing_region_price');
    }
}
