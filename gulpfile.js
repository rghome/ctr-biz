var elixir = require('laravel-elixir');
var gulp = require("gulp");

elixir(function (mix) {
	mix.less('app.less');
});

var ts = require('gulp-typescript');
var gulpTsConfig = require('gulp-tsconfig');

// gulp.task('config', function () {
// 	var tsConfig = gulpTsConfig({
// 		tsOrder: [
// 			'resources/assets/editor/src/**/*.ts',
// 			'resources/assets/editor/src/**/Main.ts',
// 		],
// 		tsConfig: {
// 			"compilerOptions": {
// 				"preserve_newlines" : true,
// 				"target": "ES3",
// 				"removeComments": true,
// 				"sourceMap": true,
// 				"noImplicitAny": false,
// 				"out": "app.js"
// 			}
// 		}
// 	});
//
// 	return gulp.src(["resources/assets/editor/src/**/*.ts"])
// 		.pipe(tsConfig())
// 		.pipe(gulp.dest('.'));
// });

gulp.task('scripts', function () {
	var tsProject = ts.createProject('tsconfig.json');
	return tsProject.src()
		.pipe(ts(tsProject))
		.pipe(gulp.dest('public/ed'));
});

gulp.task('default', function () {
	gulp.watch('resources/assets/editor/**/*.ts', ['scripts'/*, 'scripts'*/])
});

